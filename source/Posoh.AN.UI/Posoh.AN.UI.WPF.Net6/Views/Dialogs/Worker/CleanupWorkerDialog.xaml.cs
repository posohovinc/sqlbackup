﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for CleanupTaskDialog.xaml
    /// </summary>
    public partial class CleanupWorkerDialog : AbstractCleanupWorkerDialog
    {
        public CleanupWorkerDialog()
        {
            InitializeComponent();
        }
        protected override void OnInitialized()
        {
            base.OnInitialized();
            CleanupWorkerControl.Self = Vm;
            SharedWorkerControl.DataContext = Vm;
        }
    }
}
