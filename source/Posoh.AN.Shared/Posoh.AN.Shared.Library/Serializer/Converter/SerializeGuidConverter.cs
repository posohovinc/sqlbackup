﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeGuidConverter: SerializeConverter
  {
    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph,  ReflectionClient client, object ancestor)
    {
      value = new Guid(stringValue);
      return true;
    }
  }
}
