﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum BackupCompressionOptions
  {
    Default,
    On,
    Off
  }
}