﻿namespace Posoh.AN.Shared.Library.DataBaseCollector.Objects
{
    public class BaseDbObject
    {
        public string Folder { get; set; }
        public string File { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public override string ToString()
        {
            return Name;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
