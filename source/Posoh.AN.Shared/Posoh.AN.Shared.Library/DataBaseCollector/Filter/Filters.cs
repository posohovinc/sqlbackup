﻿using System;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    [XmlInclude(typeof(IncludeFilters))]
    [XmlInclude(typeof(ExcludeFilters))]
    [Serializable]
    public abstract class Filters
    {
        public Filters()
        {
        }

        public RegexFilter Regex { get; set; }
        public MaskFilter Mask { get; set; }
        public NativeFilter Native { get; set; }
        [XmlIgnoreAttribute]
        [JsonIgnore]
        public abstract Direction Direction { get; }

        public bool Match(string value)
        {
            FilterDecision result;
            if (Native != null)
            {
                result = Native.Match(value, Direction);

                if (result == FilterDecision.Accept)
                    return true;
                else if (result == FilterDecision.Dined)
                    return false;
            }


            if (Regex != null)
            {
                result = Regex.Match(value, Direction);

                if (result == FilterDecision.Accept)
                    return true;
                else if (result == FilterDecision.Dined)
                    return false;
            }

            if (Mask != null)
            {
                result = Mask.Match(value, Direction);

                if (result == FilterDecision.Accept)
                    return true;
                else if (result == FilterDecision.Dined)
                    return false;
            }

            if ((Mask == null || Mask.Patterns == null || Mask.Patterns.Count == 0) &&
                (Regex == null || Regex.Patterns == null || Regex.Patterns.Count == 0) &&
                (Native == null || Native.Patterns == null || Native.Patterns.Count == 0) &&
                Direction == Direction.Include)
                return true;

            return false;
        }

    }
}
