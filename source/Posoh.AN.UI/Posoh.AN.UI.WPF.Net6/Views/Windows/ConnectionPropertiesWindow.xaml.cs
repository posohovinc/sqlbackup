﻿using Posoh.AN.UI.WPF.Net6.Views.Controls;
using Posoh.AN.UI.WPF.Net6.Views.Dialogs;

namespace Posoh.AN.UI.WPF.Net6.Views.Windows
{
    /// <summary>
    /// Interaction logic for ConnectionPropertiesWindow.xaml
    /// </summary>
    public partial class ConnectionPropertiesWindow : BaseDialogWindow
    {
        public ConnectionPropertiesWindow():base("Connection Properties")
        {
            InitializeComponent();
        }

        public override void SetDataContext(object obj)
        {
            connectionPropertiesView.DataContext = obj;
        }

    }
}
