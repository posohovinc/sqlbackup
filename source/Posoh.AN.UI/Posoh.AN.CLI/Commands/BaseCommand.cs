namespace Posoh.AN.CLI.Commands;

public abstract class BaseCommand
{
    #region Properties

    public string[] Args { get; }
    public string Description { get; protected set; }
    public string CommandName { get; protected set;}
    protected string Separator { get; } = " ";
    protected List<Parameter> Parameters { get; set; }

    #endregion

    #region ctor

    public BaseCommand(string[] args)
    {
        Args = args;
    }

    #endregion

    #region Methods
    
    protected abstract void ExecuteCommand();
    
    public void Execute()
    {
        ParseArguments();
        ValidateParameters();
        ExecuteCommand();
    }
    
    private void ParseArguments()
    {
        var args = Args.ToList();
        foreach (var parameter in Parameters)
        {
            var index = args.FindIndex(x => x == parameter.Name);
            if (index > -1 && index < args.Count)
            {
                parameter.Value = args[index + 1];
            }
        }
    }
    
    private void ValidateParameters()
    {
        Parameters.ForEach(x =>
        {
            if (string.IsNullOrEmpty(x.Value))
                throw new ArgumentException($"Argument '{x.Name} is empty'");
        });
    }

    #endregion

}