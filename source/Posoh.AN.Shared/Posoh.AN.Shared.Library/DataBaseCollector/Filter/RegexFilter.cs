﻿using System;
using System.Text.RegularExpressions;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    [Serializable]
    public class RegexFilter : Filter
    {
        public override bool Decide(string pattern, string value)
        {
            Regex regex = new Regex(pattern);
            return regex.IsMatch(value);
        }
    }
}
