using System;
using System.Collections.Generic;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class BackupWorker : MaintenanceWorker
  {

    #region fields
    private BackupActionType backupAction;
    private BackupCompressionOptions backupCompressionOption;
    private DeviceType backupDeviceType;
    private BackupEncryptionAlgorithm backupEncryptionAlgorithm;
    private string backupEncryptorName;
    private BackupEncryptorType backupEncryptorType;
    private string backupFileExtension;
    private bool backupIsIncremental;
    private DeviceType backupPhysicalDestinationType;
    private int blockSize;
    private bool checksum;
    private string containerName;
    private bool continueOnError;
    private bool copyOnlyBackup;
    private bool createSubFolder;
    private string credentialName;
    private string destinationAutoFolderPath;
    private DestinationType destinationCreationType;
    private List<string> destinationManualList;
    private ActionForExistingBackups existingBackupsAction;
    private DateTime expireDate;
    private string fileGroupsFiles;
    private bool ignoreReplicaType;
    private bool inDays;
    private bool isBackupEncrypted;
    private bool isBlockSizeUsed;
    private bool isMaxTransferSizeUsed;
    private int maxTransferSize;
    private int retainDays;
    private string urlPrefix;
    private bool useExpiration;
    private bool verifyIntegrity;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_BACKUP_ACTION)]
    public BackupActionType BackupAction
    {
      get => backupAction;
      set 
      { 
        if (backupAction != value)
        {
          backupAction = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_BACKUP_COMPRESSION_OPTION)]
    public BackupCompressionOptions BackupCompressionOption
    {
      get => backupCompressionOption;
      set 
      { 
        if (backupCompressionOption != value)
        {
          backupCompressionOption = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_BACKUP_DEVICE_TYPE)]
    public DeviceType BackupDeviceType
    {
      get => backupDeviceType;
      set 
      { 
        if (backupDeviceType != value)
        {
          backupDeviceType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1030, Constants.PERSIST_BACKUP_ENCRYPTION_ALGORITHM)]
    public BackupEncryptionAlgorithm BackupEncryptionAlgorithm
    {
      get => backupEncryptionAlgorithm;
      set 
      { 
        if (backupEncryptionAlgorithm != value)
        {
          backupEncryptionAlgorithm = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1040, Constants.PERSIST_BACKUP_ENCRYPTOR_NAME)]
    public string BackupEncryptorName
    {
      get => backupEncryptorName;
      set 
      { 
        value = value ?? string.Empty;
        if (backupEncryptorName != value)
        {
          backupEncryptorName = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1050, Constants.PERSIST_BACKUP_ENCRYPTOR_TYPE)]
    public BackupEncryptorType BackupEncryptorType
    {
      get => backupEncryptorType;
      set 
      { 
        if (backupEncryptorType != value)
        {
          backupEncryptorType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1060, Constants.PERSIST_BACKUP_FILE_EXTENSION)]
    public string BackupFileExtension
    {
      get => backupFileExtension;
      set 
      { 
        value = value ?? string.Empty;
        if (backupFileExtension != value)
        {
          backupFileExtension = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1070, Constants.PERSIST_BACKUP_IS_INCREMENTAL)]
    public bool BackupIsIncremental
    {
      get => backupIsIncremental;
      set 
      { 
        if (backupIsIncremental != value)
        {
          backupIsIncremental = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1080, Constants.PERSIST_BACKUP_PHYSICAL_DESTINATION_TYPE)]
    public DeviceType BackupPhysicalDestinationType
    {
      get => backupPhysicalDestinationType;
      set 
      { 
        if (backupPhysicalDestinationType != value)
        {
          backupPhysicalDestinationType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1090, Constants.PERSIST_BLOCK_SIZE)]
    public int BlockSize
    {
      get => blockSize;
      set 
      { 
        if (blockSize != value)
        {
          blockSize = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1100, Constants.PERSIST_CHECKSUM)]
    public bool Checksum
    {
      get => checksum;
      set 
      { 
        if (checksum != value)
        {
          checksum = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1110, Constants.PERSIST_CONTAINER_NAME)]
    public string ContainerName
    {
      get => containerName;
      set 
      { 
        value = value ?? string.Empty;
        if (containerName != value)
        {
          containerName = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1120, Constants.PERSIST_CONTINUE_ON_ERROR)]
    public bool ContinueOnError
    {
      get => continueOnError;
      set 
      { 
        if (continueOnError != value)
        {
          continueOnError = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1130, Constants.PERSIST_COPY_ONLY_BACKUP)]
    public bool CopyOnlyBackup
    {
      get => copyOnlyBackup;
      set 
      { 
        if (copyOnlyBackup != value)
        {
          copyOnlyBackup = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1140, Constants.PERSIST_CREATE_SUB_FOLDER)]
    public bool CreateSubFolder
    {
      get => createSubFolder;
      set 
      { 
        if (createSubFolder != value)
        {
          createSubFolder = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1150, Constants.PERSIST_CREDENTIAL_NAME)]
    public string CredentialName
    {
      get => credentialName;
      set 
      { 
        value = value ?? string.Empty;
        if (credentialName != value)
        {
          credentialName = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1170, Constants.PERSIST_DESTINATION_AUTO_FOLDER_PATH)]
    public string DestinationAutoFolderPath
    {
      get => destinationAutoFolderPath;
      set 
      { 
        value = value ?? string.Empty;
        if (destinationAutoFolderPath != value)
        {
          destinationAutoFolderPath = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1180, Constants.PERSIST_DESTINATION_CREATION_TYPE)]
    public DestinationType DestinationCreationType
    {
      get => destinationCreationType;
      set 
      { 
        if (destinationCreationType != value)
        {
          destinationCreationType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1190, Constants.PERSIST_DESTINATION_MANUAL_LIST)]
    public List<string> DestinationManualList
    {
      get => destinationManualList;
      set 
      { 
        if (destinationManualList != value)
        {
          destinationManualList.Clear();
          if (value != null)
          {
            destinationManualList.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1200, Constants.PERSIST_EXISTING_BACKUPS_ACTION)]
    public ActionForExistingBackups ExistingBackupsAction
    {
      get => existingBackupsAction;
      set 
      { 
        if (existingBackupsAction != value)
        {
          existingBackupsAction = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1210, Constants.PERSIST_EXPIRE_DATE)]
    public DateTime ExpireDate
    {
      get => expireDate;
      set 
      { 
        if (expireDate != value)
        {
          expireDate = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1220, Constants.PERSIST_FILE_GROUPS_FILES)]
    public string FileGroupsFiles
    {
      get => fileGroupsFiles;
      set 
      { 
        value = value ?? string.Empty;
        if (fileGroupsFiles != value)
        {
          fileGroupsFiles = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1230, Constants.PERSIST_IGNORE_REPLICA_TYPE)]
    public bool IgnoreReplicaType
    {
      get => ignoreReplicaType;
      set 
      { 
        if (ignoreReplicaType != value)
        {
          ignoreReplicaType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1240, Constants.PERSIST_IN_DAYS)]
    public bool InDays
    {
      get => inDays;
      set 
      { 
        if (inDays != value)
        {
          inDays = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1250, Constants.PERSIST_IS_BACKUP_ENCRYPTED)]
    public bool IsBackupEncrypted
    {
      get => isBackupEncrypted;
      set 
      { 
        if (isBackupEncrypted != value)
        {
          isBackupEncrypted = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1260, Constants.PERSIST_IS_BLOCK_SIZE_USED)]
    public bool IsBlockSizeUsed
    {
      get => isBlockSizeUsed;
      set 
      { 
        if (isBlockSizeUsed != value)
        {
          isBlockSizeUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1270, Constants.PERSIST_IS_MAX_TRANSFER_SIZE_USED)]
    public bool IsMaxTransferSizeUsed
    {
      get => isMaxTransferSizeUsed;
      set 
      { 
        if (isMaxTransferSizeUsed != value)
        {
          isMaxTransferSizeUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1280, Constants.PERSIST_MAX_TRANSFER_SIZE)]
    public int MaxTransferSize
    {
      get => maxTransferSize;
      set 
      { 
        if (maxTransferSize != value)
        {
          maxTransferSize = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1290, Constants.PERSIST_RETAIN_DAYS)]
    public int RetainDays
    {
      get => retainDays;
      set 
      { 
        if (retainDays != value)
        {
          retainDays = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1200, Constants.PERSIST_URL_PREFIX)]
    public string UrlPrefix
    {
      get => urlPrefix;
      set 
      { 
        value = value ?? string.Empty;
        if (urlPrefix != value)
        {
          urlPrefix = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1310, Constants.PERSIST_USE_EXPIRATION)]
    public bool UseExpiration
    {
      get => useExpiration;
      set 
      { 
        if (useExpiration != value)
        {
          useExpiration = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1320, Constants.PERSIST_VERIFY_INTEGRITY)]
    public bool VerifyIntegrity
    {
      get => verifyIntegrity;
      set 
      { 
        if (verifyIntegrity != value)
        {
          verifyIntegrity = value;
          OnPropertyChanged();
        }
      }
    }

  }
}
