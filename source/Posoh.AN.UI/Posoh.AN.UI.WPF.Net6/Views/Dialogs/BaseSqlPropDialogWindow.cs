﻿using System.Windows;
using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs
{
    public class BaseSqlPropDialogWindow : BaseDialogWindow
    {
        protected Grid gridWithProp = new Grid();
        public BaseSqlPropDialogWindow(string title) : base(title)
        {
            gridWithProp = new Grid()
            {
                Height = 150, 
                VerticalAlignment = VerticalAlignment.Top, 
                Margin = new Thickness(0, 0, 0, 0),
                Name = "gridWithProp"
            };
            gridWithProp.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(20, 20, 0, 0),
                Text = "Connection:",
                VerticalAlignment = VerticalAlignment.Top
            });
           
            
            gridWithProp.Children.Add(new ComboBox()
            {
                Margin = new Thickness(130, 17, 75, 0),
                VerticalAlignment = VerticalAlignment.Top
            });
          
            
            gridWithProp.Children.Add(new Button()
            {
                Margin = new Thickness(0, 17, 10, 0),
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Right,
                Content = "New...",
                Width = 60
            });
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var grid = GetTemplateChild("additionalPropGrid") as Grid;
            grid?.Children.Add(gridWithProp);
        }
    }
}