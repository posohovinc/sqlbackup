﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSProtectionLevel
  {
    DontSaveSensitive,
    EncryptSensitiveWithUserKey,
    EncryptSensitiveWithPassword,
    EncryptAllWithPassword,
    EncryptAllWithUserKey,
    ServerStorage
  }
}