﻿using Posoh.AN.UI.VM.Worker;

namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    public abstract class AbstractCheckIntegrityWorkerDialog : BaseWorkerWindow<CheckIntegrityWorkerViewModel>
    {
    }
    
    public abstract class AbstractCleanupWorkerDialog : BaseWorkerWindow<CleanupWorkerViewModel>
    {
    }

    public abstract class AbstractExecuteSQLServerAgentJobWorkerDialog : BaseWorkerWindow<ExecuteSQLServerAgentJobWorkerViewModel>
    {
    }

    public abstract class AbstractExecuteSqlStatementWorkerDialog : BaseWorkerWindow<ExecuteSqlStatementWorkerViewModel>
    {
    }

    public abstract class AbstractHistoryCleanupWorkerDialog : BaseWorkerWindow<HistoryCleanupWorkerViewModel>
    {
    }

    public abstract class AbstractNotifyOperatorWorkerDialog : BaseWorkerWindow<NotifyOperatorWorkerViewModel>
    {
    }

    public abstract class AbstractRebuildIndexWorkerDialog : BaseWorkerWindow<RebuildIndexWorkerViewModel>
    {
    }

    public abstract class AbstractReorganizeIndexWorkerDialog : BaseWorkerWindow<ReorganizeIndexWorkerViewModel>
    {
    }

    public abstract class AbstractShrinkDataBaseWorkerDialog : BaseWorkerWindow<ShrinkDataBaseWorkerViewModel>
    {
    }

}
