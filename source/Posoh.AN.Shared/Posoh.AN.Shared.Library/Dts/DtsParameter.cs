﻿using System;
using System.ComponentModel;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsParameter : DtsObject
  {
    // Properties
    public string CreationName { get; }
    public TypeCode DataType { get; set; }
    public string Description { get; set; }
    public string ID { get; }
    public bool IncludeInDebugDump { get; set; }
    public string Name { get; set; }
    public bool Required { get; set; }
    public bool Sensitive { get; set; }
    //public abstract ISite Site { get; set; }
    public object Value { get; set; }
  }
}