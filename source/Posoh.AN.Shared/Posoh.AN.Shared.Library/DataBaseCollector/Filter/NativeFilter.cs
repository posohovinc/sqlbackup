﻿using System;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{

    [Serializable]
    public class NativeFilter : Filter
    {
        public override bool Decide(string pattern, string value)
        {
            return pattern == value;
        }
    }
}
