using System;
using System.Collections.Generic;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class ShrinkWorker : MaintenanceWorker
  {

    #region fields
    private int databaseSizeLimit = 50;
    private int percentLimit = 10;
    private bool returnFreedSpace = true;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_DATABASE_SIZE_LIMIT)]
    public int DatabaseSizeLimit
    {
      get => databaseSizeLimit;
      set 
      { 
        if (databaseSizeLimit != value)
        {
          databaseSizeLimit = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_PERCENT_LIMIT)]
    public int PercentLimit
    {
      get => percentLimit;
      set 
      { 
        if (percentLimit != value)
        {
          percentLimit = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_RETURN_FREED_SPACE)]
    public bool ReturnFreedSpace
    {
      get => returnFreedSpace;
      set 
      { 
        if (returnFreedSpace != value)
        {
          returnFreedSpace = value;
          OnPropertyChanged();
        }
      }
    }
  }

}
