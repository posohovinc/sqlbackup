﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using Posoh.AN.UI.WPF.Net6.Views.Controls.Simple;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls
{
    public class WorkerControlBase:UserControl
    {
        protected override void OnContentChanged (object oldContent, object newContent)
        {
            base.OnContentChanged (oldContent, newContent);
            var mainGrid = new Grid();
            mainGrid.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(150, GridUnitType.Pixel)});
            mainGrid.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)});
            
            var gridProp = new Grid();
            for (int i = 0; i < 6; i++)
            {
                gridProp.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(25, GridUnitType.Pixel)});
            }

            UIElement elem = new GradientLabel(){Caption = "General options"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 0);

            elem = new LabeledTextBox(){Caption = "Worker name"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 1);
            
            elem = new LabledCombobox(){Caption = "Connection"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 2);
            
            elem = new LabledCombobox(){Caption = "Database(s)"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 3);
            
            elem = new LabledCombobox(){Caption = "Object"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 4);
            
            elem = new LabledCombobox(){Caption = "Selection"};
            gridProp.Children.Add(elem);
            Grid.SetRow(elem, 5);
            mainGrid.Children.Add(gridProp);
            Grid.SetRow(gridProp, 0);

            
            var gridAdditional = new Grid();
            gridAdditional.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(25, GridUnitType.Pixel)});
            gridAdditional.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)});
            
            elem = new GradientLabel(){Caption = "Additional options"};
            gridAdditional.Children.Add(elem);
            Grid.SetRow(elem, 0);
            
            mainGrid.Children.Add(gridAdditional);
            Grid.SetRow(gridAdditional, 1);
            
            ((IAddChild) newContent).AddChild (mainGrid);
        }
    }
}
