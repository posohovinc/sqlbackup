﻿namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsEventsProvider: DtsContainer
  {
    public bool DisableEventHandlers { get; set; }
    public DtsEventHandlers EventHandlers { get; }
    public DtsEventInfos EventInfos { get; }
  }
}