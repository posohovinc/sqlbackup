﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSExecResult
  {
    Success,
    Failure,
    Completion,
    Canceled
  }
}