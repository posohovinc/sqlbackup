﻿using System;
using System.Collections;
using System.Collections.Generic;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter.ObjectConverter
{
  public abstract class SerializeObjectConverter : SerializeConverter
  {
    #region write property
    protected virtual void WriteClass(Serializer sender, string key, object graph, object ancestor)
    {
      var serializer = (Serializer)sender.Clone();
      serializer.SerializeAttributes = sender.SerializeAttributes;
      serializer.Serialize(graph, ancestor);
    }

    protected abstract void WriteDictionary(Serializer sender, IDictionary dictionary, Type typeKey, Type typeValue, ReflectionClient client, object ancestor);

    protected abstract void WriteArray(Serializer sender, IList enumerable, Type typeArg, ReflectionClient client, object ancestor);

    public override bool ValueToString(Serializer sender, out string stringValue, object value, object graph, ReflectionClient client, object ancestor)
    {
      stringValue = string.Empty;
      var result = ancestor != null;
      if (result)
      {
        var type = client.InfoType;
        if (typeof(Element).IsAssignableFrom(type))
        {
          WriteClass(sender, client.Key, value, ancestor);
        }
        else if (type.IsGenericType)
        {
          var typeArgs = type.GetGenericArguments();
          switch (typeArgs.Length)
          {
            case 1:
              WriteArray(sender, value as IList, typeArgs[0], client, ancestor);
              break;
            case 2:
              WriteDictionary(sender, value as IDictionary, typeArgs[0], typeArgs[1], client, ancestor);
              break;
          }
        }
        else if (type.IsArray)
        {
          var itemType = type.GetElementType();
          WriteArray(sender, value as IList, itemType, client, ancestor);
        }
      }

      return !string.IsNullOrEmpty(stringValue);
    }
    #endregion

    #region read property
    protected virtual void ReadClass(Serializer sender, string key, object graph, object ancestor)
    {
      var serializer = (Serializer)sender.Clone();
      serializer.SerializeAttributes = sender.SerializeAttributes;
      serializer.Deserialize(graph, ancestor);
    }
    protected abstract void ReadArray(Serializer sender, IList value, Type typeArg, ReflectionClient client, object ancestor);

    protected abstract void ReadDictionary(Serializer sender, IDictionary value, Type typeArg, Type type, ReflectionClient client, object ancestor);

    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      var result = ancestor != null;
      if (result)
      {
        var type = client.InfoType;
        if (typeof(Element).IsAssignableFrom(type))
        {
          ReadClass(sender, client.Key, value, ancestor);
        }
        else if (type.IsGenericType)
        {
          var typeArgs = type.GetGenericArguments();
          switch (typeArgs.Length)
          {
            case 1:
              ReadArray(sender, value as IList, typeArgs[0], client, ancestor);
              break;
            case 2:
              ReadDictionary(sender, value as IDictionary, typeArgs[0], typeArgs[1], client, ancestor);
              break;
          }
        }
        else if (type.IsArray)
        {
          var list = new ArrayList();
          var itemType = type.GetElementType();
          ReadArray(sender, list, itemType, client, ancestor);
          value = list.ToArray(itemType);
        }
      }

      return true;
    }

    #endregion
  }

}
