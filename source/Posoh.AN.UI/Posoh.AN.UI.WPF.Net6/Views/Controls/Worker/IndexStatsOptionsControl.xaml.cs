﻿using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Worker
{
    /// <summary>
    /// Interaction logic for IndexStatsOptionsControl.xaml
    /// </summary>
    public partial class IndexStatsOptionsControl : UserControl
    {
        public IndexStatsOptionsControl()
        {
            InitializeComponent();
        }
    }
}
