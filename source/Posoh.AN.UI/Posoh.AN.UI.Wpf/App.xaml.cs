﻿using System.Windows;
using Microsoft.EntityFrameworkCore;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.UI.Wpf
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    public App()
    {
      this.InitializeComponent();
 
      using (var db = new MsSqlConnectionContext())
      {
        db.Database.Migrate();
      }
    }
  }
}
