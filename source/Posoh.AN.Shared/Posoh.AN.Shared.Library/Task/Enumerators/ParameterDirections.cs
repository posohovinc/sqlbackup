﻿using System;

namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  [Flags]
  public enum ParameterDirections
  {
    Input = 1,
    Output = 2,
    ReturnValue = 4
  }
}