﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Serializer;
using Posoh.AN.Shared.Library.Task.Enumerators;
using Posoh.AN.Shared.Library.Task.Workers;
using Posoh.AN.Shared.Library.Misc;

namespace Shared.Test.WorkerTest
{
  [TestClass]
  public class ElementWorkerTest
  {
    private XmlNode GetObjectData(string className, string key = "CreationName")
    {
      XmlDocument doc = new XmlDocument();
      doc.Load(@"..\..\..\DataSource\2019\native-2019.xml");
      XmlNode root = doc.DocumentElement;
      XmlNamespaceManager namespaceManager = new XmlNamespaceManager(doc.NameTable);
      namespaceManager.AddNamespace(Constants.PERSIST_PREFIX, Constants.PERSIST_NAMESPACE);
      namespaceManager.AddNamespace(Constants.PERSIST_PREFIX_DTS, Constants.PERSIST_NAMESPACE_DTS);
      return
        root.SelectSingleNode
        (
          string.Format
          (
            @"/DTS:Executable/DTS:Executables/DTS:Executable[@DTS:CreationName=""STOCK:SEQUENCE""]/DTS:Executables/DTS:Executable[@ DTS:{0}=""{1}""]/DTS:ObjectData/SQLTask:SqlTaskData",
            key, className
          ),
          namespaceManager
        );
    }

    private string ElementWorkerXmlResult = @"<?xml version=""1.0"" encoding=""utf-8""?>
<SQLTask:SqlTaskData SQLTask:IsStoredProcedure=""True"" xmlns:SQLTask=""www.microsoft.com/sqlserver/dts/tasks/sqltask"" />";

    [TestMethod]
    public void WorkerTest()
    {
      var graph = new ElementWorker();
      graph.IsStoredProcedure = true;
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX, NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Serialize(graph, out string result);
      Assert.IsTrue(result.Equals(ElementWorkerXmlResult));
    }

    private const string HistoryCleanuXmlResult = @"<?xml version=""1.0"" encoding=""utf-8""?>
<SQLTask:SqlTaskData SQLTask:DatabaseSelectionType=""1"" SQLTask:ServerVersion=""0"" SQLTask:ExtendedLogging=""False"" SQLTask:IgnoreDatabasesInNotOnlineState=""False"" SQLTask:RemoveOlderThan=""4"" SQLTask:TimeUnitsType=""1"" SQLTask:RemoveBackupRestorHistory=""True"" SQLTask:RemoveDbMaintHistory=""True"" SQLTask:RemoveSqlAgentHistory=""True"" xmlns:SQLTask=""www.microsoft.com/sqlserver/dts/tasks/sqltask"" />";

    [TestMethod]
    public void HistoryCleanupWorkerTest()
    {

      var graph = new HistoryCleanupWorker
      {
        RemoveBackupRestoreHistory = true,
        RemoveDbMaintHistory = true,
        RemoveSqlAgentHistory = true,
        OlderThanTimeUnitType = TimeUnitType.Week,
        OlderThanTimeUnits = 4
      };
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Serialize(graph, out string result);
      Assert.IsTrue(result.Equals(HistoryCleanuXmlResult));
    }

    [TestMethod]
    public void XmlTest()
    {
      Assert.IsTrue(GetObjectData("Microsoft.DbMaintenanceUpdateStatisticsTask") != null);
    }

    [TestMethod]
    public void XmlCheckIntegrityWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceCheckIntegrityTask");
      Assert.IsTrue(node != null);
      var graph = new CheckIntegrityWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 15);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");

      //Assert.IsTrue(graph.TaskName == ""); ???

      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.IncludeIndexes == true);
      Assert.IsTrue(graph.PhysicalOnly == true);
      Assert.IsTrue(graph.Tablock == true);
      Assert.IsTrue(graph.MaximumDegreeOfParallelismUsed == true);
      Assert.IsTrue(graph.MaximumDegreeOfParallelism == 5);
      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWConfiguration");
    }

    [TestMethod]
    public void XmlExecuteAgentJobWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceExecuteAgentJobTask");
      Assert.IsTrue(node != null);
      var graph = new ExecuteAgentJobWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");

      //Assert.IsTrue(graph.TaskName == ""); ???

      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.AgentJobId == "syspolicy_purge_history");
      Assert.IsTrue(graph.SelectedDatabases.Count == 0);

    }


    [TestMethod]
    public void XmlFileCleanupWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceFileCleanupTask");
      Assert.IsTrue(node != null);
      var graph = new FileCleanupWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");

      //Assert.IsTrue(graph.TaskName == ""); ???

      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.FileTypeSelected == FileType.FileBackup);
      Assert.IsTrue(graph.FilePath == "");
      Assert.IsTrue(graph.CleanSubFolders == true);
      Assert.IsTrue(graph.FileExtension == "bak");
      Assert.IsTrue(graph.AgeBased == true);
      Assert.IsTrue(graph.DeleteSpecificFile == false);
      Assert.IsTrue(graph.OlderThanTimeUnits == 7);
      Assert.IsTrue(graph.OlderThanTimeUnitType == TimeUnitType.Day);
      Assert.IsTrue(graph.SelectedDatabases.Count == 0);
    }

    [TestMethod]
    public void XmlHistoryCleanupWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceFileCleanupTask");
      Assert.IsTrue(node != null);
      var graph = new HistoryCleanupWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");

      //Assert.IsTrue(graph.TaskName == ""); ???

      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.RemoveBackupRestoreHistory == true);
      Assert.IsTrue(graph.RemoveSqlAgentHistory == true);
      Assert.IsTrue(graph.RemoveDbMaintHistory == true);

      Assert.IsTrue(graph.OlderThanTimeUnits == 7);
      Assert.IsTrue(graph.OlderThanTimeUnitType == TimeUnitType.Day);
      Assert.IsTrue(graph.SelectedDatabases.Count == 0);

    }

    [TestMethod]
    public void XmlNotifyOperatorWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceNotifyOperatorTask");
      Assert.IsTrue(node != null);
      var graph = new NotifyOperatorWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.ServerVersion == 15);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");

      //Assert.IsTrue(graph.TaskName == ""); ???

      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.Message == "test");
      Assert.IsTrue(graph.Profile == "DefaultProfile");
      Assert.IsTrue(graph.Subject == "subjet");
      Assert.IsTrue(graph.OperatorNotify.Count == 1);
      Assert.IsTrue(graph.OperatorNotify[0] == "aposohov");
      Assert.IsTrue(graph.SelectedDatabases.Count == 0);
    }

    [TestMethod]
    public void XmlReindexWorkerTest()
    {
      var node = GetObjectData("{0E61AF3A-8CB4-433D-9650-1846693C2986}", "DTSID");
      Assert.IsTrue(node != null);
      var graph = new ReindexWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 15);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);

      Assert.IsTrue(graph.ReindexWithOriginalAmount == false);
      Assert.IsTrue(graph.ReindexPercentage == 30);

      Assert.IsTrue(graph.AdvSortInTempdb == true);
      Assert.IsTrue(graph.AdvKeepOnline == true);
      Assert.IsTrue(graph.AdvOfflineUnsupportedIndex == true);
      Assert.IsTrue(graph.PadIndex == true);

      Assert.IsTrue(graph.MaximumDegreeOfParallelismUsed == true);
      Assert.IsTrue(graph.MaximumDegreeOfParallelism == 5);
      Assert.IsTrue(graph.LowPriorityUsed == false);

      Assert.IsTrue(graph.IndexStats.FragmentationPctUsed == true);
      Assert.IsTrue(graph.IndexStats.PageCountUsed == true);
      Assert.IsTrue(graph.IndexStats.FragmentationOp == FragmentationOption.Sampled);
      Assert.IsTrue(graph.IndexStats.FragmentationPct == 40);
      Assert.IsTrue(graph.IndexStats.PageCountUsed == true);
      Assert.IsTrue(graph.IndexStats.PageCount == 900);
      Assert.IsTrue(graph.IndexStats.LastUsedInDaysUsed == true);
      Assert.IsTrue(graph.IndexStats.LastUsedInDays == 9);


      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWConfiguration");
    }

    [TestMethod]
    public void XmlReorganizeIndexWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceDefragmentIndexTask");
      Assert.IsTrue(node != null);
      var graph = new ReorganizeIndexWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 15);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);

      Assert.IsTrue(graph.CompactLargeObjects == true);

      Assert.IsTrue(graph.IndexStats.FragmentationPctUsed == true);
      Assert.IsTrue(graph.IndexStats.PageCountUsed == true);
      Assert.IsTrue(graph.IndexStats.FragmentationOp == FragmentationOption.Sampled);
      Assert.IsTrue(graph.IndexStats.FragmentationPct == 30);
      Assert.IsTrue(graph.IndexStats.PageCountUsed == true);
      Assert.IsTrue(graph.IndexStats.PageCount == 900);
      Assert.IsTrue(graph.IndexStats.LastUsedInDaysUsed == true);
      Assert.IsTrue(graph.IndexStats.LastUsedInDays == 9.0);


      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWConfiguration");
    }

    [TestMethod]
    public void XmlShrinkWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceShrinkTask");
      Assert.IsTrue(node != null);
      var graph = new ShrinkWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);

      Assert.IsTrue(graph.DatabaseSizeLimit == 40);
      Assert.IsTrue(graph.PercentLimit == 25);
      Assert.IsTrue(graph.ReturnFreedSpace == true);


      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWConfiguration");
    }

    [TestMethod]
    public void XmlTSQLExecuteWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceTSQLExecuteTask");
      Assert.IsTrue(node != null);
      var graph = new TSQLExecuteWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.All);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.SqlStatementSource == "select 0");

      Assert.IsTrue(graph.SelectedDatabases.Count == 0);
    }

    [TestMethod]
    public void XmlUpdateStatisticsWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceUpdateStatisticsTask");
      Assert.IsTrue(node != null);
      var graph = new UpdateStatisticsWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 9);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);
      Assert.IsTrue(graph.UpdateType == StatisticsTarget.Column);
      Assert.IsTrue(graph.UpdateSampleValue == 50);

      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWDiagnostics");
    }

    [TestMethod]
    public void XmlBackupWorkerTest()
    {
      var node = GetObjectData("Microsoft.DbMaintenanceBackupTask");
      Assert.IsTrue(node != null);
      var graph = new BackupWorker();
      var serializer = new SerializerXml
      {
        Prefix = Constants.PERSIST_PREFIX,
        NamespaceURI = Constants.PERSIST_NAMESPACE
      };
      serializer.Deserialize(graph, node);
      Assert.IsTrue(graph.DatabaseSelectionType == DatabaseSelection.Specific);
      Assert.IsTrue(graph.ServerVersion == 15);
      Assert.IsTrue(graph.ExtendedLogging == false);
      Assert.IsTrue(graph.LocalConnectionForLogging == "Local server connection");
      //Assert.IsTrue(graph.TaskName == ""); ???
      Assert.IsTrue(graph.IgnoreDatabasesInNotOnlineState == false);

      Assert.IsTrue(graph.BackupAction == BackupActionType.Database);
      Assert.IsTrue(graph.BackupIsIncremental == false);
      Assert.IsTrue(graph.FileGroupsFiles == "");
      Assert.IsTrue(graph.BackupDeviceType == DeviceType.File);
      Assert.IsTrue(graph.BackupPhysicalDestinationType == DeviceType.File);
      Assert.IsTrue(graph.DestinationCreationType == DestinationType.Auto);
      Assert.IsTrue(graph.DestinationAutoFolderPath == @"c:\backup");
      Assert.IsTrue(graph.ExistingBackupsAction == ActionForExistingBackups.Append);
      Assert.IsTrue(graph.CreateSubFolder == true);
      Assert.IsTrue(graph.BackupFileExtension == "");
      Assert.IsTrue(graph.VerifyIntegrity == true);
      Assert.IsTrue(graph.ExpireDate == DateTime.Parse("2020-10-15T00:00:00"));
      Assert.IsTrue(graph.RetainDays == 6);
      Assert.IsTrue(graph.InDays == true);
      Assert.IsTrue(graph.UseExpiration == true);
      Assert.IsTrue(graph.BackupCompressionOption == BackupCompressionOptions.Default);
      Assert.IsTrue(graph.CopyOnlyBackup == false);
      Assert.IsTrue(graph.IgnoreReplicaType == false);
      Assert.IsTrue(graph.CredentialName == "");
      Assert.IsTrue(graph.ContainerName == "");
      Assert.IsTrue(graph.UrlPrefix == "");
      Assert.IsTrue(graph.IsBackupEncrypted == false);
      Assert.IsTrue(graph.BackupEncryptionAlgorithm == BackupEncryptionAlgorithm.Aes128);
      Assert.IsTrue(graph.BackupEncryptorType == BackupEncryptorType.ServerCertificate);
      Assert.IsTrue(graph.BackupEncryptorName == "");
      Assert.IsTrue(graph.Checksum == true);
      Assert.IsTrue(graph.ContinueOnError == true);
      Assert.IsTrue(graph.IsBlockSizeUsed == true);
      Assert.IsTrue(graph.BlockSize == 65536);
      Assert.IsTrue(graph.IsMaxTransferSizeUsed == true);
      Assert.IsTrue(graph.MaxTransferSize == 65536);

      Assert.IsTrue(graph.SelectedDatabases.Count == 1);
      Assert.IsTrue(graph.SelectedDatabases[0] == "DWConfiguration");
    }
  }

}
