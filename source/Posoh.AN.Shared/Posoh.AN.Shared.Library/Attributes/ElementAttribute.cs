﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Posoh.AN.Shared.Library.Attributes
{

  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
  public class ElementAttribute : Attribute, IOrdered
  {
    public ElementAttribute() : this(int.MaxValue, string.Empty)
    {
    }
    public ElementAttribute(string key) : this(int.MaxValue, key)
    {
    }
    public ElementAttribute(int order) : this(order, string.Empty)
    {
    }
    public ElementAttribute(int order, string key)
    {
      Order = order;
      Key = key;
    }

    public override bool Equals(object? obj) => CompareTo(obj) == 0;

    public override string ToString()
    {
      return Key;
    }

    public override int GetHashCode()
    {
      return Order ^ Key.GetHashCode();
    }

    public string Key { get; }
    public int Order { get; }

    public int CompareTo(object? obj)
    {
      var result = 1;
      if (obj is IOrdered ordered)
      {
        result = Order - ordered.Order;
        if (result == 0)
          result = String.Compare(Key, ordered.Key, StringComparison.Ordinal);
      }

      return result;
    }
  }
}
