using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class HistoryCleanupWorker : MaintenanceWorker
  {

    #region fields
    private int olderThanTimeUnits = 4;
    private TimeUnitType olderThanTimeUnitType = TimeUnitType.Week;
    private bool _removeBackupRestoreHistory = true;
    private bool removeDbMaintHistory = true;
    private bool removeSqlAgentHistory = true;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_OLDER_THAN_TIME_UNITS)]
    public int OlderThanTimeUnits
    {
      get => olderThanTimeUnits;
      set 
      { 
        if (olderThanTimeUnits != value)
        {
          olderThanTimeUnits = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_OLDER_THAN_TIME_UNIT_TYPE)]
    public TimeUnitType OlderThanTimeUnitType
    {
      get => olderThanTimeUnitType;
      set 
      { 
        if (olderThanTimeUnitType != value)
        {
          olderThanTimeUnitType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_REMOVE_BACKUP_RESTOR_HISTORY)]
    public bool RemoveBackupRestoreHistory
    {
      get => _removeBackupRestoreHistory;
      set 
      { 
        if (_removeBackupRestoreHistory != value)
        {
          _removeBackupRestoreHistory = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1030, Constants.PERSIST_REMOVE_DB_MAINT_HISTORY)]
    public bool RemoveDbMaintHistory
    {
      get => removeDbMaintHistory;
      set 
      { 
        if (removeDbMaintHistory != value)
        {
          removeDbMaintHistory = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1040, Constants.PERSIST_REMOVE_SQL_AGENT_HISTORY)]
    public bool RemoveSqlAgentHistory
    {
      get => removeSqlAgentHistory;
      set 
      { 
        if (removeSqlAgentHistory != value)
        {
          removeSqlAgentHistory = value;
          OnPropertyChanged();
        }
      }
    }

    //[XmlOrderedAttribute(1050, Constants.PERSIST_TASK_ALLOWES_DATBASE_SELECTION)]
    public override bool TaskAllowesDatbaseSelection => false;
    //[XmlOrderedAttribute(1060, Constants.PERSIST_TASK_ALLOWES_TABLE_SELECTION)]
    public override bool TaskAllowesTableSelection => false;
  }
}
