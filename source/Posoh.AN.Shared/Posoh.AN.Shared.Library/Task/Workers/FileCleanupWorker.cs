using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class FileCleanupWorker : MaintenanceWorker
  {

    #region fields
    private bool ageBased = true;
    private bool cleanSubFolders;
    private bool deleteSpecificFile;
    private string fileExtension = string.Empty;
    private string filePath = string.Empty;
    private FileType fileTypeSelected = FileType.FileBackup;
    private string folderPath = string.Empty;
    private double olderThanTimeUnits = 4;
    private TimeUnitType olderThanTimeUnitType = TimeUnitType.Week;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_AGE_BASED)]
    public bool AgeBased
    {
      get => ageBased;
      set 
      { 
        if (ageBased != value)
        {
          ageBased = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_CLEAN_SUB_FOLDERS)]
    public bool CleanSubFolders
    {
      get => cleanSubFolders;
      set 
      { 
        if (cleanSubFolders != value)
        {
          cleanSubFolders = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_DELETE_SPECIFIC_FILE)]
    public bool DeleteSpecificFile
    {
      get => deleteSpecificFile;
      set 
      { 
        if (deleteSpecificFile != value)
        {
          deleteSpecificFile = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1030, Constants.PERSIST_FILE_EXTENSION)]
    public string FileExtension
    {
      get => fileExtension;
      set 
      { 
        value ??= string.Empty;
        if (fileExtension != value)
        {
          fileExtension = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1040, Constants.PERSIST_FILE_PATH)]
    public string FilePath
    {
      get => filePath;
      set 
      { 
        value ??= string.Empty;
        if (filePath != value)
        {
          filePath = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1050, Constants.PERSIST_FILE_TYPE_SELECTED)]
    public FileType FileTypeSelected
    {
      get => fileTypeSelected;
      set 
      { 
        if (fileTypeSelected != value)
        {
          fileTypeSelected = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1060, Constants.PERSIST_FOLDER_PATH)]
    public string FolderPath
    {
      get => folderPath;
      set 
      { 
        value ??= string.Empty;
        if (folderPath != value)
        {
          folderPath = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1070, Constants.PERSIST_OLDER_THAN_TIME_UNITS)]
    public double OlderThanTimeUnits
    {
      get => olderThanTimeUnits;
      set 
      { 
        if (olderThanTimeUnits != value)
        {
          olderThanTimeUnits = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1080, Constants.PERSIST_OLDER_THAN_TIME_UNIT_TYPE)]
    public TimeUnitType OlderThanTimeUnitType
    {
      get => olderThanTimeUnitType;
      set 
      { 
        if (olderThanTimeUnitType != value)
        {
          olderThanTimeUnitType = value;
          OnPropertyChanged();
        }
      }
    }

    //[XmlOrderedAttribute(1090, Constants.PERSIST_TASK_ALLOWES_DATBASE_SELECTION)]
    public override bool TaskAllowesDatbaseSelection => false;
    //[XmlOrderedAttribute(1100, Constants.PERSIST_TASK_ALLOWES_TABLE_SELECTION)]
    public override bool TaskAllowesTableSelection => false;
  }
}
