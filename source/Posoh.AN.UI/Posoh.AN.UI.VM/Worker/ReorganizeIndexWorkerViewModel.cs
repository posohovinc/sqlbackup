﻿using System.Collections.Generic;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.Shared.Library.Task.Workers;

namespace Posoh.AN.UI.VM.Worker
{
    public class ReorganizeIndexWorkerViewModel: WorkerViewModelBase<ReorganizeIndexWorker>
    {
        public ReorganizeIndexWorkerViewModel(IList<CoreConnection> connections) : base(connections)
        {
        }
    }
}
