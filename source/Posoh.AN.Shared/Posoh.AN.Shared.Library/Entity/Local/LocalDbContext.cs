﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Posoh.AN.Shared.Library.Entity.Local
{
    public class LocalDbContext : DbContext
    {
        public DbSet<CoreConnection> ConnectionsToBase { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<DbCollector> DbCollectors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            
            if (OperatingSystem.IsMacOS())
                appdata = appdata.Replace("/.config", "");

            appdata = Path.Combine(appdata, "SqlBackup");
            if (!Directory.Exists(appdata))
                Directory.CreateDirectory(appdata);
            
            options.UseSqlite($@"Data Source={Path.Combine(appdata, "backup.db")}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<DbCollector>(builder =>
                {
                    builder.HasNoKey();
                    builder.ToTable("DbCollector");
                });
        }
    }
}