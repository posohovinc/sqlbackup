﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Posoh.AN.Shared.Library.DataBaseCollector.Objects;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.Shared.Library.DataBaseCollector
{
    public class DownloadDbInfoWorker
    {
        public DownloadDbInfoWorker(ServerElement serverElement)
        {
            ServerElement = serverElement;
        }

        private ServerElement ServerElement { get; set; }

        public void DoDownload()
        {
            var connection = ServerElement.Connection;
            var mainfolder = Path.Combine(ServerElement.Git, ServerElement.Path, ServerElement.Connection.Name);

            var dataBases = connection.QueryHelper.GetDatabases();
            var dataBasesAfterFilters = ApplyFiltersOnCoreObjects(dataBases.Select(x => x as BaseDbObject).ToList());

            foreach (var db in dataBasesAfterFilters)
            {
                connection.Database = db.Name;
                var dbDataObjects = connection.QueryHelper.GetDbDataObjects(db.Name);
                var dbDataObjectsFiltered = ApplyFiltersOnCoreObjects(dbDataObjects.Select(x => x as BaseDbObject).ToList()).Select(x => x as DbObject).ToList();
                SaveDbObjectsInFolder(dbDataObjectsFiltered, Path.Combine(Path.Combine(mainfolder, "Databases"), db.Name), connection);
            }

            var serverObjects = connection.QueryHelper.GetDbObjects();
            var serverObjectsFiltered = ApplyFiltersOnCoreObjects(serverObjects.Select(x => x as BaseDbObject).ToList()).Select(x => x as ServerObject).ToList();
            SaveServerObjectsInFolder(serverObjectsFiltered, mainfolder, connection);
        }


        private void SaveServerObjectsInFolder(List<ServerObject> workObjects, string mainFolder, CoreConnection connection)
        {
            foreach (var job in workObjects)
            {
                if (string.IsNullOrEmpty(job.Folder) || string.IsNullOrEmpty(job.File))
                    continue;

                var folderPath = mainFolder + "\\" + job.Folder;
                var objs = connection.QueryHelper.GetDbObjectString(job);

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                foreach (var o in objs)
                {
                    SaveObjectDefenition(o, folderPath);
                }

            }
        }

        private void SaveDbObjectsInFolder(List<DbObject> objects, string mainFolder, CoreConnection connection)
        {
            foreach (var obj in objects)
            {
                if (string.IsNullOrEmpty(obj.Folder) || string.IsNullOrEmpty(obj.File))
                    continue;

                var folderPath = Path.Combine(mainFolder, obj.Folder);
                var objs = connection.QueryHelper.GetDataObjectString(obj);

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                foreach (var o in objs)
                {
                    SaveObjectDefenition(o, folderPath);
                }
            }
        }

        private void SaveObjectDefenition(ObjectDefinition obj, string mainFolder)
        {
            if (string.IsNullOrEmpty(obj.FileName))
                return;

            var filePath = Path.Combine(mainFolder, obj.FileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            if (!string.IsNullOrEmpty(obj.FileName) && !string.IsNullOrEmpty(obj.Definition))
            {
                using (StreamWriter sw = File.CreateText(filePath))
                {
                    sw.Write(obj.Definition);
                }
            }
        }

        private List<BaseDbObject> ApplyFiltersOnCoreObjects(List<BaseDbObject> objectsForFilters)
        {
            var list = new List<BaseDbObject>();
            foreach (var obj in objectsForFilters)
            {
                if (ServerElement.ApplyFilters(obj))
                    list.Add(obj);
            }

            return list;
        }
    }
}
