﻿using System.Drawing;
using System.Text.RegularExpressions;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializePointConverter: SerializeConverter
  {
    protected static bool TryParse(out int first, out int second, string value)
    {
      var regexPointSize = new Regex(@"{\w+=(?<first>\d*)\s?,\s?\w+=(?<second>\d*)}");
      var m = regexPointSize.Match(value);
      var result = m.Success;
      if (result)
      {
        first = int.Parse(m.Result("${first}"));
        second = int.Parse(m.Result("${second}"));
      }
      else
      {
        first = -1;
        second = -1;
      }
      return result;
    }
    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      value = null;
      if (TryParse(out var first, out var second, stringValue))
      {
        value = new Point(first, second);
      }

      return value != null;
    }

  }
}
