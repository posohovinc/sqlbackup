﻿select
  id               as object_id
 ,'StoredPackages' as [Folder]
 ,name + '.dtsx'   as [File]
 ,name             as [Name]
from
  msdb..sysssispackages
where
  [folderid] = '00000000-0000-0000-0000-000000000000'
order by
  name
