﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Posoh.AN.Shared.Library.Core
{
  [AttributeUsage(AttributeTargets.Field)]
  public sealed class NotPersistentAttribute : Attribute
  {
  }

  [Serializable]
  public abstract class Persistent : Disposable
  {
    #region private field
    [NotPersistent]
    [NonSerialized]
    private static readonly Dictionary<Type, List<ReflectionClient>> persistentFields =
      new Dictionary<Type, List<ReflectionClient>>();
    [NotPersistent]
    //[NonSerialized]
    private readonly Stack<byte[]> stateStack = new Stack<byte[]>();

    private const BindingFlags bindingFlags =
      BindingFlags.NonPublic |
      BindingFlags.Instance |
      BindingFlags.Public;

    #endregion
    #region private static

    private static string Key(string typeName, string memberName)
    {
      return typeName + "." + memberName;
    }

    private static List<ReflectionClient> GetPersistentField(Type type)
    {
      if (!persistentFields.TryGetValue(type, out List<ReflectionClient> result))
      {
        lock (persistentFields)
        {
          if (!persistentFields.TryGetValue(type, out result))
          {
            result = new List<ReflectionClient>();
            var fields = type.GetFields(bindingFlags);
            foreach (var field in fields)
            {
              if (field.DeclaringType == type)
              {
                if (!Attribute.IsDefined(field, typeof (NotPersistentAttribute)))
                  result.Add(new ReflectionClient(field, null));
              }
            }
            persistentFields.Add(type, result);
          }
        }
      }
      return result;
    }

    #endregion

    protected virtual void SaveState()
    {
      var state = new HybridDictionary();
      try
      {
        var currentType = GetType();
        do
        {
          var currentTypeName = currentType.FullName;
          foreach (var info in GetPersistentField(currentType))
          {
            var key = Key(currentTypeName, info.Key);

            info.GetValue(this, out object value);
            if (typeof (Persistent).IsAssignableFrom(info.InfoType))
            {
              if (value == null)
              {
                state.Add(key, null);
              }
              else
              {
                ((Persistent) value).SaveState();
              }
            }
            else
            {
              state.Add(key, value);
            }
          }
          currentType = currentType.BaseType;
        } 
        while (currentType != typeof (Persistent));
        
        using (var buffer = new MemoryStream())
        {
          var formatter = new BinaryFormatter();
          formatter.Serialize(buffer, state);
          stateStack.Push(buffer.ToArray());
        }
      }
      catch (Exception e)
      {
        throw;
      }
      //stateStack.Push(state);
    }

    protected virtual void AcceptChanges()
    {
      try
      {
        stateStack.Pop();
        var currentType = GetType();
        do
        {
          foreach (var info in GetPersistentField(currentType))
          {
            if (typeof(Persistent).IsAssignableFrom(info.InfoType))
            {
              info.GetValue(this, out object value);
              ((Persistent) value)?.AcceptChanges();
            }
          }
          currentType = currentType.BaseType;
        } 
        while (currentType != typeof(Persistent));
      }
      catch (Exception e)
      {
        throw;
      }
    }

    protected virtual void UndoChanges()
    {
      try
      {
        HybridDictionary state;
        using (var buffer = new MemoryStream(stateStack.Pop()))
        {
          buffer.Position = 0;
          var formatter = new BinaryFormatter();
          state = (HybridDictionary)formatter.Deserialize(buffer);
        }

        var currentType = GetType();
        do
        {
          var currentTypeName = currentType.FullName;
          foreach (var info in GetPersistentField(currentType))
          {
            info.GetValue(this, out object value);
            var key = Key(currentTypeName, info.Key);
            if (typeof(Persistent).IsAssignableFrom(info.InfoType))
            {
              if (state.Contains(key))
              {
                info.SetValue(this, null);
              }
              else
              {
                ((Persistent) value)?.UndoChanges();
              }
            }
            else
            {
              info.SetValue(this, state[key]);
            }
          }
          currentType = currentType.BaseType;
        } 
        while (currentType != typeof(Persistent));
      }
      catch (Exception e)
      {
        throw;
      }
    }

    protected override void Free()
    {
      base.Free();
      var currentType = GetType();
      do
      {
        foreach (var info in GetPersistentField(currentType))
        {
          if (typeof(Persistent).IsAssignableFrom(info.InfoType))
          {
            info.GetValue(this, out object value);
            ((Persistent) value)?.Dispose();
          }
        }
        currentType = currentType.BaseType;
      } 
      while (currentType != typeof(Persistent));
      stateStack.Clear();
    }
  }
}
