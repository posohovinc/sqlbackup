﻿using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for LabledLine.xaml
    /// </summary>
    public partial class LabledLine : UserControl
    {
        public LabledLine()
        {
            InitializeComponent();
        }

        public string Caption { get => tblCaption.Text; set => tblCaption.Text = value; }
    }
}
