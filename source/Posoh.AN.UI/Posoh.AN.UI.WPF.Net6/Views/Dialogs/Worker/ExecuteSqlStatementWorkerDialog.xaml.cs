﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for ExecuteSqlStatementWorkerDialog.xaml
    /// </summary>
    public partial class ExecuteSqlStatementWorkerDialog : AbstractExecuteSqlStatementWorkerDialog
    {
        public ExecuteSqlStatementWorkerDialog() 
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            ExecuteSqlStatementWorkerControl.Self = Vm;
        }
    }
}
