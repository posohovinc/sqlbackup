﻿using System.Collections.Generic;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.Shared.Library.Task.Workers;

namespace Posoh.AN.UI.VM.Worker
{
    public class NotifyOperatorWorkerViewModel : WorkerViewModelBase<NotifyOperatorWorker>
    {
        public NotifyOperatorWorkerViewModel(IList<CoreConnection> connections) : base(connections)
        {
        }
    }
}
