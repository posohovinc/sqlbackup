﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum BackupActionType
  {
    Database,
    Files,
    Log
  }
}