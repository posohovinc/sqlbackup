﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum StatisticsTarget
  {
    Index = 0,
    Column = 1,
    All = 2
  }
}