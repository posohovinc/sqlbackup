﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  class SerializeDateTimeConverter: SerializeConverter
  {
    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      if (!DateTime.TryParse(stringValue, out var propertyValue))
      {
        if (!DateTime.TryParse(stringValue, CultureInfo.InvariantCulture, DateTimeStyles.None, out propertyValue))
        {
          propertyValue = DateTime.MinValue;
        }
      }

      value = propertyValue;
      return true;
    }
  }
}
