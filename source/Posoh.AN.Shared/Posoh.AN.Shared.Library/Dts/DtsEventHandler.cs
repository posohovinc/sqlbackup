﻿namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsEventHandler : DtsContainer
  {
    public DtsExecutables Executables { get; }
    public bool HasExpressions { get; }
    public DtsPrecedenceConstraints PrecedenceConstraints { get; }
    public DtsProperties Properties { get; }
  }
}