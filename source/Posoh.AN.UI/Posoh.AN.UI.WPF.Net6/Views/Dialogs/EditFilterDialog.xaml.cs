﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditFilterDialog.xaml
    /// </summary>
    public partial class EditFilterDialog : BaseDialogWindow
    {
        public EditFilterDialog():base("Filter")
        {
            InitializeComponent();
        }
    }
}
