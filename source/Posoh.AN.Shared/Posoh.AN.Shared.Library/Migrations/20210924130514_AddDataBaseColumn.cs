﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Posoh.AN.Shared.Library.Migrations
{
    public partial class AddDataBaseColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Database",
                table: "ConnectionsToBase",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Database",
                table: "ConnectionsToBase");
        }
    }
}
