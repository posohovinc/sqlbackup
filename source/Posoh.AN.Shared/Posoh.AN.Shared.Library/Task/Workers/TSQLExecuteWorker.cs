using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class TSQLExecuteWorker : MaintenanceWorker
  {

    #region fields
    #endregion

    //[XmlOrderedAttribute(1020, Constants.PERSIST_TASK_ALLOWES_DATBASE_SELECTION)]
    public override bool TaskAllowesDatbaseSelection => false;
    //[XmlOrderedAttribute(1020, Constants.PERSIST_TASK_ALLOWES_TABLE_SELECTION)]
    public override bool TaskAllowesTableSelection => false;
  }
}
