﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum FileType
  {
    FileBackup,
    FileReport
  }
}