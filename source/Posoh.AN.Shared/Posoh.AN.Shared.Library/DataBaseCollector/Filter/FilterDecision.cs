﻿namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    public enum FilterDecision
    {
        Accept,
        Dined,
        Neutral
    }
}
