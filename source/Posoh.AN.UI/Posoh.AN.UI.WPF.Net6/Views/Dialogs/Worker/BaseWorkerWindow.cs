﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    public class BaseWorkerWindow<T>: BaseWorkerDialog
    {
        private T vm;

        public T Vm
        {
            get => vm;
            set
            {
                if (!ReferenceEquals(vm, value))
                {
                    vm = value;
                    if(value != null)
                        OnInitialized();
                }
            }
        }

        protected virtual void OnInitialized()
        {
        }
    }
}
