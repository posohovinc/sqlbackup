﻿using System.Windows;
using Posoh.AN.UI.Wpf.Views.Dialogs.Task;

namespace Posoh.AN.UI.Wpf
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      DataContext= new MainWindowViewModel();
    }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CleanupTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            var dialog = new ExecuteSQLServerAgentJobTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            var dialog = new CheckIntegrityTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            var dialog = new NotifyOperatorTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            var dialog = new RebuildIndexTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            var dialog = new ReorganizeIndexTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            var dialog = new ShrinkDataBaseTaskDialog();
            dialog.ShowDialog();
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {
            var dialog = new ExecuteSqlStatementTaskDialog();
            dialog.ShowDialog();
        }
    }
}
