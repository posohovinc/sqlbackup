using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;
using Posoh.AN.Shared.Library.Task.Objects;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class ReorganizeIndexWorker : MaintenanceWorker
  {

    #region fields
    private bool compactLargeObjects = true;
    private IndexStats indexStats = new IndexStats(15.0);
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_COMPACT_LARGE_OBJECTS)]
    public bool CompactLargeObjects
    {
      get => compactLargeObjects;
      set 
      { 
        if (compactLargeObjects != value)
        {
          compactLargeObjects = value;
          OnPropertyChanged();
        }
      }
    }
    //[XmlEmbedded]
    //[XmlOrderedAttribute(1010, Constants.PERSIST_DB_MAINTENANCE_INDEX_STATS)]
    public IndexStats IndexStats
    {
      get => indexStats;
      set 
      { 
        if (indexStats != value)
        {
          indexStats = value;
          OnPropertyChanged();
        }
      }
    }
    #region map IndexStats
    [XmlOrderedAttribute(5010, Constants.PERSIST_FRAGMENTATION_OP)]
    public FragmentationOption FragmentationOp
    {
      get => indexStats.FragmentationOp;
      set => indexStats.FragmentationOp = value;
    }
    [XmlOrderedAttribute(5020, Constants.PERSIST_FRAGMENTATION_PCT)]
    public double FragmentationPct
    {
      get => indexStats.FragmentationPct;
      set => indexStats.FragmentationPct = value;
    }
    [XmlOrderedAttribute(5030, Constants.PERSIST_FRAGMENTATION_PCT_USED)]
    public bool FragmentationPctUsed
    {
      get => indexStats.FragmentationPctUsed;
      set => indexStats.FragmentationPctUsed = value;
    }
    [XmlOrderedAttribute(5040, Constants.PERSIST_LAST_USED_IN_DAYS)]
    public double LastUsedInDays
    {
      get => indexStats.LastUsedInDays;
      set => indexStats.LastUsedInDays = value;
    }
    [XmlOrderedAttribute(5050, Constants.PERSIST_LAST_USED_IN_DAYS_USED)]
    public bool LastUsedInDaysUsed
    {
      get => indexStats.LastUsedInDaysUsed;
      set => indexStats.LastUsedInDaysUsed = value;
    }
    [XmlOrderedAttribute(5060, Constants.PERSIST_PAGE_COUNT)]
    public long PageCount
    {
      get => indexStats.PageCount;
      set => indexStats.PageCount = value;
    }
    [XmlOrderedAttribute(5070, Constants.PERSIST_PAGE_COUNT_USED)]
    public bool PageCountUsed
    {
      get => indexStats.PageCountUsed;
      set => indexStats.PageCountUsed = value;
    }
    #endregion
    public override bool TaskAllowesTableSelection => true;
  }
}
