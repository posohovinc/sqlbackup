﻿namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsError: DtsObject
  {
    public string Description { get; set; }
    public int ErrorCode { get; set; }
    public int HelpContext { get; set; }
    public string HelpFile { get; set; }
    public string IDOfInterfaceWithError { get; set; }
    public string Source { get; set; }
    public string SubComponent { get; set; }
    public object TimeStamp { get; }
  }
}