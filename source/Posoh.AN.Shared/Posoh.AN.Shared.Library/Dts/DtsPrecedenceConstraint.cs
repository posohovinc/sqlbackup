﻿using System.ComponentModel;
using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsPrecedenceConstraint : DtsObject
  {
    public DtsExecutable ConstrainedExecutable { get; }
    public string CreationName { get; }
    public string Description { get; set; }
    public DTSPrecedenceEvalOp EvalOp { get; set; }
    public bool EvaluatesTrue { get; }
    public string Expression { get; set; }
    public string ID { get; }
    public bool LogicalAnd { get; set; }
    public string Name { get; set; }
    public DtsContainer Parent { get; }
    public DtsExecutable PrecedenceExecutable { get; }
    public ISite Site { get; set; }
    public DTSExecResult Value { get; set; }
  }
}