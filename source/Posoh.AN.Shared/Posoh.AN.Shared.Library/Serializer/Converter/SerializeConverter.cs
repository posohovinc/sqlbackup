﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeConverter
  {
    public virtual bool ValueToString(Serializer sender, out string stringValue, object value, object graph, ReflectionClient client, object ancestor)
    {
      stringValue = value.ToString();
      return true;
    }

    public virtual bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      value = Convert.ChangeType(stringValue, client.InfoType);
      return true;
    }
  }
}
