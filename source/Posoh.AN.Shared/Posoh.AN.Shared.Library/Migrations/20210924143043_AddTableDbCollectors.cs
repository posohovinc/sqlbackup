﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Posoh.AN.Shared.Library.Migrations
{
    public partial class AddTableDbCollectors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbCollector",
                columns: table => new
                {
                    CoreConnectionId = table.Column<Guid>(nullable: false),
                    ServerElement = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "FK_DbCollector_ConnectionsToBase_CoreConnectionId",
                        column: x => x.CoreConnectionId,
                        principalTable: "ConnectionsToBase",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbCollector_CoreConnectionId",
                table: "DbCollector",
                column: "CoreConnectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbCollector");
        }
    }
}
