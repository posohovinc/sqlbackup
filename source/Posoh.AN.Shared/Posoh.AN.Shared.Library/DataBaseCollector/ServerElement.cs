﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.DataBaseCollector.Filter;
using Posoh.AN.Shared.Library.DataBaseCollector.Objects;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.Shared.Library.DataBaseCollector
{
    [Serializable]
    public class ServerElement:Element
    {
        public ServerElement()
        {
            Connection = new CoreConnection();
        }
        [XmlElement]
        public CoreConnection Connection { get; set; }
        [XmlElement]
        public Dictionary<string, ExcludeFilters> ExcludeFilters { get; set; }
        [XmlElement]
        public IDictionary<string, IncludeFilters> IncludeFilters { get; set; }
        [XmlAttribute]
        public string Git { get; set; }
        [XmlAttribute]
        public string Path { get; set; }
        public string Name
        {
            get
            {
                if (Connection != null)
                    return Connection.Instance;
                return string.Empty;
            }
        }

        public bool ApplyFilters(BaseDbObject filterObject)
        {
            var result = ApplyIncludeFilter(filterObject.Type, filterObject.Name);
            if (result)
                result = !ApplyExcludeFilter(filterObject.Type, filterObject.Name);

            return result;
        }

        private bool ApplyIncludeFilter(string typeName, string value)
        {
            Filter.IncludeFilters include;
            if (IncludeFilters != null && IncludeFilters.TryGetValue(typeName, out include))
            {
                return include.Match(value);
            }
            else
                return true;
        }

        private bool ApplyExcludeFilter(string typeName, string value)
        {
            Filter.ExcludeFilters exclude;
            if (ExcludeFilters != null && ExcludeFilters.TryGetValue(typeName, out exclude))
            {
                return exclude.Match(value);
            }
            else
                return false;
        }

        public override string ToString()
        {
            return Connection.Instance;
        }
    }
}
