﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSObjectHostType
  {
    Task,
    ConnectionManager,
    LogProvider,
    ForEachEnumerator
  }
}