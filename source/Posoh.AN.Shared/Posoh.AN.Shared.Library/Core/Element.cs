using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Posoh.AN.Shared.Library.Core
{
  [Serializable]
  public class Element : Persistent, IPersistent, INotifyPropertyChanged 
  {
    #region private field

    private bool editing;
    private bool changed;
    [NonSerialized, NotPersistent] private PropertyChangedEventHandler propertyChangedHandler;
    [NotPersistent, NonSerialized] private EventHandler deleteHandler;

    public event PropertyChangedEventHandler PropertyChanged
    {
      add
      {
        lock (Locker)
          propertyChangedHandler = (PropertyChangedEventHandler)
            Delegate.Combine(propertyChangedHandler, value);
      }
      remove
      {
        lock (Locker)
          propertyChangedHandler = (PropertyChangedEventHandler)
            Delegate.Remove(propertyChangedHandler, value);
      }
    }

    public event EventHandler OnDelete
    {
      add
      {
        lock (Locker)
          deleteHandler = (EventHandler) Delegate.Combine(deleteHandler, value);
      }
      remove
      {
        lock (Locker)
          deleteHandler = (EventHandler) Delegate.Remove(deleteHandler, value);
      }
    }

    #endregion

    public Element()
    {
    }

    protected static void ClearEventHandler(Delegate eventHandler)
    {
      if (eventHandler != null)
        foreach (var d in eventHandler.GetInvocationList())
          eventHandler = Delegate.RemoveAll(eventHandler, d);
    }

    protected override void Free()
    {
      ClearEventHandler(propertyChangedHandler);
      ClearEventHandler(deleteHandler);
      base.Free();
    }

    [Browsable(false)]
    public bool Validate => GetValidate();

    public virtual void DefaultValues()
    {
    }

    protected void OnPropertyChanged(string propertyName)
    {
      OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
    }

    [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
    protected void OnPropertyChanged()
    {
      var methodName = new System.Diagnostics.StackTrace().GetFrame(1)?.GetMethod()?.Name;
      if (methodName != null && methodName.Length > 3)
      {
        OnPropertyChanged(new PropertyChangedEventArgs(methodName.Substring(4)));
      }
    }
    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
      propertyChangedHandler?.Invoke(this, e);
      changed = true;
    }

    public bool IsChanged => changed;

    public virtual void BeginEdit()
    {
      if (!editing)
      {
        SaveState();
        editing = true;
      }
    }

    public virtual void EndEdit()
    {
      if (editing)
      {
        AcceptChanges();
        editing = false;
      }
    }
    public virtual void CancelEdit()
    {
      if (editing)
      {
        UndoChanges();
        editing = false;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual void Delete()
    {
      deleteHandler?.Invoke(this, EventArgs.Empty);
    }

    protected virtual bool GetValidate()
    {
      return true;
    }
  }
}
