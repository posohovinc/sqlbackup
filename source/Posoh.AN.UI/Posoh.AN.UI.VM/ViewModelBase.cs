﻿using System;
using System.ComponentModel;

namespace Posoh.AN.UI.VM
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        #region Fields

        private bool editing;
        private bool changed;
        private PropertyChangedEventHandler propertyChangedHandler;
        private EventHandler deleteHandler;
        private readonly object locker = new object();

        #endregion

        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                lock (locker)
                    propertyChangedHandler = (PropertyChangedEventHandler)
                        Delegate.Combine(propertyChangedHandler, value);
            }
            remove
            {
                lock (locker)
                    propertyChangedHandler = (PropertyChangedEventHandler)
                        Delegate.Remove(propertyChangedHandler, value);
            }
        }

        public event EventHandler OnDelete
        {
            add
            {
                lock (locker)
                    deleteHandler = (EventHandler) Delegate.Combine(deleteHandler, value);
            }
            remove
            {
                lock (locker)
                    deleteHandler = (EventHandler) Delegate.Remove(deleteHandler, value);
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        protected void OnPropertyChanged()
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(1)?.GetMethod()?.Name;
            if (methodName != null && methodName.Length > 3)
            {
                OnPropertyChanged(new PropertyChangedEventArgs(methodName.Substring(4)));
            }
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            propertyChangedHandler?.Invoke(this, e);
            changed = true;
        }

        #endregion
    }
}