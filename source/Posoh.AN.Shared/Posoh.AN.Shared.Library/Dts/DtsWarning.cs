﻿namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsWarning: DtsObject
  {
    public string Description { get; set; }
    public int HelpContext { get; set; }
    public string HelpFile { get; set; }
    public string IDOfInterfaceWithWarning { get; set; }
    public string Source { get; set; }
    public string SubComponent { get; set; }
    public object TimeStamp { get; }
    public int WarningCode { get; set; }
  }
}