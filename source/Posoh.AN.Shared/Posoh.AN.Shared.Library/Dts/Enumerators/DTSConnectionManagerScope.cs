﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSConnectionManagerScope
  {
    Package,
    Project
  }
}