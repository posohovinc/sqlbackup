using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class NotifyOperatorWorker : MaintenanceWorker
  {

    #region fields
    private string message = string.Empty;
    private List<string> operatorNotify = new List<string>();
    private string profile = string.Empty;
    private string subject = string.Empty;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_MESSAGE)]
    public string Message
    {
      get => message;
      set 
      { 
        value ??= string.Empty;
        if (message != value)
        {
          message = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlEmbedded]
    [XmlArrayItem(Constants.PERSIST_OPERATOR_NOTIFY_ITEM)]
    [XmlOrderedElement(1010, Constants.PERSIST_OPERATOR_NOTIFY)]
    public List<string> OperatorNotify
    {
      get => operatorNotify;
      set 
      { 
        if (operatorNotify != value)
        {
          operatorNotify.Clear();
          if (value != null)
          {
            operatorNotify.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_PROFILE)]
    public string Profile
    {
      get => profile;
      set 
      { 
        value ??= string.Empty;
        if (profile != value)
        {
          profile = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1030, Constants.PERSIST_SUBJECT)]
    public string Subject
    {
      get => subject;
      set 
      { 
        value ??= string.Empty;
        if (subject != value)
        {
          subject = value;
          OnPropertyChanged();
        }
      }
    }
    public override bool TaskAllowesDatbaseSelection => false;
    public override bool TaskAllowesTableSelection => false;

  }
}
