﻿using System;
using System.Collections.Generic;
using Posoh.AN.Shared.Library.DataBaseCollector.Objects;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.Shared.Library.DataBaseCollector
{
    public class QueryHelper
    {
        #region private
        [NonSerialized]
        private readonly CoreConnection connection;
        #endregion

        internal QueryHelper(CoreConnection connection)
        {
            this.connection = connection;
        }

        #region Reading server objects
        #region List databases

        public DateTime GetServerDate()
        {
            var date = DateTime.MinValue;
            lock (connection.SyncRoot)
            {
                connection.Connect();
                try
                {
                    using (var dr = connection.ExecuteReader("SELECT getdate()"))
                    {
                        if (dr.Read())
                        {
                            date = dr.GetDateTime(0);
                        }
                    }
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return date;
        }



        #endregion
        //#region JobList
        //public IList JobList(string selected)
        //{
        //  var result = new List<JobDescription>();
        //  lock (connection.SyncRoot)
        //  {
        //    connection.Connect();
        //    try
        //    {
        //      connection.QueryHelper.ExecuteList(result, Queries.Queries.SelectJobDescription);
        //      if (!string.IsNullOrEmpty(selected))
        //      {
        //        foreach (var job in result)
        //        {
        //          if (string.Compare(job.Name, selected, true) == 0)
        //          {
        //            job.Checked = true;
        //            break;
        //          }
        //        }
        //      }
        //    }
        //    finally
        //    {
        //      connection.Disconnect();
        //    }
        //  }
        //  return result;
        //}
        //#endregion

        #endregion

        public void ExecuteCommand(string cmd)
        {
            lock (connection.SyncRoot)
            {
                connection.Connect();
                try
                {
                    connection.ExecuteNonQuery(cmd);
                }
                finally
                {
                    connection.Disconnect();
                }
            }
        }
        public int ExecuteScalar(string cmd)
        {
            int result;
            lock (connection.SyncRoot)
            {
                connection.Connect();
                try
                {
                    result = (int)connection.ExecuteScalar(cmd);
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return result;
        }

        public virtual List<DbObject> GetDatabases()
        {
            var result = new List<DbObject>();
            lock (connection.SyncRoot)
            {
                connection.Connect();
                try
                {
                    using (var dr = connection.ExecuteReader(Properties.Resources.GetDataBases))
                    {
                        while (dr.Read())
                            result.Add(new DbObject { Name = dr.GetString("Name"), Type = "DB" });
                    }
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return result;
        }

        public List<DbObject> GetDbDataObjects(string dbName)
        {
            var result = new List<DbObject>();
            lock (connection.SyncRoot)
            {
                try
                {
                    connection.Connect();
                    using (var dr = connection.ExecuteReader(string.Format(Properties.Resources.GetDbObjects, dbName)))
                    {
                        while (dr.Read())
                        {
                            var obj = new DbObject()
                            {
                                Id = dr.GetInt32("object_id"),
                                Folder = dr.GetString("Folder"),
                                File = dr.GetString("File"),
                                Name = dr.GetString("Name"),
                                Type = dr.GetString("Type").Trim(),
                            };
                            result.Add(obj);
                        }
                    }
                }
                catch
                {
                    //TODO need write to log
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return result;
        }


        public List<ObjectDefinition> GetDataObjectString(DbObject obj)
        {
            var list = new List<ObjectDefinition>();
            lock (connection.SyncRoot)
            {
                try
                {
                    connection.Connect();
                    try
                    {
                        using (var command = connection.GetSqlCommand(Properties.Resources.GetObjectText))
                        {
                            command.Parameters.AddWithValue("@id", obj.Id
                            );
                            using (var dr = new CoreSafeReader(command.ExecuteReader()))
                            {
                                while (dr.Read())
                                {
                                    var file = dr.GetString("File");
                                    if (!string.IsNullOrEmpty(file))
                                    {
                                        var defenition = new ObjectDefinition { FileName = dr.GetString("File"), Definition = dr.GetString("Definition") };
                                        list.Add(defenition);
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        connection.Disconnect();
                    }
                }
                catch (Exception ex)
                {
                    var messa = ex.Message;
                    //TODO need write to log
                }
            }
            return list;
        }

        public List<ServerObject> GetDbObjects()
        {
            var result = new List<ServerObject>();
            lock (connection.SyncRoot)
            {
                try
                {
                    connection.Connect();
                    using (var dr = connection.ExecuteReader(Properties.Resources.GetDbJobs))
                    {
                        while (dr.Read())
                        {
                            var obj = new ServerObject()
                            {
                                Id = dr.GetGuid("object_id"),
                                Folder = dr.GetString("Folder"),
                                File = dr.GetString("File"),
                                Name = dr.GetString("Name"),
                                Type = "JB"
                            };
                            result.Add(obj);
                        }
                    }

                    using (var dr = connection.ExecuteReader(Properties.Resources.GetDbPackages))
                    {
                        while (dr.Read())
                        {
                            var obj = new ServerObject()
                            {
                                Id = dr.GetGuid("object_id"),
                                Folder = dr.GetString("Folder"),
                                File = dr.GetString("File"),
                                Name = dr.GetString("Name"),
                                Type = "PCG"
                            };
                            result.Add(obj);
                        }
                    }
                }
                catch
                {
                    //TODO need write to log
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return result;
        }

        public List<ObjectDefinition> GetDbObjectString(ServerObject obj)
        {
            var list = new List<ObjectDefinition>();
            lock (connection.SyncRoot)
            {
                try
                {
                    connection.Connect();
                    string query;
                    if (obj.Type == "JB")
                        query = Properties.Resources.GetJobStepsText;
                    else
                        query = Properties.Resources.GetPackageText;

                    using (var command = connection.GetSqlCommand(query))
                    {
                        command.Parameters.AddWithValue("@id", obj.Id);
                        using (var dr = new CoreSafeReader(command.ExecuteReader()))
                        {
                            while (dr.Read())
                            {
                                list.Add(new ObjectDefinition { FileName = dr.GetString("File"), Definition = dr.GetString("Definition") });
                            }
                        }
                    }
                }
                catch
                {
                    //TODO need write to log
                }
                finally
                {
                    connection.Disconnect();
                }
            }
            return list;
        }
    }
}
