﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter.ObjectConverter
{
  public class SerializeXmlObjectConverter : SerializeObjectConverter
  {
    private XmlNode IfEmbeddedThenAddNode(Serializer sender, ReflectionClient client, XmlNode ancestor)
    {
      var valueNode = ancestor;
      if (client.Info.GetCustomAttribute<XmlEmbeddedAttribute>() == null)
      {
        valueNode = ((SerializerXml)sender).CreateElement(ancestor, client.Key);
      }

      return valueNode;
    }

    protected override void WriteClass(Serializer sender, string key, object graph, object ancestor)
    {
      var valueNode = ((SerializerXml) sender).CreateElement(ancestor, key);
      base.WriteClass(sender, key, graph, valueNode);
    }

    protected override void WriteDictionary(Serializer sender, IDictionary dictionary, Type typeKey, Type typeValue, ReflectionClient client, object ancestor)
    {
      if (dictionary.Count == 0)
        return;

      sender.GetConverter().TryGetConverter(typeValue, out var converter);
      var isClass = typeValue.IsClass && (converter.GetType() != typeof(SerializeStringConverter));

      var valueNode = IfEmbeddedThenAddNode(sender, client, (XmlNode)ancestor);

      foreach (var key in dictionary.Keys)
      {
        var value = dictionary[key];

        if (isClass)
        {
          WriteClass(sender, key.ToString(), value, valueNode);
        }
        else
        {
          if ((converter != null) && converter.ValueToString(sender, out var stringValue, value, dictionary, client, valueNode))
          {
            sender.AddPair(valueNode, value, key.ToString(), stringValue);
          }
        }
      }
    }
    protected override void WriteArray(Serializer sender, IList enumerable, Type typeArg, ReflectionClient client, object ancestor)
    {
      if(enumerable.Count == 0)
        return;

      sender.GetConverter().TryGetConverter(typeArg, out var converter);
      var isClass = typeArg.IsClass && (converter.GetType() != typeof(SerializeStringConverter));

      var valueNode = IfEmbeddedThenAddNode(sender, client, (XmlNode)ancestor);

      var item = client.Info.GetCustomAttribute<XmlArrayItemAttribute>();
      var key = (item != null) ? item.ElementName : ((XmlNode)ancestor).Name;

      foreach (var value in enumerable)
      {
        if (isClass)
        {
          WriteClass(sender, key, value, valueNode);
        }
        else
        {
          if ((converter != null) && converter.ValueToString(sender, out var stringValue, value, enumerable, client, valueNode))
          {
            sender.AddPair(valueNode, value, key, stringValue);
          }
        }
      }
    }

    protected override void ReadArray(Serializer sender, IList value, Type typeArg, ReflectionClient client, object ancestor)
    {
      sender.GetConverter().TryGetConverter(typeArg, out var converter);
      var isClass = typeArg.IsClass && (converter.GetType() != typeof(SerializeStringConverter));

     
      var valueNode = (client.Info.GetCustomAttributes<XmlEmbeddedAttribute>() != null) ? (XmlNode)ancestor : ((XmlNode)ancestor).FirstChild;
      var arrayItem = string.Empty;
      var itemAttribute = (XmlArrayItemAttribute)client.Info.GetCustomAttribute<XmlArrayItemAttribute>();
      if (itemAttribute != null)
      {
        var prefix = ((SerializerXml) sender).Prefix;
        arrayItem = (string.IsNullOrEmpty(prefix) ? "" : prefix + ":") + itemAttribute.ElementName;
      }


      while (valueNode != null)
      {
        object resultValue = null;
        var stringValue = string.IsNullOrEmpty(arrayItem) ? valueNode.InnerText : valueNode.Attributes[arrayItem].Value;

        if (isClass)
        {
          resultValue = Activator.CreateInstance(typeArg);
          ReadClass(sender, stringValue, resultValue, valueNode);
        }
        else
        {
          resultValue = Convert.ChangeType(stringValue, typeArg);
        }
        value.Add(resultValue);
        valueNode = valueNode.NextSibling;
      }
    }

    protected override void ReadDictionary(Serializer sender, IDictionary dictionary, Type typeKey, Type typeValue, ReflectionClient client, object ancestor)
    {
      sender.GetConverter().TryGetConverter(typeValue, out var converter);
      var isClass = typeValue.IsClass && (converter.GetType() != typeof(SerializeStringConverter));

      var valueNode = ((XmlNode)ancestor).FirstChild;
      while (valueNode != null)
      {
        object resultValue = null;
        var keyValue = Convert.ChangeType(valueNode.Name, typeKey); ;
        var stringValue = valueNode.InnerText;

        if (isClass)
        {
          resultValue = Activator.CreateInstance(typeValue);
          ReadClass(sender, stringValue, resultValue, valueNode);
        }
        else
        {
          resultValue = Convert.ChangeType(stringValue, typeValue);
        }
        dictionary.Add(keyValue, resultValue);
        valueNode = valueNode.NextSibling;
      }
    }
  }
}
