﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum DeviceType
  {
    LogicalDevice,
    Tape,
    File,
    Pipe,
    VirtualDevice,
    Url
  }
}