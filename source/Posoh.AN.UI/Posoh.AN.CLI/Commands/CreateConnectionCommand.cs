using Microsoft.EntityFrameworkCore;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.CLI.Commands;

public class CreateConnectionCommand : BaseCommand
{
    public CreateConnectionCommand(string[] args) : base(args)
    {
        CommandName = "createConnection";
        Parameters = new List<Parameter>()
        {
            new Parameter() {Name = "-conname"},
            new Parameter() {Name = "-server"},
            new Parameter() {Name = "-user"},
            new Parameter() {Name = "-pass"},
            new Parameter() {Name = "-dbname"}
        };
    }

    protected override void ExecuteCommand()
    {
        using (var context = new LocalDbContext())
        {
            context.Database.Migrate();
            context.ConnectionsToBase.Add(new CoreConnection()
            {
                AuthType = 0,
                Database = Parameters.First(x => x.Name == "-dbname").Value,
                Instance = Parameters.First(x => x.Name == "-server").Value,
                UserName = Parameters.First(x => x.Name == "-user").Value,
                Password = Parameters.First(x => x.Name == "-pass").Value,
                Name = Parameters.First(x => x.Name == "-conname").Value,
                ServerId = Guid.NewGuid(),
            });
            context.SaveChanges();
        }
    }
}