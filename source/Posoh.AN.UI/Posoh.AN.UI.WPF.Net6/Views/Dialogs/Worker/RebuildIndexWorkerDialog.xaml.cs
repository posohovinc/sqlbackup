﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for RebuildIndexWorkerDialog.xaml
    /// </summary>
    public partial class RebuildIndexWorkerDialog : AbstractRebuildIndexWorkerDialog
    {
        public RebuildIndexWorkerDialog() 
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            RebuildIndexWorkerControl.Self = Vm;
        }
    }
}
