﻿using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.UI.VM;

namespace Posoh.AN.UI.WPF.Net6.ViewModels.Windows
{
    public class ConnectionPropertiesWindowViewModel :ViewModelBase
    {
        #region Fields

        private CoreConnection coreConnection;
        private ConnectionPropertiesViewModel _connectionPropertiesViewModel = new ConnectionPropertiesViewModel();

        #endregion

        public CoreConnection CoreConnection
        {
            get { return coreConnection; }
            set
            {
                coreConnection = value;
                OnPropertyChanged();
                _connectionPropertiesViewModel.CoreConnection = value;
            }
        }

        public ConnectionPropertiesViewModel ConnectionPropertiesViewModel
        {
            get { return _connectionPropertiesViewModel; }
        }
    }
}
