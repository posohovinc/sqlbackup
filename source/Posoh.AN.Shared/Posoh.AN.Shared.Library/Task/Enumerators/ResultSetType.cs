﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum ResultSetType
  {
    ResultSetType_None = 1,
    ResultSetType_Rowset = 3,
    ResultSetType_SingleRow = 2,
    ResultSetType_XML = 4
  }
}