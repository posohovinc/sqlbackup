﻿using System.Windows;
using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    public class BaseWorkerDialog : Window
    {
        public BaseWorkerDialog()
        {
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }
        static BaseWorkerDialog()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BaseWorkerDialog),
                new FrameworkPropertyMetadata(typeof(BaseWorkerDialog)));
        }


        protected void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        protected void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        protected virtual void OnCloseClick()
        {
            
        }
        protected virtual void OnOkClick()
        {
            
        }

        public override void OnApplyTemplate()
        {
            Button cancelButton = GetTemplateChild("cancelButton") as Button;
            if (cancelButton != null)
                cancelButton.Click += CloseClick;

            Button okButton = GetTemplateChild("okButton") as Button;
            if (okButton != null)
                okButton.Click += OkClick;
        }

    }
}