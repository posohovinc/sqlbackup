﻿using System;
using System.ComponentModel;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsExtendedProperty : DtsObject
  {
    public string CreationName { get; }
    public TypeCode DataType { get; }
    public string Description { get; set; }
    public string ID { get; }
    public string Name { get; set; }
    public string Namespace { get; set; }
    //public ISite Site { get; set; }
    public object Value { get; set; }
  }
}