﻿using System;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Objects
{
    public class ServerObject:BaseDbObject
    {
        public Guid Id { get; set; }
    }
}
