﻿using System;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    [Serializable]
    public class IncludeFilters : Filters
    {
        [XmlIgnore]
        public override Direction Direction => Direction.Include;
    }
}
