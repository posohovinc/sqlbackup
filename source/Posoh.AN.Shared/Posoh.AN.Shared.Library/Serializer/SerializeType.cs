﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Posoh.AN.Shared.Library.Serializer
{
  public enum SerializeType
  {
    Xml,
    Json
  }
}
