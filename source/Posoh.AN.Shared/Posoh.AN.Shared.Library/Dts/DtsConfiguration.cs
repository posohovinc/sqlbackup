﻿using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsConfiguration:  DtsObject
  {
    public string ConfigurationString { get; set; }
    public DTSConfigurationType ConfigurationType { get; set; }
    public string CreationName { get; }
    public string Description { get; set; }
    public string ID { get; }
    public string Name { get; set; }
    public string PackagePath { get; set; }
  }
}