﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for HistoryCleanupWorkerDialog.xaml
    /// </summary>
    public partial class HistoryCleanupWorkerDialog : AbstractHistoryCleanupWorkerDialog
    {
        public HistoryCleanupWorkerDialog() 
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            HistoryCleanupWorkerControl.Self = Vm;
        }
    }
}
