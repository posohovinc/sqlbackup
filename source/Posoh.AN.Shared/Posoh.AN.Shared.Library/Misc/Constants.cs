﻿namespace Posoh.AN.Shared.Library.Misc
{
  public static class Constants
  {
    public const string PERSIST_ADV_KEEP_ONLINE = "KeepOnline";
    public const string PERSIST_ADV_OFFLINE_UNSUPPORTED_INDEX = "SkipUnsupported";
    public const string PERSIST_ADV_SORT_IN_TEMPDB = "Sort";
    public const string PERSIST_AGE_BASED = "AgeBased";
    public const string PERSIST_AGENT_JOB_ID = "AgentJobID";
    public const string PERSIST_BACKUP_ACTION = "BackupAction";
    public const string PERSIST_BACKUP_COMPRESSION_OPTION = "BackupCompressionAction";
    public const string PERSIST_BACKUP_DEVICE_TYPE = "BackupDeviceType";
    public const string PERSIST_BACKUP_ENCRYPTION_ALGORITHM = "BackupEncryptionAlgorithm";
    public const string PERSIST_BACKUP_ENCRYPTOR_NAME = "BackupEncryptorName";
    public const string PERSIST_BACKUP_ENCRYPTOR_TYPE = "BackupEncryptorType";
    public const string PERSIST_BACKUP_FILE_EXTENSION = "BackupFileExtension";
    public const string PERSIST_BACKUP_IS_INCREMENTAL = "BackupIsIncremental";
    public const string PERSIST_BACKUP_PHYSICAL_DESTINATION_TYPE = "BackupPhysicalDestinationType";
    public const string PERSIST_BLOCK_SIZE = "BlockSize";
    public const string PERSIST_BYPASS_PREPARE = "BypassPrepare";
    public const string PERSIST_CHECKSUM = "Checksum";
    public const string PERSIST_CLEAN_SUB_FOLDERS = "CleanSubFolders";
    public const string PERSIST_CODE_PAGE = "CodePage";
    public const string PERSIST_COMPACT_LARGE_OBJECTS = "CompactLargeObjects";
    public const string PERSIST_CONNECTION = "Connection";
    public const string PERSIST_CONTAINER_NAME = "ContainerName";
    public const string PERSIST_CONTINUE_ON_ERROR = "ContinueOnError";
    public const string PERSIST_COPY_ONLY_BACKUP = "CopyOnlyBackup";
    public const string PERSIST_CREATE_SUB_FOLDER = "BackupCreateSubFolder";
    public const string PERSIST_CREDENTIAL_NAME = "CredentialName";
    public const string PERSIST_DATABASE_NAME = "DatabaseName";
    public const string PERSIST_DATABASE_SELECTION_TYPE = "DatabaseSelectionType";
    public const string PERSIST_DATABASE_SIZE_LIMIT = "DatabaseSizeLimit";
    public const string PERSIST_DB_MAINTENANCE_INDEX_STATS = "IndexStats";
    public const string PERSIST_DELETE_SPECIFIC_FILE = "DeleteSpecificFile";
    public const string PERSIST_DESTINATION_AUTO_FOLDER_PATH = "BackupDestinationAutoFolderPath";
    public const string PERSIST_DESTINATION_CREATION_TYPE = "BackupDestinationType";
    public const string PERSIST_DESTINATION_MANUAL_LIST = "DestinationManualList";
    public const string PERSIST_EXISTING_BACKUPS_ACTION = "BackupActionForExistingBackups";
    public const string PERSIST_EXPIRE_DATE = "ExpireDate";
    public const string PERSIST_EXTENDED_LOGGING = "ExtendedLogging";
    public const string PERSIST_FILE_EXTENSION = "FileExtension";
    public const string PERSIST_FILE_GROUPS_FILES = "BackupFileGroupsFiles";
    public const string PERSIST_FILE_PATH = "FilePath";
    public const string PERSIST_FILE_TYPE_SELECTED = "FileTypeSelected";
    public const string PERSIST_FOLDER_PATH = "FolderPath";
    public const string PERSIST_IGNORE_REPLICA_TYPE = "IgnoreReplicaType";
    public const string PERSIST_INCLUDE_INDEXES = "IncludeIndexes";
    public const string PERSIST_IN_DAYS = "InDays";
    public const string PERSIST_IS_BACKUP_ENCRYPTED = "IsBackupEncrypted";
    public const string PERSIST_IS_BLOCK_SIZE_USED = "IsBlockSizeUsed";
    public const string PERSIST_IS_MAX_TRANSFER_SIZE_USED = "IsMaxTransferSizeUsed";
    public const string PERSIST_IS_STORED_PROCEDURE = "IsStoredProcedure";
    public const string PERSIST_LOCAL_CONNECTION_FOR_LOGGING = "LocalConnectionForLogging";
    public const string PERSIST_LOW_PRIORITY_ABORT_AFTER_WAIT = "LowPriorityAbortAfterWait";
    public const string PERSIST_LOW_PRIORITY_MAX_DURATION = "LowPriorityMaxDuration";
    public const string PERSIST_LOW_PRIORITY_USED = "LowPriorityUsed";
    public const string PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM = "MaximumDegreeOfParallelism";
    public const string PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM_USED = "MaximumDegreeOfParallelismUsed";
    public const string PERSIST_MAX_TRANSFER_SIZE = "MaxTransferSize";
    public const string PERSIST_MESSAGE = "Message";
    public const string PERSIST_OBJECT_TYPE_SELECTION = "ObjectTypeSelection";
    public const string PERSIST_OLDER_THAN_TIME_UNITS = "RemoveOlderThan";
    public const string PERSIST_OLDER_THAN_TIME_UNIT_TYPE = "TimeUnitsType";
    public const string PERSIST_OPERATOR_NOTIFY = "OperatorNotifyList";
    public const string PERSIST_OPERATOR_NOTIFY_ITEM = "OperatorNotify";
    public const string PERSIST_PAD_INDEX = "PadIndex";
    public const string PERSIST_PARAMETER_BINDINGS = "ParameterBindings";
    public const string PERSIST_PERCENT_LIMIT = "DatabasePercentLimit";
    public const string PERSIST_PHYSICAL_ONLY = "PhysicalOnly";
    public const string PERSIST_PROFILE = "Profile";
    public const string PERSIST_REINDEX_PERCENTAGE = "Percentage";
    public const string PERSIST_REINDEX_WITH_ORIGINAL_AMOUNT = "UseOriginalAmount";
    public const string PERSIST_REMOVE_BACKUP_RESTOR_HISTORY = "RemoveBackupRestoreHistory";
    public const string PERSIST_REMOVE_DB_MAINT_HISTORY = "RemoveDbMaintHistory";
    public const string PERSIST_REMOVE_SQL_AGENT_HISTORY = "RemoveAgentHistory";
    public const string PERSIST_IGNORE_DATABASES_IN_NOT_ONLINE_STATE = "IgnoreDatabasesInNotOnlineState";
    public const string PERSIST_RESULT_SET_BINDINGS = "ResultSetBindings";
    public const string PERSIST_RESULT_SET_TYPE = "ResultSetType";
    public const string PERSIST_RETAIN_DAYS = "RetainDays";
    public const string PERSIST_RETURN_FREED_SPACE = "DatabaseReturnFreeSpace";
    public const string PERSIST_SELECTED_DATABASES = "SelectedDatabases";
    public const string PERSIST_SELECTED_TABLES = "SelectedTables";
    public const string PERSIST_SERVER_VERSION = "ServerVersion";
    public const string PERSIST_SQL_STATEMENT_SOURCE = "SqlStatementSource";
    public const string PERSIST_SQL_STATEMENT_SOURCE_TYPE = "SqlStatementSourceType";
    public const string PERSIST_SQL_TASK_DATA = "SqlTaskData";
    public const string PERSIST_SUBJECT = "Subject";
    public const string PERSIST_TABLE_NAME = "TableName";
    public const string PERSIST_TABLE_SELECTION_TYPE = "TableSelectionType";
    public const string PERSIST_TABLOCK = "Tablock";
    public const string PERSIST_TASK_NAME = "TaskName";
    public const string PERSIST_TIME_OUT = "TimeOut";
    public const string PERSIST_TYPE_CONVERSION_MODE = "TypeConversionMode";
    public const string PERSIST_UPDATE_SAMPLE_VALUE = "UpdateSampleValue";
    public const string PERSIST_UPDATE_SCAN_TYPE = "UpdateScanType";
    public const string PERSIST_UPDATE_TYPE = "UpdateStatisticsType";
    public const string PERSIST_URL_PREFIX = "UrlPrefix";
    public const string PERSIST_USE_EXPIRATION = "UseExpiration";
    public const string PERSIST_VERIFY_INTEGRITY = "BackupVerifyIntegrity";
    
    public const string PERSIST_FRAGMENTATION_OP = "FragmentationOp";
    public const string PERSIST_FRAGMENTATION_PCT = "FragmentationPercent";
    public const string PERSIST_FRAGMENTATION_PCT_USED = "CheckFragmentationPercentUsed";
    public const string PERSIST_LAST_USED_IN_DAYS = "CheckLastUsageInDays";
    public const string PERSIST_LAST_USED_IN_DAYS_USED = "CheckLastUsageInDaysUsed";
    public const string PERSIST_PAGE_COUNT = "PageCount";
    public const string PERSIST_PAGE_COUNT_USED = "CheckPageCountUsed";

    public const string PERSIST_NAMESPACE = @"www.microsoft.com/sqlserver/dts/tasks/sqltask";
    public const string PERSIST_PREFIX = "SQLTask";
    public const string PERSIST_PREFIX_DTS = "DTS";
    public const string PERSIST_NAMESPACE_DTS = "www.microsoft.com/SqlServer/Dts";

  }
}