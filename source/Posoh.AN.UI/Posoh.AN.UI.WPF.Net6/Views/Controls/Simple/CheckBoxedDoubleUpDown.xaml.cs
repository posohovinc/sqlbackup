﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for CheckBoxedDoubleUpDown.xaml
    /// </summary>
    public partial class CheckBoxedDoubleUpDown : UserControl
    {
        public static readonly DependencyProperty IsCheckedProperty;
        public static readonly DependencyProperty ValueProperty;

        public CheckBoxedDoubleUpDown()
        {
            InitializeComponent();            
            Binding binding = new Binding("IsChecked");
            binding.Source = this;
            chbProp.SetBinding(CheckBox.IsCheckedProperty, binding);

        }

        static CheckBoxedDoubleUpDown()
        {
            IsCheckedProperty = DependencyProperty.Register(
                "IsChecked",
                typeof(bool),
                typeof(CheckBoxedDoubleUpDown));
            ValueProperty = DependencyProperty.Register(
                "Value",
                typeof(double),
                typeof(CheckBoxedDoubleUpDown));
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(CheckBoxedDoubleUpDown), new FrameworkPropertyMetadata(typeof(CheckBoxedDoubleUpDown)));
        }

        public bool IsChecked
        {
            get => (bool)GetValue(IsCheckedProperty);
            set => SetValue(IsCheckedProperty, value);
        }
        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public string PropName
        {
            get => chbProp.Content.ToString();
            set => chbProp.Content = value;
        }

        // private void chbProp_Checked(object sender, RoutedEventArgs e)
        // {
        //     if (chbProp.IsChecked != null) 
        //         IsChecked = chbProp.IsChecked.Value;
        // }

        private void dudValue_ValueChanged(object sender, Events.ValueChangedEventArgs e)
        {
            Value = dudValue.Value;
        }
    }
}
