﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum AbortAfterWait
  {
    None = 0,
    Blockers = 1,
    Self = 2
  }
}