﻿using System;
using System.IO;
using System.Text.Json;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Serializer.Converter.ObjectConverter;
using Posoh.AN.Shared.Library.Serializer.Enumerator;

namespace Posoh.AN.Shared.Library.Serializer
{
  public class SerializerJson: Serializer
  {
    public SerializerJson() : base(SerializeType.Json)
    {
      SerializeAttributes = new[] { typeof(ElementAttribute) };
    }

    public override void Serialize(object graph, out string result)
    {
      result = string.Empty;
      var json = new JsonElement();
      //var p = new JsonProperty(json);

    }

    public override void Serialize(TextWriter streamWriter, object graph)
    {
      throw new System.NotImplementedException();
    }

    public override void AddPair(object ancestor, object value, string clientKey, string stringValue)
    {
      throw new System.NotImplementedException();
    }

    protected override SerializeIterator GetIterator(Type serializeAttributes, object ancestor)
    {
      throw new NotImplementedException();
    }

    public override void Deserialize(object graph, string data)
    {
      throw new NotImplementedException();
    }
  }
}