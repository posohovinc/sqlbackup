using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Objects
{
  public class IndexStats: Element
  {

    #region fields
    private FragmentationOption fragmentationOp;
    private double fragmentationPct;
    private bool fragmentationPctUsed;
    private double lastUsedInDays;
    private bool lastUsedInDaysUsed;
    private long pageCount;
    private bool pageCountUsed;
    #endregion

    public IndexStats(double fragmentationPct)
    {
      fragmentationPctUsed = true;
      pageCountUsed = true;
      fragmentationOp = FragmentationOption.Fast;
      pageCount = 1000L;
      lastUsedInDays = 7.0;
      this.fragmentationPct = fragmentationPct;
    }

    [XmlOrderedAttribute(10, Constants.PERSIST_FRAGMENTATION_OP)]
    public FragmentationOption FragmentationOp
    {
      get => fragmentationOp;
      set 
      { 
        if (fragmentationOp != value)
        {
          fragmentationOp = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(20, Constants.PERSIST_FRAGMENTATION_PCT)]
    public double FragmentationPct
    {
      get => fragmentationPct;
      set 
      { 
        if (fragmentationPct != value)
        {
          fragmentationPct = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(30, Constants.PERSIST_FRAGMENTATION_PCT_USED)]
    public bool FragmentationPctUsed
    {
      get => fragmentationPctUsed;
      set 
      { 
        if (fragmentationPctUsed != value)
        {
          fragmentationPctUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(40, Constants.PERSIST_LAST_USED_IN_DAYS)]
    public double LastUsedInDays
    {
      get => lastUsedInDays;
      set 
      { 
        if (lastUsedInDays != value)
        {
          lastUsedInDays = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(50, Constants.PERSIST_LAST_USED_IN_DAYS_USED)]
    public bool LastUsedInDaysUsed
    {
      get => lastUsedInDaysUsed;
      set 
      { 
        if (lastUsedInDaysUsed != value)
        {
          lastUsedInDaysUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(60, Constants.PERSIST_PAGE_COUNT)]
    public long PageCount
    {
      get => pageCount;
      set 
      { 
        if (pageCount != value)
        {
          pageCount = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(70, Constants.PERSIST_PAGE_COUNT_USED)]
    public bool PageCountUsed
    {
      get => pageCountUsed;
      set 
      { 
        if (pageCountUsed != value)
        {
          pageCountUsed = value;
          OnPropertyChanged();
        }
      }
    }

  }
}
