﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Posoh.AN.Shared.Library.Attributes
{
  public interface IOrdered : IComparable
  {
    string Key { get; }
    int Order { get; }
  }
}
