﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for CheckIntegrityWorkerDialog.xaml
    /// </summary>
    public partial class CheckIntegrityWorkerDialog : AbstractCheckIntegrityWorkerDialog
    {
        public CheckIntegrityWorkerDialog()
        {
            InitializeComponent();
            //CheckIntegrityWorkerControl.DataContext = new CheckIntegrityWorkerViewModel()
            //{ Worker = new CheckIntegrityWorker() };
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            CheckIntegrityWorkerControl.Self = Vm;
        }

    }
}
