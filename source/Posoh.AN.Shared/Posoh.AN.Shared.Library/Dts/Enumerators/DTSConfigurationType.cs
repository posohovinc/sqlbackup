﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSConfigurationType
  {
    ParentVariable,
    ConfigFile,
    EnvVariable,
    RegEntry,
    IParentVariable,
    IConfigFile,
    IRegEntry,
    SqlServer,
    IniFile,
    ISqlServer,
    IIniFile
  }
}