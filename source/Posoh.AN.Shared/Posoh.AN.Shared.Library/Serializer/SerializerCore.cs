﻿using System;
using System.IO;
using System.Text;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.Serializer.Converter;
using Posoh.AN.Shared.Library.Serializer.Enumerator;

namespace Posoh.AN.Shared.Library.Serializer
{
  [Serializable]
  public abstract class Serializer: ICloneable
  {
    #region fields

    [NonSerialized] private SerializeValueConverter valueConverter;
    [NonSerialized] private Type[] serializeAttributes;
    [NonSerialized] private Type currentSerializeAttribute;
    private SerializeType serializeType = SerializeType.Xml;
    #endregion

    #region special coverter
    #endregion

    public SerializeValueConverter GetConverter()
    {
      if (valueConverter == null)
      {
        valueConverter = ValueConverterFactory.GetValueConverter(serializeType);
      }

      return valueConverter;
    }
    protected Serializer(SerializeType serializeType)
    {
      this.serializeType = serializeType;
    }
    public Type[] SerializeAttributes
    {
      get => serializeAttributes;
      set
      {
        serializeAttributes = value;
        currentSerializeAttribute = serializeAttributes[0];
      }
    }
    protected Type CurrentSerializeAttribute => currentSerializeAttribute;
    public byte[] Serialize(object graph)
    {
      Serialize(graph, out var result);
      return Encoding.UTF8.GetBytes(result);
    }
    public abstract void Serialize(object graph, out string result);

    public void Serialize(string fileName, object graph)
    {
      using (var streamWriter = new StreamWriter(fileName, false))
      {
        Serialize(streamWriter, graph);
      }
    }
    public abstract void Serialize(TextWriter streamWriter, object graph);
    public abstract void AddPair(object ancestor, object value, string clientKey, string stringValue);

    public void Serialize(object graph, object ancestor)
    {
      if (graph != null)
      {
        GetConverter();
        foreach (var serializeAttribute in SerializeAttributes)
        {
          currentSerializeAttribute = serializeAttribute;
          var reflections = Reflection.PropertyList(graph, currentSerializeAttribute);
          foreach (var client in reflections)
          {
            client.GetValue(graph, out object value);
            if ((value != null) && !client.IsDefault(value) && valueConverter.TryGetConverter(value.GetType(), out var converter))
            {
              if (converter.ValueToString(this, out var stringValue, value, graph, client, ancestor))
              {
                AddPair(ancestor, value, client.Key, stringValue);
              }
            }
          }
        }
      }
    }

    protected abstract SerializeIterator GetIterator(Type serializeAttributes, object ancestor);

    public void Deserialize(object graph, object ancestor)
    {
      if (graph != null)
      {
        GetConverter();
        foreach (var serializeAttribute in SerializeAttributes)
        {
          currentSerializeAttribute = serializeAttribute;

          var reflections = Reflection.PropertyHash(graph, currentSerializeAttribute);
          var iterator = GetIterator(currentSerializeAttribute, ancestor);
          while (iterator.MoveNext())
          {
            var pair = iterator.Current;
            if (pair != null && reflections.TryGetValue(pair.Key, out var client))
            {
              client.GetValue(graph, out object value);
              if (valueConverter.TryGetConverter(client.InfoType, out var converter))
              {
                if (converter.StringToValue(this, pair.Value, ref value, graph, client, pair.Ancestor))
                {
                  client.SetValue(graph, value);
                }
              }
            }
          }
        }
      }
    }

    public abstract void Deserialize(object graph, string data);

    public void Deserialize(string fileName, object graph)
    {
      using (var streamRead = new StreamReader(fileName, Encoding.UTF8, false))
      {
        Serialize(streamRead, graph);
      }
    }

    public void Deserialize(TextReader streamRead, object graph)
    {
      Deserialize(streamRead.ReadToEnd(), graph);
    }


    public static string Encrypt(string value)
    {
      return value;
    }
    public static string Decrypt(string value)
    {
      return value;
    }
    public object Clone()
    {
      return Clonner.Clone(this);
    }
  }

}
