﻿using System.Collections.ObjectModel;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.UI.VM;

namespace PPosoh.AN.UI.WPF.Net6.ViewModels
{
    public class BaseSqlPropDialogViewModel:ViewModelBase
    {
        #region Fields

        private ObservableCollection<CoreConnection> _connectionsToBase;
        private ObservableCollection<CoreConnection> _dataBases;
        private ObservableCollection<CoreConnection> _objects;
        private ObservableCollection<CoreConnection> _selections;

        #endregion
        
        public BaseSqlPropDialogViewModel()
        {
        }

        public ObservableCollection<CoreConnection> ConnectionsToBase
        {
            get => _connectionsToBase;
            set
            {
                _connectionsToBase = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CoreConnection> DataBases
        {
            get => _dataBases;
            set
            {
                _dataBases = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CoreConnection> Objects
        {
            get => _objects;
            set
            {
                _objects = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CoreConnection> Selections
        {
            get => _selections;
            set
            {
                _selections = value;
                OnPropertyChanged();
            }
        }
    }
}