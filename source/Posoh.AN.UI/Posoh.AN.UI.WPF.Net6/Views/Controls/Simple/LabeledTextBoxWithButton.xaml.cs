﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for LabeledTextBoxWithButton.xaml
    /// </summary>
    public partial class LabeledTextBoxWithButton : UserControl
    {
        public Action? ButtonAction;
        public static readonly DependencyProperty TextProperty;
        public static readonly DependencyProperty ButtonNameProperty;
        public static readonly DependencyProperty CaptionProperty;


        public LabeledTextBoxWithButton()
        {
            InitializeComponent();
            Binding binding = new Binding("Text");
            binding.Source = this;
            TbText.SetBinding(TextBox.TextProperty, binding);
            BtnOption.SetBinding(Button.ContentProperty, new Binding("ButtonName") { Source = this });
            TblCaption.SetBinding(TextBox.TextProperty, new Binding("Caption") { Source = this });
        }

        static LabeledTextBoxWithButton()
        {
            TextProperty = DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(LabeledTextBoxWithButton));

            ButtonNameProperty = DependencyProperty.Register(
                "ButtonName",
                typeof(string),
                typeof(LabeledTextBoxWithButton));
            
            CaptionProperty = DependencyProperty.Register(
                "Caption",
                typeof(string),
                typeof(LabeledTextBoxWithButton));
        }

        public string Caption
        {
            get => (string)GetValue(CaptionProperty);
            set => SetValue(CaptionProperty, value);
        }


        public string ButtonName
        {
            get => (string)GetValue(ButtonNameProperty);
            set => SetValue(ButtonNameProperty, value);
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public string ButtonText
        {
            get { return BtnOption.Content.ToString(); }
            set { BtnOption.Content = value; }
        }

        public bool VisibilityButton
        {
            get { return BtnOption.Visibility == Visibility.Visible; }
            set
            {
                if (value)
                    BtnOption.Visibility = Visibility.Visible;
                else
                    BtnOption.Visibility = Visibility.Hidden;
            }
        }

        private void tbText_TextChanged(object sender, TextChangedEventArgs e)
        {
            Text = TbText.Text;
        }

        private void BtnOption_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonAction?.Invoke();
        }
    }
}