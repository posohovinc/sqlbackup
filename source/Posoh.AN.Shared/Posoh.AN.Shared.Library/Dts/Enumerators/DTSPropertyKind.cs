﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSPropertyKind
  {
    Other,
    VariableReadOnly,
    VariableReadWrite,
    Connection,
    Sensitive
  }
}