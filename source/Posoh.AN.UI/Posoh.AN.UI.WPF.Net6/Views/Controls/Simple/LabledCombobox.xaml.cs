﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for LabledCombobox.xaml
    /// </summary>
    public partial class LabledCombobox : UserControl
    {
        public static readonly DependencyProperty ItemsSourceProperty;
        public static readonly DependencyProperty SelectedValueProperty;
        
        public LabledCombobox()
        {
            InitializeComponent();
            Binding binding = new Binding("ItemsSource");
            binding.Source = this;
            cbItems.SetBinding(ItemsControl.ItemsSourceProperty, binding);        
            cbItems.SetBinding(Selector.SelectedValueProperty, new Binding("SelectedValue"){Source = this});

        }
        
        static LabledCombobox()
        {
            ItemsSourceProperty = DependencyProperty.Register(
                "ItemsSource",
                typeof(IEnumerable),
                typeof(LabledCombobox));
            
            SelectedValueProperty = DependencyProperty.Register(
                "SelectedValue",
                typeof(object),
                typeof(LabledCombobox));
        }
        
        public RoutedEventArgs OnClick;
        public string Caption
        {
            get
            {
                return tblCaption.Text;
            }
            set
            {
                tblCaption.Text = value;
            }
        }

        //public bool VisibleButton
        //{
        //    //TODO hide last grid
        //}

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            //TODO delegate
        }
        
        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }
        
        public object SelectedValue
        {
            get { return GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }
    }
}
