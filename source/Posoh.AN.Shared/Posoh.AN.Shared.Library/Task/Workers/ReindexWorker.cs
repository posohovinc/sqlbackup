using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;
using Posoh.AN.Shared.Library.Task.Objects;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class ReindexWorker: MaintenanceWorker
  {

    #region fields
    private bool advKeepOnline;
    private bool advOfflineUnsupportedIndex;
    private bool advSortInTempdb;
    private IndexStats indexStats = new IndexStats(30.0);
    private AbortAfterWait lowPriorityAbortAfterWait = AbortAfterWait.None;
    private int lowPriorityMaxDuration;
    private bool lowPriorityUsed;
    private int maximumDegreeOfParallelism = 1;
    private bool maximumDegreeOfParallelismUsed;
    private bool padIndex = true;
    private int reindexPercentage = 20;
    private bool reindexWithOriginalAmount = true;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_ADV_KEEP_ONLINE)]
    public bool AdvKeepOnline
    {
      get => advKeepOnline;
      set 
      { 
        if (advKeepOnline != value)
        {
          advKeepOnline = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_ADV_OFFLINE_UNSUPPORTED_INDEX)]
    public bool AdvOfflineUnsupportedIndex
    {
      get => advOfflineUnsupportedIndex;
      set 
      { 
        if (advOfflineUnsupportedIndex != value)
        {
          advOfflineUnsupportedIndex = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_ADV_SORT_IN_TEMPDB)]
    public bool AdvSortInTempdb
    {
      get => advSortInTempdb;
      set 
      { 
        if (advSortInTempdb != value)
        {
          advSortInTempdb = value;
          OnPropertyChanged();
        }
      }
    }

    //[XmlEmbedded]
    //[XmlOrderedAttribute(1030, Constants.PERSIST_DB_MAINTENANCE_INDEX_STATS)]
    public IndexStats IndexStats
    {
      get => indexStats;
      set 
      { 
        if (indexStats != value)
        {
          indexStats = value;
          OnPropertyChanged();
        }
      }
    }
    #region map IndexStats
    [XmlOrderedAttribute(5010, Constants.PERSIST_FRAGMENTATION_OP)]
    public FragmentationOption FragmentationOp
    {
      get => indexStats.FragmentationOp;
      set => indexStats.FragmentationOp = value;
    }
    [XmlOrderedAttribute(5020, Constants.PERSIST_FRAGMENTATION_PCT)]
    public double FragmentationPct
    {
      get => indexStats.FragmentationPct;
      set => indexStats.FragmentationPct = value;
    }
    [XmlOrderedAttribute(5030, Constants.PERSIST_FRAGMENTATION_PCT_USED)]
    public bool FragmentationPctUsed
    {
      get => indexStats.FragmentationPctUsed;
      set => indexStats.FragmentationPctUsed = value;
    }
    [XmlOrderedAttribute(5040, Constants.PERSIST_LAST_USED_IN_DAYS)]
    public double LastUsedInDays
    {
      get => indexStats.LastUsedInDays;
      set => indexStats.LastUsedInDays = value;
    }
    [XmlOrderedAttribute(5050, Constants.PERSIST_LAST_USED_IN_DAYS_USED)]
    public bool LastUsedInDaysUsed
    {
      get => indexStats.LastUsedInDaysUsed;
      set => indexStats.LastUsedInDaysUsed = value;
    }
    [XmlOrderedAttribute(5060, Constants.PERSIST_PAGE_COUNT)]
    public long PageCount
    {
      get => indexStats.PageCount;
      set => indexStats.PageCount = value;
    }
    [XmlOrderedAttribute(5070, Constants.PERSIST_PAGE_COUNT_USED)]
    public bool PageCountUsed
    {
      get => indexStats.PageCountUsed;
      set => indexStats.PageCountUsed = value;
    }
    #endregion

    [XmlOrderedAttribute(1070, Constants.PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM)]
    public int MaximumDegreeOfParallelism
    {
      get => maximumDegreeOfParallelism;
      set 
      { 
        if (maximumDegreeOfParallelism != value)
        {
          maximumDegreeOfParallelism = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1080, Constants.PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM_USED)]
    public bool MaximumDegreeOfParallelismUsed
    {
      get => maximumDegreeOfParallelismUsed;
      set 
      { 
        if (maximumDegreeOfParallelismUsed != value)
        {
          maximumDegreeOfParallelismUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1090, Constants.PERSIST_PAD_INDEX)]
    public bool PadIndex
    {
      get => padIndex;
      set 
      { 
        if (padIndex != value)
        {
          padIndex = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1100, Constants.PERSIST_REINDEX_PERCENTAGE)]
    public int ReindexPercentage
    {
      get => reindexPercentage;
      set 
      { 
        if (reindexPercentage != value)
        {
          reindexPercentage = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1110, Constants.PERSIST_REINDEX_WITH_ORIGINAL_AMOUNT)]
    public bool ReindexWithOriginalAmount
    {
      get => reindexWithOriginalAmount;
      set 
      { 
        if (reindexWithOriginalAmount != value)
        {
          reindexWithOriginalAmount = value;
          OnPropertyChanged();
        }
      }
    }

    [XmlOrderedAttribute(1040, Constants.PERSIST_LOW_PRIORITY_ABORT_AFTER_WAIT)]
    public AbortAfterWait LowPriorityAbortAfterWait
    {
      get => lowPriorityAbortAfterWait;
      set
      {
        if (lowPriorityAbortAfterWait != value)
        {
          lowPriorityAbortAfterWait = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1050, Constants.PERSIST_LOW_PRIORITY_MAX_DURATION)]
    public int LowPriorityMaxDuration
    {
      get => lowPriorityMaxDuration;
      set
      {
        if (lowPriorityMaxDuration != value)
        {
          lowPriorityMaxDuration = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1060, Constants.PERSIST_LOW_PRIORITY_USED)]
    public bool LowPriorityUsed
    {
      get => lowPriorityUsed;
      set
      {
        if (lowPriorityUsed != value)
        {
          lowPriorityUsed = value;
          OnPropertyChanged();
        }
      }
    }


    public override bool TaskAllowesTableSelection => true;
  }
}
