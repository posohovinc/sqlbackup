﻿using System;
using System.Collections;
using System.Text.Json;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter.ObjectConverter
{
  public class SerializeJsonObjectConverter: SerializeObjectConverter
  {
    #region Write Property
    protected override void WriteDictionary(Serializer sender, IDictionary dictionary, Type typeKey, Type typeValue, ReflectionClient client, object ancestor)
    {
      using var jsonDoc = JsonDocument.Parse("");
    }

    protected override void WriteArray(Serializer sender, IList enumerable, Type typeArg, ReflectionClient client, object ancestor)
    {
    }
    #endregion
    #region Read Property

    protected override void ReadArray(Serializer sender, IList value, Type typeArg, ReflectionClient client, object ancestor)
    {
      throw new NotImplementedException();
    }

    protected override void ReadDictionary(Serializer sender, IDictionary value, Type typeArg, Type type, ReflectionClient client,
      object ancestor)
    {
      throw new NotImplementedException();
    }

    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      return base.StringToValue(sender, stringValue, ref value, graph, client, ancestor);
    }
    #endregion
  }

}
