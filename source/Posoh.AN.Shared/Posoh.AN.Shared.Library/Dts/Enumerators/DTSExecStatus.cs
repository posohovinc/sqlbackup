﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSExecStatus
  {
    None = 1,
    Validating = 2,
    Executing = 3,
    Suspended = 4,
    Completed = 5,
    Abend = 6,
  }
}