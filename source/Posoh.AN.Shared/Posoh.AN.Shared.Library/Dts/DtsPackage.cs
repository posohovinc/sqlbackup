﻿using System;
using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsPackage : DtsEventsProvider
  {
    public long CertificateContext { get; set; }
//    public X509Certificate CertificateObject { get; set; }
    public string CheckpointFileName { get; set; }
    //public DTSCheckpointUsage CheckpointUsage { get; set; }
    public bool CheckSignatureOnLoad { get; set; }
    public DtsConfigurations Configurations { get; }
    public DtsConnections Connections { get; }
    public DateTime CreationDate { get; set; }
    public string CreatorComputerName { get; set; }
    public string CreatorName { get; set; }
    //public IDTSEvents DesignEvents { get; set; }
    public string DesignTimeProperties { get; set; }
    public string DumpDescriptor { get; set; }
    public bool DumpOnAnyError { get; set; }
    public bool EnableConfigurations { get; set; }
    public bool EnableDump { get; set; }
    public bool EncryptCheckpoints { get; set; }
    public DtsErrors Errors { get; }
    public DtsExecutables Executables { get; }
    public DtsExtendedProperties ExtendedProperties { get; }
    public bool FailPackageOnFailure { get; set; }
    public bool HasExpressions { get; }
    public bool IgnoreConfigurationsOnLoad { get; set; }
    public bool InteractiveMode { get; set; }
    //public LogProviders LogProviders { get; }
    public int MaxConcurrentExecutables { get; set; }
    public bool OfflineMode { get; set; }
    internal string PackageLocation { get; }
    //public string PackagePassword { get; set; }
    public DTSPriorityClass PackagePriorityClass { get; set; }
    public DTSPackageType PackageType { get; set; }
    //public PackageUpgradeOptions PackageUpgradeOptions { get; set; }
    public DtsParameters Parameters { get; }
    public DtsPrecedenceConstraints PrecedenceConstraints { get; }
    //public IDTSProject100 Project { get; }
    public DtsProperties Properties { get; }
    public DTSProtectionLevel ProtectionLevel { get; set; }
    public bool SafeRecursiveProjectPackageExecution { get; set; }
    public bool SaveCheckpoints { get; set; }
    public bool SuppressConfigurationWarnings { get; set; }
    internal DTSTargetServerVersion TargetServerVersion { get; set; }
    public bool UpdateObjects { get; set; }
    public int VersionBuild { get; set; }
    public string VersionComments { get; set; }
    public string VersionGUID { get; }
    public int VersionMajor { get; set; }
    public int VersionMinor { get; set; }
    public DtsWarnings Warnings { get; }
  }
}