﻿using System;
using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsProperty: DtsObject
  {
    public string ConnectionType { get; }
    public bool Get { get; }
    public string Name { get; }
    public DTSPropertyKind PropertyKind { get; }
    public bool Set { get; }
    public TypeCode Type { get; }
    public string TypeConverter { get; }
    public string UITypeEditor { get; }
  }
}