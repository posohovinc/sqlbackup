﻿using System.Windows;
using Posoh.AN.Shared.Library.Task.Workers;
using Posoh.AN.UI.VM.Worker;
using Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker;

namespace Posoh.AN.UI.WPF.Net6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Vm = new MainWindowViewModel();
            DataContext = Vm;
        }
        private MainWindowViewModel Vm { get; set; }

        private void Cleanup_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new CleanupWorkerDialog
                {
                    Vm = new CleanupWorkerViewModel(Vm.ConnectionsToBase) { Worker = new FileCleanupWorker() },
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void ExecuteJob_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new ExecuteSQLServerAgentJobWorkerDialog
                {
                    Vm = new ExecuteSQLServerAgentJobWorkerViewModel(Vm.ConnectionsToBase){Worker = new ExecuteAgentJobWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void CheckDb_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new CheckIntegrityWorkerDialog
                {
                    Vm = new CheckIntegrityWorkerViewModel(Vm.ConnectionsToBase){Worker = new CheckIntegrityWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void HistoryCleanup_Click(object sender, RoutedEventArgs e)
        {
            // var dialog = new NotifyOperatorWorkerDialog
            // {
            //     Vm = new NotifyOperatorWorkerViewModel(){Worker = new NotifyOperatorWorker()},
            //     Owner = this
            // };
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new HistoryCleanupWorkerDialog()
                {
                    Vm = new HistoryCleanupWorkerViewModel(Vm.ConnectionsToBase) { Worker = new HistoryCleanupWorker() },
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void Rebuild_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new RebuildIndexWorkerDialog
                {
                    Vm = new RebuildIndexWorkerViewModel(Vm.ConnectionsToBase){Worker = new ReindexWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void Reorganize_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new ReorganizeIndexWorkerDialog
                {
                    Vm = new ReorganizeIndexWorkerViewModel(Vm.ConnectionsToBase){Worker = new ReorganizeIndexWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void Shrink_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new ShrinkDataBaseWorkerDialog
                {
                    Vm = new ShrinkDataBaseWorkerViewModel(Vm.ConnectionsToBase){Worker = new ShrinkWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }

        private void Execute_TSQL_Click(object sender, RoutedEventArgs e)
        {
            if (Vm.ConnectionsToBase != null)
            {
                var dialog = new ExecuteSqlStatementWorkerDialog
                {
                    Vm = new ExecuteSqlStatementWorkerViewModel(Vm.ConnectionsToBase){Worker = new TSQLExecuteWorker()},
                    Owner = this
                };
                dialog.ShowDialog();
            }
        }
    }
}