﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum TableSelection
  {
    None = 0,
    All = 1,
    Specific = 2
  }
}