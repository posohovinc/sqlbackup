﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum BackupEncryptionAlgorithm
  {
    Aes128,
    Aes192,
    Aes256,
    TripleDes
  }
}