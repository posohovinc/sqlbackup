﻿using System.Windows;
using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for DoubleUpDownLable.xaml
    /// </summary>
    public partial class DoubleUpDownLable : UserControl
    {
        public DoubleUpDownLable()
        {
            InitializeComponent();
        }

        public string UnitsToolTip
        {
            get => tbTextAfterTextBox.Text;
            set => tbTextAfterTextBox.Text = value;
        }

        public bool VisibilityUnitsToolTip
        {
            get => tbTextAfterTextBox.Visibility == Visibility.Visible;
            set => tbTextAfterTextBox.Visibility = value ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
