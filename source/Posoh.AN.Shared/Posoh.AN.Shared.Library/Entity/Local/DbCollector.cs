﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Posoh.AN.Shared.Library.Entity.Local
{
    public class DbCollector
    {
        public CoreConnection Connection { get; set; }
        public Guid CoreConnectionId { get; set; }
        public string ServerElement { get; set; }
    }
}
