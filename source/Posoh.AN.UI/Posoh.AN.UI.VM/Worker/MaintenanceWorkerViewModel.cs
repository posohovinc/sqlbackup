﻿using System.Collections.Generic;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.Shared.Library.Task.Workers;

namespace Posoh.AN.UI.VM.Worker
{
    public class MaintenanceWorkerViewModel : WorkerViewModelBase<MaintenanceWorker>
    {
        public MaintenanceWorkerViewModel(IList<CoreConnection> connections) : base(connections)
        {
        }
    }
}
