using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class UpdateStatisticsWorker : MaintenanceWorker
  {

    #region fields
    private int updateSampleValue = 50;
    private StatisticsScanType updateScanType = StatisticsScanType.FullScan;
    private StatisticsTarget updateType = StatisticsTarget.All;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_UPDATE_SAMPLE_VALUE)]
    public int UpdateSampleValue
    {
      get => updateSampleValue;
      set 
      { 
        if (updateSampleValue != value)
        {
          updateSampleValue = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_UPDATE_SCAN_TYPE)]
    public StatisticsScanType UpdateScanType
    {
      get => updateScanType;
      set 
      { 
        if (updateScanType != value)
        {
          updateScanType = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_UPDATE_TYPE)]
    public StatisticsTarget UpdateType
    {
      get => updateType;
      set 
      { 
        if (updateType != value)
        {
          updateType = value;
          OnPropertyChanged();
        }
      }
    }

    public override bool TaskAllowesDatbaseSelection => true;
  }
}
