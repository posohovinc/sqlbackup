﻿using Posoh.AN.Shared.Library.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Text.Json.Serialization;
using Posoh.AN.Shared.Library.DataBaseCollector;
using Posoh.AN.Shared.Library.Security;

namespace Posoh.AN.Shared.Library.Entity.Local
{
    [Serializable]
    public class CoreConnection : Element
    {
        #region Fields

        private Guid _serverId;
        private string _name;
        private string _instance;
        private int _authType;
        private string? _userName;
        private string? _password;
        private int _refCount;
        [NonSerialized, NotPersistent]
        private SqlConnectionStringBuilder _builder;
        [NonSerialized, NotPersistent]
        private SqlConnection _connection;
        [NonSerialized, NotPersistent]
        private string? _dataBase;
        [NonSerialized, NotPersistent]
        private QueryHelper _connectionHelper;
        [NonSerialized, NotPersistent]
        public readonly object SyncRoot = new object();

        #endregion

        public CoreConnection()
        {
            _builder = new SqlConnectionStringBuilder
            {
                InitialCatalog = "master",
                ConnectTimeout = 0,
                Pooling = false,
                MultipleActiveResultSets = false,
                Encrypt = false,
                TrustServerCertificate = false,
                PacketSize = 4096
            };
        }

        #region Properties

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ServerId
        {
            get { return _serverId; }
            set
            {
                if (_serverId == value) return;
                _serverId = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Connection Name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Server Name
        /// </summary>
        public string Instance
        {
            get { return _instance; }
            set
            {
                if (_instance == value) return;
                _instance = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// 0 - Windows Auth
        /// 1 - SQL server Auth
        /// 2 - Azure AD Universal with MFA
        /// 3 - Azure AD Password
        /// 4 - Azure AD Integrated
        /// </summary>
        public int AuthType
        {
            get { return _authType; }
            set
            {
                if (_authType == value) return;
                _authType = value;
                OnPropertyChanged();
            }
        }
        public string? UserName
        {
            get { return _userName; }
            set
            {
                if (_userName == value) return;
                _userName = value;
                OnPropertyChanged();
            }
        }
        public string? Password
        {
            get { return _password; }
            set
            {
                if (_password == value) return;
                _password = value;
                OnPropertyChanged();
            }
        }
        public string? Database
        {
            get => string.IsNullOrEmpty(_dataBase) ? "master" : _dataBase;
            set
            {
                value ??= string.Empty;
                if (string.Compare(_dataBase, value, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    _dataBase = value;
                    ResetConnection();
                }
            }
        }

        [JsonIgnore]
        public bool IsOpen => (ConnectionState.Open == (ConnectionState.Open & _connection.State));

        [JsonIgnore]
        public QueryHelper QueryHelper => _connectionHelper ??= new QueryHelper(this);
        [JsonIgnore]
        public SqlConnection Connection
        {
            get
            {
                lock (SyncRoot)
                {
                    if (_connection == null)
                    {
                        if (_builder == null)
                        {
                            _builder = new SqlConnectionStringBuilder()
                            {
                                Pooling = false,
                                MultipleActiveResultSets = false,
                                Encrypt = false,
                                TrustServerCertificate = false,
                                PacketSize = 4096
                            };
                        }

                        _builder.IntegratedSecurity = AuthType == 1;
                        if (_builder.IntegratedSecurity)
                        {
                            _builder.UserID = _userName;
                            _builder.Password = _builder.Password = EncryptionSys.DecryptString(_password);
                        }
                        _builder.ConnectTimeout = 15;
                        _builder.DataSource = Instance;
                        _builder.InitialCatalog = Database;
                        _connection = new SqlConnection(_builder.ConnectionString);
                    }
                }
                return _connection;
            }
        }

        #endregion

        #region Methods
        public override string ToString()
        {
            return Name;
        }

        #region Misc
        internal void ResetConnection()
        {
            lock (SyncRoot)
            {
                if (_connection != null)
                {
                    _refCount = 1;
                    Disconnect();
                    _connection.Dispose();
                    _connection = null;
                    _connectionHelper = null;
                }
            }
        }

        public void Connect()
        {
            lock (SyncRoot)
            {
                if (_refCount == 0 || !IsOpen)
                {
                    try
                    {
                        if (!IsOpen)
                            Connection.Open();
                        _refCount = 0;
                    }
                    catch (Exception exception)
                    {
                        ResetConnection();
                        if (exception.InnerException != null)
                            throw exception.InnerException;
                        throw;
                    }
                }
                _refCount++;
            }
        }
        public void Disconnect()
        {
            lock (SyncRoot)
            {
                if (_refCount > 0)
                {
                    _refCount--;
                    if (_refCount == 0)
                    {
                        if (IsOpen) Connection.Close();
                    }
                }
            }
        }

        #region static method
        internal static SqlCommand GetSqlCommand(string query, SqlConnection thisConnection)
        {
            var result = new SqlCommand(query, thisConnection) { CommandTimeout = thisConnection.ConnectionTimeout };
            return result;
        }
        internal static CoreSafeReader ExecuteReader(string query, SqlConnection thisConnection)
        {
            if (ConnectionState.Open != (ConnectionState.Open & thisConnection.State))
                thisConnection.Open();
            return new CoreSafeReader(GetSqlCommand(query, thisConnection).ExecuteReader());
        }
        #endregion
        internal SqlCommand GetSqlCommand(string query)
        {
            return GetSqlCommand(query, _connection);
        }
        internal object ExecuteScalar(string query)
        {
            object result;
            lock (SyncRoot)
            {
                Connect();
                try
                {
                    result = GetSqlCommand(query).ExecuteScalar();
                }
                finally
                {
                    Disconnect();
                }
            }
            return result;
        }
        internal CoreSafeReader ExecuteReader(string query)
        {
            if (!IsOpen) Connect();
            return new CoreSafeReader(GetSqlCommand(query).ExecuteReader());
        }
        internal int ExecuteNonQuery(string query)
        {
            int result;
            lock (SyncRoot)
            {
                Connect();
                try
                {
                    result = GetSqlCommand(query).ExecuteNonQuery();
                }
                finally
                {
                    Disconnect();
                }
            }
            return result;
        }
        #endregion

        #endregion
    }
}