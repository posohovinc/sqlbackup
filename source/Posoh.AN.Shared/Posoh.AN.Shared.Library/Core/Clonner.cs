﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Posoh.AN.Shared.Library.Core
{
  public static class Clonner
  {
    public static object Clone(object obj)
    {
      using (var buffer = new MemoryStream())
      {
        var formatter = new BinaryFormatter();
        formatter.Serialize(buffer, obj);
        buffer.Position = 0;
        var clone = formatter.Deserialize(buffer);
        return clone;
      }
    }
  }
}