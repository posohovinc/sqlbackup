using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class ExecuteAgentJobWorker : MaintenanceWorker
  {

    #region fields
    private string agentJobId = string.Empty;
    #endregion

    [XmlOrderedAttribute(100, Constants.PERSIST_AGENT_JOB_ID)]
    public string AgentJobId
    {
      get => agentJobId;
      set 
      { 
        value ??= string.Empty;
        if (agentJobId != value)
        {
          agentJobId = value;
          OnPropertyChanged();
        }
      }
    }
    //[XmlOrderedAttribute(100, Constants.PERSIST_TASK_ALLOWES_DATBASE_SELECTION)]
    public override bool TaskAllowesDatbaseSelection => false;
    //[XmlOrderedAttribute(100, Constants.PERSIST_TASK_ALLOWES_TABLE_SELECTION)]
    public override bool TaskAllowesTableSelection => false;

  }
}
