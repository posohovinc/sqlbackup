﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSLoggingMode
  {
    UseParentSetting,
    Enabled,
    Disabled
  }
}