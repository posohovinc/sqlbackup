﻿using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Worker
{
    public class WorkerBaseControl<T>: UserControl
    {
        private T _workerViewModel;

        public T WorkerViewModel
        {
            get => _workerViewModel;
            set
            {
                _workerViewModel = value;
                if(_workerViewModel != null)
                {
                    DataContext = value;
                }
            }
        }
        
        public T Self
        {
            get => WorkerViewModel;
            set => WorkerViewModel = value;
        }


        protected virtual void OnInitialization(){}
    }
}
