﻿using System;
using System.ComponentModel;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsVariable: DtsObject
  {
    // Properties
    public string CreationName { get; }
    public TypeCode DataType { get; }
    public string Description { get; set; }
    public bool EvaluateAsExpression { get; set; }
    public string Expression { get; set; }
    public string ID { get; }
    public bool IncludeInDebugDump { get; set; }
    public string Name { get; set; }
    public string Namespace { get; set; }
    public DtsContainer Parent { get; }
    public DtsProperties Properties { get; }
    public string QualifiedName { get; }
    public bool RaiseChangedEvent { get; set; }
    public bool ReadOnly { get; set; }
    public ISite Site { get; set; }
    public bool SystemVariable { get; }
    public object Value { get; set; }
  }
}