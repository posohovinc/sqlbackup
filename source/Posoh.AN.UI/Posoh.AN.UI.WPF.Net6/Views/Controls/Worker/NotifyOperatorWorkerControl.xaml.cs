﻿namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Worker
{
    /// <summary>
    /// Interaction logic for NotifyOperatorTaskControl.xaml
    /// </summary>
    public partial class NotifyOperatorWorkerControl : AbstractNotifyOperatorWorkerControl
    {
        public NotifyOperatorWorkerControl()
        {
            InitializeComponent();
        }
    }
}
