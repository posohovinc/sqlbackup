﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;

namespace Posoh.AN.Shared.Library.Core
{
  public static class Reflection
  {
    private const BindingFlags bindingFlags =
      BindingFlags.NonPublic |
      BindingFlags.Instance |
      BindingFlags.Public;

    private static readonly Object SyncObj = new Object();
    private static readonly Dictionary<string, Dictionary<string, ReflectionClient>>
      propertyHash = new Dictionary<string, Dictionary<string, ReflectionClient>>();
    private static readonly Dictionary<string, List<ReflectionClient>> 
      propertyList = new Dictionary<string, List<ReflectionClient>>();

    private static string HashKey(Object target, Type graph)
    {
      return target.GetType().FullName + "&" + graph.FullName;
    }

    private static string HashKey<T>(object target)
    {
      return HashKey(target, typeof(T));
    }

    public static Dictionary<string, ReflectionClient> PropertyHash<T>(object target)
    {
      return PropertyHash(target, typeof(T));
    }

    public static Dictionary<string, ReflectionClient> PropertyHash(object target, Type keyAttributeType)
    {
      var key = HashKey(target, keyAttributeType);

      if (!propertyHash.TryGetValue(key, out var result))
      {
        lock (SyncObj)
        {
          if (!propertyHash.TryGetValue(key, out result))
          {
            result = new Dictionary<string, ReflectionClient>();

            var properties = target.GetType().GetMembers(bindingFlags);
            foreach (var property in properties)
            {
              switch (property.MemberType)
              {
                case MemberTypes.Field:
                case MemberTypes.Property:
                {
                  var assign = property.GetCustomAttributes(keyAttributeType, true);
                  foreach (Attribute attribute in assign)
                  {
                    if (attribute.GetType() == keyAttributeType)
                    {
                      var refClient = new ReflectionClient(property, attribute);
                      result.Add(refClient.Key, refClient);
                      break;
                    }
                  }
                  break;
                }
              }
            }
            propertyHash.Add(key, result);
          }
        }
      }

      return result;
    }

    public static List<ReflectionClient> PropertyList<T>(object target)
    {
      return PropertyList(target, typeof(T));
    }

    public static List<ReflectionClient> PropertyList(object target, Type graph)
    {
      var key = HashKey(target, graph);

      List<ReflectionClient> result;
      if (!propertyList.TryGetValue(key, out result))
      {
        lock (SyncObj)
        {
          if (!propertyList.TryGetValue(key, out result))
          {
            var hash = PropertyHash(target, graph);
            result = new List<ReflectionClient>(hash.Values);
            result.Sort();
            propertyList.Add(key, result);
          }
        }
      }

      return result;
    }

    #region constructor with param

    public static T Create<T>(Type type, params object[] args)
    {
      return (T) Create(type, args);
    }

    public static object Create(Type type, params object[] args)
    {
      try
      {
        return Activator.CreateInstance(type, args);
      }
      catch (TargetInvocationException e)
      {
        throw e.InnerException;
      }
    }

    #endregion
  }
}

