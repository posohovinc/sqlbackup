﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum TimeUnitType
  {
    Day = 0,
    Week = 1,
    Month = 2,
    Year = 3,
    Hour = 5,
  }
}