﻿
using Posoh.AN.Shared.Library.Serializer.Converter.ObjectConverter;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class ValueConverterFactory
  {
    public static SerializeValueConverter GetValueConverter(SerializeType serializeType)
    {
      var result = new SerializeValueConverter();
      switch (serializeType)
      {
        case SerializeType.Xml:
          result.RegisterConverter(typeof(object), new SerializeXmlObjectConverter());
          break;
        case SerializeType.Json:
          result.RegisterConverter(typeof(object), new SerializeJsonObjectConverter());
          break;
      }
      return result;
    }
  }
}
