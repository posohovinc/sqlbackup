﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for ShrinkDataBaseWorkerDialog.xaml
    /// </summary>
    public partial class ShrinkDataBaseWorkerDialog : AbstractShrinkDataBaseWorkerDialog
    {
        public ShrinkDataBaseWorkerDialog()
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            ShrinkDataBaseWorkerControl.Self = Vm;
        }
    }
}
