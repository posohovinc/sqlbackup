﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.Attributes
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]

  public class XmlOrderedAttributeAttribute : XmlAttributeAttribute, IOrdered
  {
    public XmlOrderedAttributeAttribute(string attributeName) : this(int.MaxValue, attributeName, null)
    {
    }
    public XmlOrderedAttributeAttribute(Type type) : this(int.MaxValue, string.Empty, type)
    {
    }
    public XmlOrderedAttributeAttribute(string attributeName, Type type) : this(int.MaxValue, attributeName, type)
    {
    }
    public XmlOrderedAttributeAttribute(int order) : this(order, string.Empty, null)
    {
    }
    public XmlOrderedAttributeAttribute(int order, string attributeName) : this(order, attributeName, null)
    {
    }
    public XmlOrderedAttributeAttribute(int order, string attributeName, Type type) : base(attributeName, type)
    {
      Order = order;
    }
    public override bool Equals(object? obj) => CompareTo(obj) == 0;

    public override string ToString()
    {
      return Key;
    }

    public override int GetHashCode()
    {
      return Order ^ Key.GetHashCode();
    }

    public int CompareTo(object? obj)
    {
      var result = 1;
      if (obj is IOrdered ordered)
      {
        result = Order - ordered.Order;
        if (result == 0)
          result = String.Compare(Key, ordered.Key, StringComparison.Ordinal);
      }

      return result;
    }

    public string Key => AttributeName;

    public int Order { get; }
  }
}
