﻿using System.Windows;
using System.Windows.Controls;
using Posoh.AN.UI.WPF.Net6.Views.Dialogs;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls
{
    public class DataBasePropDialogWindow: BaseSqlPropDialogWindow
    {
        public DataBasePropDialogWindow(string title) : base(title)
        {
            gridWithProp.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(20, 50, 0, 0),
                Text = "DataBase(s):",
                VerticalAlignment = VerticalAlignment.Top
            });
            gridWithProp.Children.Add(new ComboBox()
            {
                Margin = new Thickness(130, 47, 10, 0),
                VerticalAlignment = VerticalAlignment.Top
            });
        }
    }
}
