﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for ExecuteSQLServerAgentJobWorkerDialog.xaml
    /// </summary>
    public partial class ExecuteSQLServerAgentJobWorkerDialog : AbstractExecuteSQLServerAgentJobWorkerDialog
    {
        public ExecuteSQLServerAgentJobWorkerDialog()
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            ExecuteSqlServerAgentJobWorkerControl.Self = Vm;
        }
    }
}
