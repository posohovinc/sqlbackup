﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeBooleanConverter: SerializeConverter
  {
    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      if (int.TryParse(stringValue, out var intValue))
      {
        value = intValue != 0;
        return true;
      }
      else
      {
        return base.StringToValue(sender, stringValue, ref value, graph, client, ancestor);
      }
    }
  }
}
