﻿using System.Windows;
using System.Windows.Controls;
using Posoh.AN.UI.WPF.Net6.Views.Controls.Simple;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Primitive
{
    public class LabeledTextBox : LabeledBase
    {
        private TextBox tbText = new TextBox();
        private TextBlock tblUnits = new TextBlock();
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            tbText.Margin = new Thickness(10, 1, 10, 1);
            Grid.Children.Add(tbText);
            Grid.SetColumn(tbText, 1);

            tblUnits.Margin = new Thickness(10, 1, 10, 1);
            Grid.Children.Add(tblUnits);
            Grid.SetColumn(tblUnits, 2);

            base.OnContentChanged(oldContent, newContent);
        }

        public string Text
        {
            get => tbText.Text;
            set => tbText.Text = value;
        }
        public string UnitsToolTip
        {
            get => tblUnits.Text;
            set => tblUnits.Text = value;
        }
        public bool VisibilityUnitsToolTip
        {
            get => tblUnits.Visibility == Visibility.Visible;
            set => tblUnits.Visibility = value ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
