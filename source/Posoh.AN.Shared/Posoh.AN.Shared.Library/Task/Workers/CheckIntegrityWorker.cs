using System;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  public class CheckIntegrityWorker : MaintenanceWorker
  {

    #region fields
    private bool includeIndexes = true;
    private long maximumDegreeOfParallelism = 1L;
    private bool maximumDegreeOfParallelismUsed;
    private bool physicalOnly = true;
    private bool tablock;
    #endregion

    [XmlOrderedAttribute(1000, Constants.PERSIST_INCLUDE_INDEXES)]
    public bool IncludeIndexes
    {
      get => includeIndexes;
      set 
      { 
        if (includeIndexes != value)
        {
          includeIndexes = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1010, Constants.PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM)]
    public long MaximumDegreeOfParallelism
    {
      get => maximumDegreeOfParallelism;
      set 
      { 
        if (maximumDegreeOfParallelism != value)
        {
          maximumDegreeOfParallelism = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1020, Constants.PERSIST_MAXIMUM_DEGREE_OF_PARALLELISM_USED)]
    public bool MaximumDegreeOfParallelismUsed
    {
      get => maximumDegreeOfParallelismUsed;
      set 
      { 
        if (maximumDegreeOfParallelismUsed != value)
        {
          maximumDegreeOfParallelismUsed = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1030, Constants.PERSIST_PHYSICAL_ONLY)]
    public bool PhysicalOnly
    {
      get => physicalOnly;
      set 
      { 
        if (physicalOnly != value)
        {
          physicalOnly = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(1040, Constants.PERSIST_TABLOCK)]
    public bool Tablock
    {
      get => tablock;
      set 
      { 
        if (tablock != value)
        {
          tablock = value;
          OnPropertyChanged();
        }
      }
    }
  }
}
