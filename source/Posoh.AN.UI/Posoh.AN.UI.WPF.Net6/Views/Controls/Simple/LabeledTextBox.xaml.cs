﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for LabeledTextBox.xaml
    /// </summary>
    public partial class LabeledTextBox : LabeledBase
    {
        public static readonly DependencyProperty TextProperty;

        public LabeledTextBox()
        {
            InitializeComponent();
            Binding binding = new Binding("Text");
            binding.Source = this;
            tbText.SetBinding(TextBox.TextProperty, binding);
        }

        static LabeledTextBox()
        {
            TextProperty = DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(LabeledTextBox));
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }
        
        public string UnitsToolTip
        {
            get
            {
                return tbTextAfterTextBox.Text;
            }
            set
            {
                tbTextAfterTextBox.Text = value;
            }
        }
        public bool VisibilityUnitsToolTip
        {
            get
            {
                return tbTextAfterTextBox.Visibility == Visibility.Visible;
            }
            set
            {
                if (value)
                    tbTextAfterTextBox.Visibility = Visibility.Visible;
                else
                    tbTextAfterTextBox.Visibility = Visibility.Hidden;
            }
        }
    }
}
