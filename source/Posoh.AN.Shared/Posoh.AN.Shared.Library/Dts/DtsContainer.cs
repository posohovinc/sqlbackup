﻿using System;
using System.ComponentModel;
using System.Data;
using Microsoft.EntityFrameworkCore.Diagnostics.Internal;
using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsContainer: DtsExecutable
  {
    public string CreationName { get; }
    public bool DebugMode { get; set; }
    public bool DelayValidation { get; set; }
    public string Description { get; set; }
    public bool Disable { get; set; }
    public int ExecutionDuration { get; }
    public DTSExecResult ExecutionResult { get; }
    public DTSExecStatus ExecutionStatus { get; }
    public bool FailPackageOnFailure { get; set; }
    public bool FailParentOnFailure { get; set; }
    public object ForcedExecutionValue { get; set; }
    public DTSForcedExecResult ForceExecutionResult { get; set; }
    public bool ForceExecutionValue { get; set; }
    public string ID { get; }
    public bool IsDefaultLocaleID { get; }
    public IsolationLevel IsolationLevel { get; set; }
    public int LocaleID { get; set; }
    //public LogEntryInfos LogEntryInfos { get; }
    public DTSLoggingMode LoggingMode { get; set; }
    public LoggingOptions LoggingOptions { get; }
    public int MaximumErrorCount { get; set; }
    public string Name { get; set; }
    public DtsContainer Parent { get; }
    public ISite Site { get; set; }
    public DateTime StartTime { get; }
    public DateTime StopTime { get; }
    public bool SuspendRequired { get; set; }
    public DTSTransactionOption TransactionOption { get; set; }
    //public VariableDispenser VariableDispenser { get; }
    public DtsVariables Variables { get; }

  }
}