﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum DatabaseSelection
  {
    None = 0,
    All = 1,
    System = 2,
    User = 3,
    Specific = 4
  }
}