﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for NumericCombobox.xaml
    /// </summary>
    public partial class NumericCombobox : UserControl
    {
        public static readonly DependencyProperty ItemsSourceProperty;
        public static readonly DependencyProperty SelectedValueProperty;
        public static readonly DependencyProperty ValueProperty;
        
        public NumericCombobox()
        {
            InitializeComponent();

            DoubleUpDown.SetBinding(DoubleUpDown.ValueProperty, new Binding("Value"){Source = this});
            CbItems.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("ItemsSource"){Source = this});        
            CbItems.SetBinding(Selector.SelectedValueProperty, new Binding("SelectedValue"){Source = this});
        }

        static NumericCombobox()
        {
            ValueProperty = DependencyProperty.Register(
                "Value",
                typeof(double),
                typeof(NumericCombobox));
            
            ItemsSourceProperty = DependencyProperty.Register(
                "ItemsSource",
                typeof(IEnumerable),
                typeof(NumericCombobox));
            
            SelectedValueProperty = DependencyProperty.Register(
                "SelectedValue",
                typeof(object),
                typeof(NumericCombobox));
        }

        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }
        
        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }
        
        public object SelectedValue
        {
            get => GetValue(SelectedValueProperty);
            set => SetValue(SelectedValueProperty, value);
        }
    }
}
