﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Serializer.Converter;
using Posoh.AN.Shared.Library.Serializer.Enumerator;

namespace Posoh.AN.Shared.Library.Serializer
{
  [Serializable]
  public class SerializerXml : Serializer
  {
    public class Utf8StringWriter : StringWriter
    {
      public override Encoding Encoding => Encoding.UTF8;
    }

    [NonSerialized]
    private XmlDocument xmlDoc;
    [NonSerialized]
    private XmlNamespaceManager namespaceManager;

    public XmlDocument CreateXmlDocument()
    {
      var doc = new XmlDocument();
      var decl = doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
      doc.InsertBefore(decl, doc.DocumentElement);
      namespaceManager = new XmlNamespaceManager(doc.NameTable);
      namespaceManager.AddNamespace(Prefix, NamespaceURI);
      return doc;
    }

    public XmlAttribute CreateElementAttribute(object ancestor, string key, string attributeValue = "")
    {
      var node = ((XmlNode)ancestor);
      var result = node.OwnerDocument.CreateAttribute(Prefix, key, NamespaceURI);
      node.Attributes.Append(result);
      if (!string.IsNullOrEmpty(attributeValue))
      {
        result.Value = attributeValue;
      }

      return result;
    }

    public XmlNode CreateElement(object ancestor, string key, string elementValue = "")
    {
      XmlNode result;
      var parent = ((XmlNode)ancestor);
      if (parent == null)
      {
        result = xmlDoc.CreateElement(Prefix, key, NamespaceURI);
        xmlDoc.AppendChild(result);
      }
      else
      {
        result = parent.OwnerDocument.CreateElement(Prefix, key, NamespaceURI);
        parent.AppendChild(result);
      }
      if (!string.IsNullOrEmpty(elementValue))
      {
        result.InnerText = elementValue;
      }
      return result;
    }

    public SerializerXml(): base (SerializeType.Xml)
    {
      SerializeAttributes = new[] {typeof(XmlOrderedAttributeAttribute), typeof(XmlOrderedElementAttribute), typeof(XmlNodeValueAttribute) };
      Prefix = string.Empty;
      NamespaceURI = string.Empty;
    }

    private void SerializeGraph(object graph)
    {
      var graphType = graph.GetType();
      var rootAttribute = graphType.GetCustomAttribute<XmlRootAttribute>();
      var elementName = (rootAttribute != null) ? rootAttribute.ElementName : graphType.FullName;
      var rootNode = CreateElement(null, elementName);

      Serialize(graph, rootNode);

    }

    public override void Serialize(object graph, out string result)
    {
      xmlDoc = CreateXmlDocument();
      SerializeGraph(graph);
      var stringWriter = new Utf8StringWriter();
      xmlDoc.Save(stringWriter);
      result = stringWriter.ToString();
    }

    public override void Serialize(TextWriter streamWriter, object graph)
    {
      Serialize(graph, out string xml);


      using (TextReader tr = new StringReader(xml))
      {
        xmlDoc.Load(tr);
      }

      using (var xmlTextWriter = new XmlTextWriter(streamWriter))
      {
        xmlTextWriter.Formatting = Formatting.Indented;
        xmlTextWriter.IndentChar = '\t';
        xmlTextWriter.Indentation = 1;

        xmlDoc.WriteTo(xmlTextWriter);
        xmlTextWriter.Flush();
      }
    }


    public override void AddPair(object ancestor, object value, string key, string stringValue)
    {
      if (CurrentSerializeAttribute == typeof(XmlOrderedAttributeAttribute))
      {
        CreateElementAttribute(ancestor, key, stringValue);
      }
      else if (CurrentSerializeAttribute == typeof(XmlNodeValueAttribute))
      {
        ((XmlNode)ancestor).InnerText = stringValue;
      }
      else
      {
         CreateElement(ancestor, key, stringValue);
      }
    }

    protected override SerializeIterator GetIterator(Type serializeAttributes, object ancestor)
    {
      if (serializeAttributes == typeof(XmlOrderedAttributeAttribute))
      {
        return new SerializeXmlAttributeIterator((XmlNode)ancestor);
      }
      else
      {
        return new SerializeXmlIterator((XmlNode)ancestor);
      }
    }

    public override void Deserialize(object graph, string data)
    {
      xmlDoc = new XmlDocument();
      using TextReader tr = new StringReader(data);
      xmlDoc.Load(tr);
      Deserialize(graph, xmlDoc.DocumentElement);
    }

    public string Prefix { get; set; }
    public string NamespaceURI { get; set; }
  }
}
