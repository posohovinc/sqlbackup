declare @object_id  bigint = @id

declare 
   @type varchar(2)
  ,@lf varchar(2) = char(13)+ char(10)
  ,@tab varchar(2) = '  '
  ,@object_name sysname
  ,@file_name   sysname
  ,@definition varchar(max) = ''

select @type = o.[type]
from sys.objects o 
where o.[object_id] = @object_id

if @type = 'U'
begin

	select
	  @object_name = '[' + s.name + '].[' + o.name + ']'
	 ,@file_name = s.name + '.' + o.name + '.sql'
	 ,@object_id = o.[object_id]
	from
	  sys.objects o with (nowait)
		join sys.schemas s with (nowait) on o.[schema_id] = s.[schema_id]
	where
		  o.[object_id] = @object_id
	  and o.[type] = 'U'
	  and o.is_ms_shipped = 0

	declare @SQL nvarchar(max) = ''

	;
	with index_column
	as
	(
	  select
		ic.[object_id]
	   ,ic.index_id
	   ,ic.is_descending_key
	   ,ic.is_included_column
	   ,c.name
	  from
		sys.index_columns ic with (nowait)
		  join sys.columns c with (nowait) on ic.[object_id] = c.[object_id]
			  and ic.column_id = c.column_id
	  where
		ic.[object_id] = @object_id
	),
	fk_columns
	as
	(
	  select
		k.constraint_object_id
	   ,cname =  c.name
	   ,rcname = rc.name
	  from
		sys.foreign_key_columns k with (nowait)
		  join sys.columns rc with (nowait) on rc.[object_id] = k.referenced_object_id
			  and rc.column_id = k.referenced_column_id
		  join sys.columns c with (nowait) on c.[object_id] = k.parent_object_id
			  and c.column_id = k.parent_column_id
	  where
		k.parent_object_id = @object_id
	)
	select
	  @SQL = 'create table ' + @object_name + @lf + '(' + @lf + stuff((
		select
		  @tab + ',[' + c.name + '] ' +
		  case
			 when c.is_computed = 1 then 'as ' + cc.[definition]
			else tp.name +
			  case
				when tp.name in ('varchar', 'char', 'varbinary', 'binary', 'text') then '(' +
				  case
					when c.max_length = -1 then 'max'
					else cast(c.max_length as varchar(5))
				  end + ')'
				when tp.name in ('nvarchar', 'nchar', 'ntext') then '(' +
				  case
					when c.max_length = -1 then 'max'
					else cast(c.max_length / 2 as varchar(5))
				  end + ')'
				when tp.name in ('datetime2', 'time2', 'datetimeoffset') then '(' + cast(c.scale as varchar(5)) + ')'
				when tp.name = 'decimal' then '(' + cast(c.[precision] as varchar(5)) + ',' + cast(c.scale as varchar(5)) + ')'
				else ''
			  end +
        /*
			  case
				when c.collation_name is not null then ' COLLATE ' + c.collation_name
				else ''
			  end +
        */
			  case
				--when c.is_nullable = 1 then ' null'
				when c.is_nullable = 1 then ''
				else ' not null'
			  end +
			  case
				when dc.[definition] is not null then ' default' + dc.[definition]
				else ''
			  end +
			  case
				when ic.is_identity = 1 then ' identity(' + cast(isnull(ic.seed_value, '0') as char(1)) + ',' + cast(isnull(ic.increment_value, '1') as char(1)) + ')'
				else ''
			  end
		  end + @lf  
		from
		  sys.columns c with (nowait)
			join sys.types tp with (nowait) on c.user_type_id = tp.user_type_id
			left join sys.computed_columns cc with (nowait) on c.[object_id] = cc.[object_id]
				and c.column_id = cc.column_id
			left join sys.default_constraints dc with (nowait) on c.default_object_id != 0
				and c.[object_id] = dc.parent_object_id
				and c.column_id = dc.parent_column_id
			left join sys.identity_columns ic with (nowait) on c.is_identity = 1
				and c.[object_id] = ic.[object_id]
				and c.column_id = ic.column_id
		where
		  c.[object_id] = @object_id
		order by
		  c.column_id
		for xml path (''), type
	  ).value('.', 'nvarchar(max)'), 1, 2, @tab + ' ')
	  + isnull((
		select
		  @tab + ',constraint [' + k.name + '] primary key (' + (
			select
			  stuff((
				select
				  ', [' + c.name + '] ' +
				  case
					when ic.is_descending_key = 1 then 'desc'
					else 'asc'
				  end
				from
				  sys.index_columns ic with (nowait)
					join sys.columns c with (nowait) on c.[object_id] = ic.[object_id]
						and c.column_id = ic.column_id
				where
				  ic.is_included_column = 0
				  and ic.[object_id] = k.parent_object_id
				  and ic.index_id = k.unique_index_id
				for xml path (N''), type
			  ).value('.', 'nvarchar(max)'), 1, 2, '')
		  ) + ')' + @lf
		from
		  sys.key_constraints k with (nowait)
		where
		  k.parent_object_id = @object_id
		  and k.[type] = 'PK'
	  ), '') + ')' + @lf+ 'GO' + @lf
	  + isnull((
		select
		  (
			select
			  @lf +
			  'alter table ' + @object_name + ' with'
			  +
			  case
				when fk.is_not_trusted = 1 then ' nocheck'
				else ' check'
			  end +
			  ' add constraint [' + fk.name + '] foreign key('
			  + stuff((
				select
				  ', [' + k.cname + ']'
				from
				  fk_columns k
				where
				  k.constraint_object_id = fk.[object_id]
				for xml path (''), type
			  ).value('.', 'nvarchar(max)'), 1, 2, '')
			  + ')' +
			  ' references [' + schema_name(ro.[schema_id]) + '].[' + ro.name + '] ('
			  + stuff((
				select
				  ', [' + k.rcname + ']'
				from
				  fk_columns k
				where
				  k.constraint_object_id = fk.[object_id]
				for xml path (''), type
			  ).value('.', 'nvarchar(max)'), 1, 2, '')
			  + ')'
			  +
			  case
				when fk.delete_referential_action = 1 then ' ON DELETE CASCADE'
				when fk.delete_referential_action = 2 then ' ON DELETE SET null'
				when fk.delete_referential_action = 3 then ' ON DELETE SET default'
				else ''
			  end
			  +
			  case
				when fk.update_referential_action = 1 then ' ON UPDATE CASCADE'
				when fk.update_referential_action = 2 then ' ON UPDATE SET null'
				when fk.update_referential_action = 3 then ' ON UPDATE SET default'
				else ''
			  end
			  + @lf + 'alter table ' + @object_name + ' check constraint [' + fk.name + ']' + @lf
			from
			  sys.foreign_keys fk with (nowait)
				join sys.objects ro with (nowait) on ro.[object_id] = fk.referenced_object_id
			where
			  fk.parent_object_id = @object_id
			for xml path (N''), type
		  ).value('.', 'nvarchar(max)')
	  ), '')
	  + isnull(((
		select
		  @lf + 'create' +
		  case
			when i.is_unique = 1 then ' unique'
			else ''
		  end
		  + ' nonclustered index [' + i.name + '] ON ' + @object_name + ' (' +
		  stuff((
			select
			  ', [' + c.name + ']' +
			  case
				when c.is_descending_key = 1 then ' desc'
				-- else ' asc'
			  end
			from
			  index_column c
			where
			  c.is_included_column = 0
			  and c.index_id = i.index_id
			for xml path (''), type
		  ).value('.', 'nvarchar(max)'), 1, 2, '') + ')'
		  + isnull(@lf + 'include (' +
		  stuff((
			select
			  ', [' + c.name + ']'
			from
			  index_column c
			where
			  c.is_included_column = 1
			  and c.index_id = i.index_id
			for xml path (''), type
		  ).value('.', 'nvarchar(max)'), 1, 2, '') + ')', '') + @lf
		from
		  sys.indexes i with (nowait)
		where
		  i.[object_id] = @object_id
		  and i.is_primary_key = 0
		  and i.[type] = 2
		for xml path (''), type
	  ).value('.', 'nvarchar(max)')
	  ), '')

	select @definition = stuff(@SQL, charindex(',', @SQL), 1, '')

end
else
begin
	select
	  @file_name = schema_name(o.schema_id) + '.' + o.name + '.sql'
	 ,@definition = m.definition + @lf
	from
	  sys.sql_modules m
		join sys.objects o on m.object_id = o.object_id
	where
	  m.object_id = @object_id
end


select 
  @definition = @definition 
  + 'exec sys.sp_addextendedproperty N''' + ep.name
  + ''', N''' + cast(value as varchar(max))
  + ''', N''SCHEMA' 
  + ''', N''' + iif(ob.parent_object_id > 0, object_schema_name(ob.parent_object_id), object_schema_name(ob.object_id))
  + ''', N''' +
  case
    when ob.parent_object_id > 0 then 
      'TABLE'
      + ''', N''' + object_name(ob.parent_object_id)
    else 
    case
      when ob.type in ('TF', 'FN', 'IF', 'FS', 'FT') then 'FUNCTION'
      when ob.type in ('P', 'PC', 'RF', 'X') then 'PROCEDURE'
      when ob.type in ('U', 'IT') then 'TABLE'
      when ob.type = 'SQ' then 'QUEUE'
      else upper(ob.type_desc)  collate Cyrillic_General_CI_AS
    end
      + ''', N''' + ob.name
  end
  +
  case
    when col.column_id is null then ''
    else ''',''COLUMN'', N''' + col.name
  end 
  + '''' + @lf
from
  sys.extended_properties ep
    inner join sys.objects ob        on ep.major_id = ob.OBJECT_ID
                                    and class = 1
    left outer join sys.columns col  on ep.major_id = col.Object_id
                                    and class = 1
                                    and ep.minor_id = col.column_id
where ob.OBJECT_ID = @object_id

/* Result */
select @file_name as [File], @definition as [Definition]
where isnull(@file_name,'') <> ''
