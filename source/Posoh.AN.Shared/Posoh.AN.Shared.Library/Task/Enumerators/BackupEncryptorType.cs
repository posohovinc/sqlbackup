﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum BackupEncryptorType
  {
    ServerCertificate,
    ServerAsymmetricKey
  }
}