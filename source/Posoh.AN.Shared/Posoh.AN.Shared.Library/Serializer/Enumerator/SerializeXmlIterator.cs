﻿using System.Xml;

namespace Posoh.AN.Shared.Library.Serializer.Enumerator
{
  public sealed class SerializeXmlIterator: SerializeIterator
  {
    private readonly XmlNode ancestor;
    private XmlNode currentNode;
    public SerializeXmlIterator(XmlNode ancestor)
    {
      this.ancestor = ancestor;
      Reset();
    }

    public override bool MoveNext()
    {
      var result = currentNode != null;
      if (result)
      {
        current = new SerializerPair(currentNode.LocalName, currentNode.Value, currentNode);
        currentNode = currentNode.NextSibling;
      }

      return result;
    }

    public override void Reset()
    {
      currentNode = ancestor.FirstChild;
    }
  }
}
