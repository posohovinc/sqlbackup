﻿using System;
using System.Data;

namespace Posoh.AN.Shared.Library.DataBaseCollector
{

    internal class CoreSafeReader : IDataReader
    {
        #region private
        private IDataReader dataReader;
        #endregion

        public CoreSafeReader(IDataReader dataReader)
        {
            this.dataReader = dataReader;
        }
        public IDataReader DataReader
        {
            get { return dataReader; }
        }
        public string GetString(string name)
        {
            return GetString(dataReader.GetOrdinal(name));
        }
        public virtual string GetString(int i)
        {
            return dataReader.IsDBNull(i) ? string.Empty : dataReader.GetString(i);
        }
        public object GetValue(string name)
        {
            return GetValue(dataReader.GetOrdinal(name));
        }
        public virtual string GetName(int i)
        {
            return dataReader.GetName(i);
        }
        public string GetDataTypeName(string name)
        {
            return GetDataTypeName(dataReader.GetOrdinal(name));
        }
        public virtual string GetDataTypeName(int i)
        {
            return dataReader.GetDataTypeName(i);
        }
        public Type GetFieldType(string name)
        {
            return GetFieldType(dataReader.GetOrdinal(name));
        }
        public virtual Type GetFieldType(int i)
        {
            return dataReader.GetFieldType(i);
        }
        public virtual object GetValue(int i)
        {
            return dataReader.IsDBNull(i) ? null : dataReader.GetValue(i);
        }
        public int GetValues(object[] values)
        {
            return dataReader.GetValues(values);
        }
        public int GetOrdinal(string name)
        {
            return dataReader.GetOrdinal(name);
        }
        public int GetInt32(string name)
        {
            return GetInt32(dataReader.GetOrdinal(name));
        }
        public virtual int GetInt32(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetInt32(i);
        }
        public double GetDouble(string name)
        {
            return GetDouble(dataReader.GetOrdinal(name));
        }
        public virtual double GetDouble(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetDouble(i);
        }
        public Guid GetGuid(string name)
        {
            return GetGuid(dataReader.GetOrdinal(name));
        }
        public virtual Guid GetGuid(int i)
        {
            return dataReader.IsDBNull(i) ? Guid.Empty : dataReader.GetGuid(i);
        }
        public bool GetBoolean(string name)
        {
            return GetBoolean(dataReader.GetOrdinal(name));
        }
        public virtual bool GetBoolean(int i)
        {
            return dataReader.IsDBNull(i) ? false : dataReader.GetBoolean(i);
        }
        public byte GetByte(string name)
        {
            return GetByte(dataReader.GetOrdinal(name));
        }
        public virtual byte GetByte(int i)
        {
            return dataReader.IsDBNull(i) ? (byte)0 : dataReader.GetByte(i);
        }
        public long GetBytes(string name, long fieldOffset, byte[] buffer, int bufferOffset, int length)
        {
            return GetBytes(dataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length);
        }
        public virtual long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferOffset, int length)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetBytes(i, fieldOffset, buffer, bufferOffset, length);
        }
        public char GetChar(string name)
        {
            return GetChar(dataReader.GetOrdinal(name));
        }
        public virtual char GetChar(int i)
        {
            return dataReader.IsDBNull(i) ? char.MinValue : dataReader.GetChar(i);
        }
        public long GetChars(string name, long fieldOffset, char[] buffer, int bufferOffset, int length)
        {
            return GetChars(dataReader.GetOrdinal(name), fieldOffset, buffer, bufferOffset, length);
        }
        public virtual long GetChars(int i, long fieldOffset, char[] buffer, int bufferOffset, int length)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetChars(i, fieldOffset, buffer, bufferOffset, length);
        }
        public DateTime GetDateTime(string name)
        {
            return GetDateTime(dataReader.GetOrdinal(name));
        }
        public virtual DateTime GetDateTime(int i)
        {
            return dataReader.IsDBNull(i) ? DateTime.MinValue : dataReader.GetDateTime(i);
        }
        public IDataReader GetData(string name)
        {
            return GetData(dataReader.GetOrdinal(name));
        }
        public virtual IDataReader GetData(int i)
        {
            return dataReader.GetData(i);
        }
        public bool IsDBNull(string name)
        {
            return IsDBNull(GetOrdinal(name));
        }
        public virtual bool IsDBNull(int i)
        {
            return dataReader.IsDBNull(i);
        }
        public int FieldCount
        {
            get { return dataReader.FieldCount; }
        }
        public decimal GetDecimal(string name)
        {
            return GetDecimal(dataReader.GetOrdinal(name));
        }
        public virtual decimal GetDecimal(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetDecimal(i);
        }
        public float GetFloat(string name)
        {
            return GetFloat(dataReader.GetOrdinal(name));
        }
        public virtual float GetFloat(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetFloat(i);
        }
        public short GetInt16(string name)
        {
            return GetInt16(dataReader.GetOrdinal(name));
        }
        public virtual short GetInt16(int i)
        {
            return dataReader.IsDBNull(i) ? (short)0 : dataReader.GetInt16(i);
        }
        public Int64 GetInt64(string name)
        {
            return GetInt64(dataReader.GetOrdinal(name));
        }
        public virtual Int64 GetInt64(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : dataReader.GetInt64(i);
        }
        public object this[string name]
        {
            get
            {
                var value = dataReader[name];
                return DBNull.Value.Equals(value) ? null : value;
            }
        }
        public virtual object this[int i]
        {
            get
            {
                return dataReader.IsDBNull(i) ? null : dataReader[i];
            }
        }
        public bool Read()
        {
            return dataReader.Read();
        }
        public int Depth
        {
            get { return dataReader.Depth; }
        }
        public bool IsClosed
        {
            get { return dataReader.IsClosed; }
        }
        public int RecordsAffected
        {
            get { return dataReader.RecordsAffected; }
        }
        public void Close()
        {
            dataReader.Close();
        }
        public DataTable GetSchemaTable()
        {
            return dataReader.GetSchemaTable();
        }
        public bool NextResult()
        {
            return dataReader.NextResult();
        }

        #region misc
        public long GetIntValue(string name)
        {
            return GetIntValue(dataReader.GetOrdinal(name));
        }
        public virtual long GetIntValue(int i)
        {
            return dataReader.IsDBNull(i) ? 0 : long.Parse(GetValue(i).ToString());
        }

        public virtual T GetValue<T>(string name)
        {
            return GetValue<T>(dataReader.GetOrdinal(name));
        }
        public T GetValue<T>(int i)
        {
            return dataReader.IsDBNull(i) ? default(T) : (T)dataReader.GetValue(i);
        }
        #endregion
        #region IDisposable
        private bool dispose;
        protected virtual void Dispose(bool disposing)
        {
            if (!dispose)
            {
                if (disposing)
                {
                    dataReader.Dispose();
                    dataReader = null;
                }
            }
            dispose = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~CoreSafeReader()
        {
            Dispose(false);
        }
        #endregion
    }
}
