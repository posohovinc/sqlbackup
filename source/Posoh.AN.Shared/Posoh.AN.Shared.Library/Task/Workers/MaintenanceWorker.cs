using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  public class MaintenanceWorker : ElementWorker
  {

    #region fields
    private DatabaseSelection databaseSelectionType = DatabaseSelection.All;
    private List<string> selectedDatabases = new List<string>();
    private TableSelection tableSelectionType = TableSelection.None;
    private List<string> selectedTables = new List<string>();
    private ObjectType objectTypeSelection = ObjectType.Invalid;
    private int serverVersion;
    private bool extendedLogging;
    private string localConnectionForLogging;
    private string taskName;
    private bool ignoreDatabasesInNotOnlineState;
    private List<string> description = new List<string>();
    private string runId;
    private List<string> sqlBatchCommands = new List<string>();
    private string taskConnectionName;
    private bool taskNameWasModified;
    #endregion

    [XmlOrderedAttribute(10, Constants.PERSIST_DATABASE_SELECTION_TYPE)]
    public DatabaseSelection DatabaseSelectionType
    {
      get => databaseSelectionType;
      set 
      { 
        if (databaseSelectionType != value)
        {
          databaseSelectionType = value;
          OnPropertyChanged();
        }
      }
    }

    [XmlEmbedded]
    [XmlOrderedElement(20, Constants.PERSIST_SELECTED_DATABASES)]
    [XmlArrayItem(Constants.PERSIST_DATABASE_NAME)]
    public List<string> SelectedDatabases
    {
      get => selectedDatabases;
      set 
      { 
        if (selectedDatabases != value)
        {
          selectedDatabases.Clear();
          if (value != null)
          {
            selectedDatabases.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }

    [DefaultValue(TableSelection.None)]
    [XmlOrderedAttribute(30, Constants.PERSIST_TABLE_SELECTION_TYPE)]
    public TableSelection TableSelectionType
    {
      get => tableSelectionType;
      set 
      { 
        if (tableSelectionType != value)
        {
          tableSelectionType = value;
          OnPropertyChanged();
        }
      }
    }

    [XmlOrderedElement(40, Constants.PERSIST_SELECTED_TABLES)]
    [XmlArrayItem(Constants.PERSIST_TABLE_NAME)]
    public List<string> SelectedTables
    {
      get => selectedTables;
      set 
      { 
        if (selectedTables != value)
        {
          selectedTables.Clear();
          if (value != null)
          {
            selectedTables.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(ObjectType.Invalid)]
    [XmlOrderedAttribute(50, Constants.PERSIST_OBJECT_TYPE_SELECTION)]
    public ObjectType ObjectTypeSelection
    {
      get => objectTypeSelection;
      set 
      { 
        if (objectTypeSelection != value)
        {
          objectTypeSelection = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(60, Constants.PERSIST_SERVER_VERSION)]
    public int ServerVersion
    {
      get => serverVersion;
      set 
      { 
        if (serverVersion != value)
        {
          serverVersion = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(70, Constants.PERSIST_EXTENDED_LOGGING)]
    public bool ExtendedLogging
    {
      get => extendedLogging;
      set 
      { 
        if (extendedLogging != value)
        {
          extendedLogging = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(80, Constants.PERSIST_LOCAL_CONNECTION_FOR_LOGGING)]
    public string LocalConnectionForLogging
    {
      get => localConnectionForLogging;
      set 
      { 
        value ??= string.Empty;
        if (localConnectionForLogging != value)
        {
          localConnectionForLogging = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(90, Constants.PERSIST_TASK_NAME)]
    public string TaskName
    {
      get => taskName;
      set 
      { 
        value ??= string.Empty;
        if (taskName != value)
        {
          taskName = value;
          OnPropertyChanged();
        }
      }
    }
    [XmlOrderedAttribute(100, Constants.PERSIST_IGNORE_DATABASES_IN_NOT_ONLINE_STATE)]
    public bool IgnoreDatabasesInNotOnlineState
    {
      get => ignoreDatabasesInNotOnlineState;
      set 
      { 
        if (ignoreDatabasesInNotOnlineState != value)
        {
          ignoreDatabasesInNotOnlineState = value;
          OnPropertyChanged();
        }
      }
    }

    public virtual bool TaskAllowesDatbaseSelection => true;
    public virtual bool TaskAllowesTableSelection => false;

    public virtual List<string> Description
    {
      get => description;
      set 
      { 
        if (description != value)
        {
          description.Clear();
          if (value != null)
          {
            description.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }

    public string RunId
    {
      get => runId;
      set 
      { 
        value ??= string.Empty;
        if (runId != value)
        {
          runId = value;
          OnPropertyChanged();
        }
      }
    }
    protected List<string> SqlBatchCommands
    {
      get => sqlBatchCommands;
      set 
      { 
        if (sqlBatchCommands != value)
        {
          sqlBatchCommands.Clear();
          if (value != null)
          {
            sqlBatchCommands.AddRange(value);
          }
          OnPropertyChanged();
        }
      }
    }
    public string TaskConnectionName
    {
      get => taskConnectionName;
      set 
      { 
        value ??= string.Empty;
        if (taskConnectionName != value)
        {
          taskConnectionName = value;
          OnPropertyChanged();
        }
      }
    }
    public bool TaskNameWasModified
    {
      get => taskNameWasModified;
      set 
      { 
        if (taskNameWasModified != value)
        {
          taskNameWasModified = value;
          OnPropertyChanged();
        }
      }
    }
  }
}
