﻿using Posoh.AN.UI.VM.Worker;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Worker
{
    public abstract class AbstractCheckIntegrityWorkerControl : WorkerBaseControl<CheckIntegrityWorkerViewModel>
    {
    }
    public abstract class AbstractCleanupWorkerControl : WorkerBaseControl<CleanupWorkerViewModel>
    {
    }
    public abstract class AbstractExecuteSQLServerAgentJobWorkerControl : WorkerBaseControl<ExecuteSQLServerAgentJobWorkerViewModel>
    {
    }
    public abstract class AbstractExecuteSqlStatementWorkerControl : WorkerBaseControl<ExecuteSqlStatementWorkerViewModel>
    {
    }
    public abstract class AbstractHistoryCleanupWorkerControl : WorkerBaseControl<HistoryCleanupWorkerViewModel>
    {
    }
    public abstract class AbstractNotifyOperatorWorkerControl : WorkerBaseControl<NotifyOperatorWorkerViewModel>
    {
    }
    public abstract class AbstractRebuildIndexWorkerControl : WorkerBaseControl<RebuildIndexWorkerViewModel>
    {
    }
    public abstract class AbstractReorganizeIndexWorkerControl : WorkerBaseControl<ReorganizeIndexWorkerViewModel>
    {
    }
    public abstract class AbstractShrinkDataBaseWorkerControl : WorkerBaseControl<ShrinkDataBaseWorkerViewModel>
    {
    }
    public abstract class AbstractUpdateStatisticsWorkerControl : WorkerBaseControl<UpdateStatisticsWorkerViewModel>
    {
    }
    public abstract class AbstractSharedWorkerControl : WorkerBaseControl<MaintenanceWorkerViewModel>
    {
    }
}
