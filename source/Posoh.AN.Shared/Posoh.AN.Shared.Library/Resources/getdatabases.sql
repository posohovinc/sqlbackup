﻿SELECT
name AS [Name]
from master.sys.databases
where [name] not in ('tempdb', 'master', 'model', 'msdb') and database_id not in(
select database_id from msdb.sys.database_mirroring
where mirroring_role_desc='Mirror')
and state = 0
order by Name 