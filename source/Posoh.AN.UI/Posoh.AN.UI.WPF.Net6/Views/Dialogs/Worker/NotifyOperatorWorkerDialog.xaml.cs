﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for NotifyOperatorWorkerDialog.xaml
    /// </summary>
    public partial class NotifyOperatorWorkerDialog : AbstractNotifyOperatorWorkerDialog
    {
        public NotifyOperatorWorkerDialog() 
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            NotifyOperatorWorkerControl.Self = Vm;
        }
    }
}
