﻿using System;
using System.Collections.Generic;
using System.Text;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.Shared.Library.Task.Workers;

namespace Posoh.AN.UI.VM.Worker
{
    public class ExecuteSqlStatementWorkerViewModel : WorkerViewModelBase<TSQLExecuteWorker>
    {
        public ExecuteSqlStatementWorkerViewModel(IList<CoreConnection> connections) : base(connections)
        {
        }
    }
}
