select
  j.name + '.' + s.step_name + '.sql' as [File]
 ,s.command                           as [Definition]
from
  msdb.dbo.sysjobs j
    join msdb.dbo.sysjobsteps s on s.job_id = j.job_id
where
  j.job_id = @id
