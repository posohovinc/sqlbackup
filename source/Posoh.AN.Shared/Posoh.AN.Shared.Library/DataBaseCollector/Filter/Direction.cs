﻿namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    public enum Direction
    {
        Include,
        Exclude
    }
}
