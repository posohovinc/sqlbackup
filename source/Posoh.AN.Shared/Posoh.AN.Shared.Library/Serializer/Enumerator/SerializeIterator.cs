﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Internal;

namespace Posoh.AN.Shared.Library.Serializer.Enumerator
{
  public abstract  class SerializeIterator : IEnumerator<SerializerPair>, IEnumerator, IDisposable
  {
    protected SerializerPair current;
    public abstract bool MoveNext();

    public abstract void Reset();

    public SerializerPair Current => current;

    object? IEnumerator.Current => Current;

    public void Dispose()
    {
    }
  }
}
