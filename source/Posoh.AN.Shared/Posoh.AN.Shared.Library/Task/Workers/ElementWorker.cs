using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.Misc;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task.Workers
{
  [Serializable]
  [XmlRoot(Constants.PERSIST_SQL_TASK_DATA)]
  public class ElementWorker: Element
  {

    #region fields
    private string connection;
    private uint timeOut;
    private bool isStoredProcedure;
    private bool bypassPrepare = true;
    private SqlStatementSourceType sqlStatementSourceType = SqlStatementSourceType.DirectInput;
    private string sqlStatementSource = string.Empty;
    private uint codePage;
    private ResultSetType resultSetType = ResultSetType.ResultSetType_None;
    private TypeConversionMode typeConversionMode = TypeConversionMode.Allowed;
    private object executionValue;
    private ParameterBindings parameterBindings = new ParameterBindings();
    private ResultBindings resultSetBindings = new ResultBindings();
    #endregion

    [XmlOrderedAttribute(10, Constants.PERSIST_CONNECTION)]
    public string Connection
    {
      get => connection;
      set 
      { 
        value ??= string.Empty;
        if (connection != value)
        {
          connection = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue((uint)0)]
    [XmlOrderedAttribute(20, Constants.PERSIST_TIME_OUT)]
    public uint TimeOut
    {
      get => timeOut;
      set 
      { 
        if (timeOut != value)
        {
          timeOut = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(false)]
    [XmlOrderedAttribute(30, Constants.PERSIST_IS_STORED_PROCEDURE)]
    public bool IsStoredProcedure
    {
      get => isStoredProcedure;
      set 
      { 
        if (isStoredProcedure != value)
        {
          isStoredProcedure = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(true)]
    [XmlOrderedAttribute(40, Constants.PERSIST_BYPASS_PREPARE)]
    public bool BypassPrepare
    {
      get => bypassPrepare;
      set 
      { 
        if (bypassPrepare != value)
        {
          bypassPrepare = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(SqlStatementSourceType.DirectInput)]
    [XmlOrderedAttribute(50, Constants.PERSIST_SQL_STATEMENT_SOURCE_TYPE)]
    public SqlStatementSourceType SqlStatementSourceType
    {
      get => sqlStatementSourceType;
      set 
      { 
        if (sqlStatementSourceType != value)
        {
          sqlStatementSourceType = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue("")]
    [XmlOrderedAttribute(60, Constants.PERSIST_SQL_STATEMENT_SOURCE)]
    public string SqlStatementSource
    {
      get => sqlStatementSource;
      set 
      { 
        value ??= string.Empty;
        if (sqlStatementSource != value)
        {
          sqlStatementSource = value;
          OnPropertyChanged();
        }
      }
    }

    [DefaultValue((uint)0)]
    [XmlOrderedAttribute(70, Constants.PERSIST_CODE_PAGE)]
    public uint CodePage
    {
      get => codePage;
      set 
      { 
        if (codePage != value)
        {
          codePage = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(ResultSetType.ResultSetType_None)]
    [XmlOrderedAttribute(80, Constants.PERSIST_RESULT_SET_TYPE)]
    public ResultSetType ResultSetType
    {
      get => resultSetType;
      set 
      { 
        if (resultSetType != value)
        {
          resultSetType = value;
          OnPropertyChanged();
        }
      }
    }
    [DefaultValue(TypeConversionMode.Allowed)]
    [XmlOrderedAttribute(90, Constants.PERSIST_TYPE_CONVERSION_MODE)]
    public TypeConversionMode TypeConversionMode
    {
      get => typeConversionMode;
      set 
      { 
        if (typeConversionMode != value)
        {
          typeConversionMode = value;
          OnPropertyChanged();
        }
      }
    }

    public object ExecutionValue
    {
      get => executionValue;
      set 
      { 
        if (executionValue != value)
        {
          executionValue = value;
          OnPropertyChanged();
        }
      }
    }

    [XmlOrderedElement(100, Constants.PERSIST_PARAMETER_BINDINGS)]
    public ParameterBindings ParameterBindings => parameterBindings;

    [XmlOrderedElement(110, Constants.PERSIST_RESULT_SET_BINDINGS)]
    public ResultBindings ResultSetBindings =>resultSetBindings;
    
  }
}
