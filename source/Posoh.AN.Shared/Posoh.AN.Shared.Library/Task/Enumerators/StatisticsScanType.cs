﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum StatisticsScanType
  {
    Percent = 1,
    Rows = 2,
    FullScan = 3,
    Resample = 4,
    Default = 5,
  }
}