using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Posoh.AN.CLI.Commands;

namespace CLI.Test.Commands;

[TestClass]
public class CreateConnectionCommandTest
{
    [TestMethod]
    public void CreateCommandTest()
    {
        var command = new CreateConnectionCommand((new List<string>()
        {
            "-conname",
            "TestName",
            "-server",
            "ServerName",
            "-user",
            "UserName",
            "-pass",
            "PassName",
            "-dbname",
            "DbName"
        }).ToArray());
        command.Execute();
    }
}