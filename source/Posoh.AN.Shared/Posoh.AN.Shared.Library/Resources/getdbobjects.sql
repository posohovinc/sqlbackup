﻿select
   o.object_id
  ,CASE o.type
    WHEN 'AF' THEN 'Programmability\Functions' -- = агрегатнаџ функциџ (среда CLR)
    WHEN 'C' THEN 'Programmability\Checks' -- = ограничение CHECK
    WHEN 'D' THEN 'Programmability\Defaults' -- = значение по умолчанию (DEFAULT), в ограничении или независимо заданное
    WHEN 'F' THEN 'Constraints\ForeignKeys' -- = ограничение FOREIGN KEY
    WHEN 'FN' THEN 'Programmability\Functions' -- = скалџрнаџ функциџ SQL
    WHEN 'FS' THEN 'Programmability\Assemblies' -- = скалџрнаџ функциџ сборки (среда CLR)
    WHEN 'FT' THEN 'Programmability\Assemblies' -- = функциџ сборки (среда CLR) с табличным значением
    WHEN 'IF' THEN 'Programmability\Functions' -- = встроеннаџ функциџ SQL с табличным значением
    WHEN 'IT' THEN 'Tables\Internal' -- = внутреннџџ таблица
    WHEN 'P' THEN 'Programmability\Procedures' -- = хранимаџ процедура SQL
    WHEN 'PC' THEN 'Programmability\Assemblies' -- = хранимаџ процедура сборки (среда CLR)
    WHEN 'PG' THEN 'Programmability\Plan Guides' -- = структура плана
    WHEN 'PK' THEN 'Constraints\PrimaryKeys' -- = ограничение PRIMARY KEY
    WHEN 'R' THEN 'Rules' -- = правило (старый стиль, изолированный)
    WHEN 'RF' THEN 'ReplicationFilterProcedures' -- = процедура фильтра репликации
    WHEN 'S' THEN 'Tables\System' -- = системнаџ базоваџ таблица
    WHEN 'SN' THEN 'Synonyms' -- = синоним
    WHEN 'SO' THEN 'Programmability\Sequences' -- = объект последовательности
    WHEN 'U' THEN 'Tables' -- = таблица (пользовательскаџ)
    WHEN 'V' THEN 'Views' -- = представление
    WHEN 'EC' THEN 'Constraints\Edge' -- = ограничение ребра
    WHEN 'SQ' THEN 'ServiceQueues' -- = очередь обслуживаниџ
    WHEN 'TA' THEN 'Triggers\CLR' -- = триггер DML сборки (среда CLR)
    WHEN 'TF' THEN 'Programmability\Functions' -- = возвращающаџ табличное значение функциџ SQL
    WHEN 'TR' THEN 'Triggers\DML' -- = триггер DML SQL
    WHEN 'TT' THEN 'Programmability\Types\TableTypes' -- = табличный тип
    WHEN 'UQ' THEN 'Constraints\Unique' -- = ограничение UNIQUE
    WHEN 'X' THEN 'ExtendedPocedures' -- = расширеннаџ хранимаџ процедура
  END    AS [Folder]
  , schema_name(o.schema_id) + '.' + o.name + '.sql'  [File]
  ,o.type [Type]
  ,schema_name(o.schema_id) + '.' + o.name [Name]
FROM sys.objects o 
  left join sys.sql_modules m ON m.object_id = o.object_id
ORDER BY [Folder], [File]

