﻿using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.UI.VM;

namespace Posoh.AN.UI.WPF.Net6.ViewModels
{
    public class ConnectionPropertiesViewModel:ViewModelBase
    {
        #region Fields

        private CoreConnection coreConnection;
        private bool _isReadOnlyUserName;
        private bool _isReadOnlyPassword;

        #endregion

        public CoreConnection CoreConnection
        {
            get { return coreConnection; }
            set
            {
                coreConnection = value;
                OnPropertyChanged();
                OnPropertyChanged("SelectedAuthType");
                UpdateReadOnly(coreConnection.AuthType);
            }
        }

        public int SelectedAuthType
        {
            get { return coreConnection.AuthType; }
            set
            {
                coreConnection.AuthType = value;
                UpdateReadOnly(value);
                OnPropertyChanged();
            }

        }
        public bool IsReadOnlyUserName
        {
            get { return _isReadOnlyUserName; }
            set
            {
                _isReadOnlyUserName = value;
                OnPropertyChanged();
            }

        }
        public bool IsReadOnlyPassword
        {
            get { return _isReadOnlyPassword; }
            set
            {
                _isReadOnlyPassword = value;
                OnPropertyChanged();
            }

        }

        private void UpdateReadOnly(int authType)
        {
            switch (authType)
            {
                case 0:
                    IsReadOnlyUserName = true;
                    IsReadOnlyPassword = true;
                    break;
                case 1:
                    IsReadOnlyUserName = false;
                    IsReadOnlyPassword = false;
                    break;
                case 2:
                    IsReadOnlyUserName = false;
                    IsReadOnlyPassword = true;
                    break;
                case 3:
                    IsReadOnlyUserName = false;
                    IsReadOnlyPassword = false;
                    break;
                case 4:
                    IsReadOnlyUserName = false;
                    IsReadOnlyPassword = false;
                    break;
            }
        }
    }
}