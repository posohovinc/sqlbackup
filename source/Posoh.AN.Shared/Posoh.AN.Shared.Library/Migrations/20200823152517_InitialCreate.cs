﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Posoh.AN.Shared.Library.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConnectionsToBase",
                columns: table => new
                {
                    ServerId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Instance = table.Column<string>(nullable: true),
                    AuthType = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionsToBase", x => x.ServerId);
                });

            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    PlanId = table.Column<Guid>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Data = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.PlanId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConnectionsToBase");

            migrationBuilder.DropTable(
                name: "Plans");
        }
    }
}
