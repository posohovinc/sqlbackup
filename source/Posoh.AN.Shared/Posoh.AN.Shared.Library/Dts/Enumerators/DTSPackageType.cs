﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSPackageType
  {
    Default = 0,
    DTSWizard = 1,
    DTSDesigner = 2,
    SQLReplication = 3,
    DTSDesigner100 = 5,
    SQLDBMaint = 6,
  }
}