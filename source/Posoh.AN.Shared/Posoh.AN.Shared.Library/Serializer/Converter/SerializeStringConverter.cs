﻿using System;
using System.ComponentModel;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeStringConverter: SerializeConverter
  {
    public override bool ValueToString(Serializer sender, out string stringValue, object value, object graph, ReflectionClient client, object ancestor)
    {
      var result = base.ValueToString(sender, out stringValue, value, graph, client, ancestor);
      if (result && Attribute.GetCustomAttribute(client.Info, typeof(PasswordPropertyTextAttribute)) != null)
      {
        stringValue = Serializer.Decrypt(stringValue);
      }

      return result;
    }

    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      var propertyValue =
        (Attribute.GetCustomAttribute(client.Info, typeof(PasswordPropertyTextAttribute)) == null)
          ? stringValue
          : Serializer.Encrypt(stringValue);
      return base.StringToValue(sender, propertyValue, ref value, graph, client, ancestor);
    }
  }
}
