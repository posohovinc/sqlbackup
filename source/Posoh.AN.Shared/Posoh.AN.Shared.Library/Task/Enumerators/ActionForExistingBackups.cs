﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum ActionForExistingBackups
  {
    Append,
    Overwrite
  }
}