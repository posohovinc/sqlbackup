﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSForcedExecResult
  {
    None = -1,
    Success = 0,
    Completion = 2,
    Failure = 1,
  }
}