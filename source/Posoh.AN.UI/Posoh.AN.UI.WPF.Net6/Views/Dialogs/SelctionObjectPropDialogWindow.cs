﻿using System.Windows;
using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls
{
    public class SelctionObjectPropDialogWindow:DataBasePropDialogWindow
    {
        public SelctionObjectPropDialogWindow(string title) : base(title)
        {
            gridWithProp.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(20, 80, 0, 0),
                Text = "Object:",
                VerticalAlignment = VerticalAlignment.Top
            });
            gridWithProp.Children.Add(new TextBlock()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(20, 110, 0, 0),
                Text = "Selection:",
                VerticalAlignment = VerticalAlignment.Top
            }); 
            gridWithProp.Children.Add(new ComboBox()
            {
                Margin = new Thickness(130, 77, 10, 0),
                VerticalAlignment = VerticalAlignment.Top
            });
            gridWithProp.Children.Add(new ComboBox()
            {
                Margin = new Thickness(130, 107, 10, 0),
                VerticalAlignment = VerticalAlignment.Top
            });
        }
    }
}
