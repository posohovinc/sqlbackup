﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSTransactionOption
  {
    NotSupported,
    Supported,
    Required
  }
}