﻿select
  job_id             as object_id
 ,'ServerAgent\Jobs' as [Folder]
 ,[name] + '.sql'    as [File]
 ,[name]             as [Name]
from
  msdb.dbo.sysjobs
order by
  [Name] asc
