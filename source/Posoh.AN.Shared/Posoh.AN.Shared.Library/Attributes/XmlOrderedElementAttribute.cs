﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.Attributes
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue, AllowMultiple = true)]
  public class XmlOrderedElementAttribute : XmlElementAttribute, IOrdered
  {
    public XmlOrderedElementAttribute() : this(int.MaxValue, string.Empty)
    {
    }
    public XmlOrderedElementAttribute(string key) : this(int.MaxValue, key)
    {
    }
    public XmlOrderedElementAttribute(int order) : this(order, string.Empty)
    {
    }
    public XmlOrderedElementAttribute(int order, string key)
    {
      Order = order;
      ElementName = key;
    }
    public int CompareTo(object? obj)
    {
      var result = 1;
      if (obj is IOrdered ordered)
      {
        result = Order - ordered.Order;
        if (result == 0)
          result = String.Compare(Key, ordered.Key, StringComparison.Ordinal);
      }

      return result;
    }


    public string Key => ElementName;
  }
}
