﻿using System;
using System.ComponentModel;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeEnumConverter : SerializeConverter
  {
    public override bool ValueToString(Serializer sender, out string stringValue, object value, object graph, ReflectionClient client, object ancestor)
    {
      stringValue = ((int)value).ToString();
      return true;
    }
    public override bool StringToValue(Serializer sender, string stringValue, ref object value, object graph, ReflectionClient client, object ancestor)
    {
      var converter = TypeDescriptor.GetConverter(client.InfoType);
      value = !(converter.GetType() == typeof(EnumConverter)) ? converter.ConvertFrom(stringValue) : Enum.Parse(client.InfoType, stringValue);
      return true;
    }
  }
}
