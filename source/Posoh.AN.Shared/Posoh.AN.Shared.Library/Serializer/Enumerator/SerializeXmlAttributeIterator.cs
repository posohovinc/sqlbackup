﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Posoh.AN.Shared.Library.Serializer.Enumerator
{
  public sealed class SerializeXmlAttributeIterator: SerializeIterator
  {
    private readonly XmlNode ancestor;
    private int currentIndex;
    public SerializeXmlAttributeIterator(XmlNode ancestor)
    {
      this.ancestor = ancestor;
      Reset();
    }

    public override bool MoveNext()
    {
      var result = (ancestor != null) && (ancestor.Attributes != null) && (currentIndex < ancestor.Attributes.Count);
      if (result)
      {
        var currentNode = ancestor.Attributes[currentIndex++];
        current = new SerializerPair(currentNode.LocalName, currentNode.Value, currentNode);
      }

      return result;
    }

    public override void Reset()
    {
      currentIndex = 0;
    }
  }
  
}
