﻿using System;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsEventInfo : DtsObject
  {
    public bool AllowEventHandlers { get; }
    public string CreationName { get; }
    public string Description { get; set; }
    public string ID { get; }
    public string Name { get; set; }
    public int ParameterCount { get; }
    public string[] ParameterDescriptions { get; }
    public string[] ParameterNames { get; }
    public TypeCode[] ParameterTypes { get; }
  }
}