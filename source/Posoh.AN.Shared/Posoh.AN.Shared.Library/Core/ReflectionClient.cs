﻿using System;
using System.ComponentModel;
using System.Reflection;
using Posoh.AN.Shared.Library.Attributes;

namespace Posoh.AN.Shared.Library.Core
{
  public sealed class ReflectionClient : IComparable
  {
    private const BindingFlags AllLevelFlags =
      BindingFlags.FlattenHierarchy |
      BindingFlags.Instance |
      BindingFlags.Public |
      BindingFlags.NonPublic;

    private readonly Attribute attribute;
    private readonly MemberInfo memberInfo;
    private readonly object defaultValue;
    public ReflectionClient(MemberInfo memberInfo, Attribute attribute)
    {
      this.attribute = attribute;
      this.memberInfo = memberInfo;
      this.defaultValue = memberInfo.GetCustomAttribute<DefaultValueAttribute>()?.Value;
    }

    public MemberInfo Info => memberInfo;
    public string Key
    {
      get
      {
        var result = memberInfo.Name;
        if ((attribute is IOrdered order) && (!string.IsNullOrEmpty(order.Key)))
          result = order.Key;
        return result;
      }
    }
    public bool IsDefault(object value)
    {
      return (defaultValue != null) && defaultValue.Equals(value);
    }

    public Type InfoType =>
      memberInfo.MemberType == MemberTypes.Property ?
        ((PropertyInfo)memberInfo).PropertyType :
        ((FieldInfo)memberInfo).FieldType;
    public void SetValue(object target, object value)
    {
      if (memberInfo.MemberType == MemberTypes.Property)
        ((PropertyInfo)memberInfo).SetValue(target, value);
      else
        ((FieldInfo)memberInfo).SetValue(target, value);
    }
    public void GetValue(object target, out object value)
    {
      if (memberInfo.MemberType == MemberTypes.Property)
        value = ((PropertyInfo)memberInfo).GetValue(target);
      else
        value = ((FieldInfo)memberInfo).GetValue(target);
    }
    public override bool Equals(object? obj) => CompareTo(obj) == 0;
    public int CompareTo(object? obj)
    {
      var result = 1;
      if (obj is ReflectionClient client)
      {
        if ((attribute is IOrdered ordered) && (client.attribute is IOrdered clientOrdered))
        {
          result = ordered.CompareTo(clientOrdered);
        }
        else
        {
          result = String.Compare(Key, client.Key, StringComparison.Ordinal);
        }
      }
      return result;
    }
    public override int GetHashCode()
    {
      return (attribute is IOrdered order) ? order.GetHashCode() : Key.GetHashCode();
    }
    public override string ToString()
    {
      return Key;
    }
  }
}
