﻿using System.Windows.Controls;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    /// <summary>
    /// Interaction logic for LabledDoubleUpDown.xaml
    /// </summary>
    public partial class LabledDoubleUpDown : UserControl
    {
        public LabledDoubleUpDown()
        {
            InitializeComponent();
        }
        public string Caption
        {
            get
            {
                return tblCaption.Text;
            }
            set
            {
                tblCaption.Text = value;
            }
        }
    }
}
