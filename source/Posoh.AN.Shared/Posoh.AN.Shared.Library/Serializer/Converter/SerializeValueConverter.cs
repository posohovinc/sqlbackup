﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace Posoh.AN.Shared.Library.Serializer.Converter
{
  public class SerializeValueConverter
  {
    #region fields
    private static readonly SerializeConverter defaultConverter = new SerializeConverter();
    private static readonly SerializeEnumConverter enumConverter = new SerializeEnumConverter();
    private Dictionary<Type, SerializeConverter> converterHash;
    #endregion
    public SerializeValueConverter()
    {
      converterHash = new Dictionary<Type, SerializeConverter>()
      {
        {typeof(string), new SerializeStringConverter()},
        {typeof(TimeSpan), new SerializeTimeSpanConverter()},
        {typeof(DateTime), new SerializeDateTimeConverter()},
        {typeof(Version), new SerializeVersionConverter()},
        {typeof(Point), new SerializePointConverter()},
        {typeof(Size), new SerializeSizeConverter()},
        {typeof(bool), new SerializeBooleanConverter()},
        {typeof(Guid), new SerializeGuidConverter()},
      };
    }

    public void RegisterConverter(Type type, SerializeConverter serializeConverter)
    {
      if (converterHash.ContainsKey(type))
        converterHash[type] = serializeConverter;
      else
        converterHash.Add(type, serializeConverter);
    }

    public bool TryGetConverter(Type type, out SerializeConverter converter)
    {
      if (!converterHash.TryGetValue(type, out converter))
      {
        if (type.IsClass || type.IsArray)
        {
          converterHash.TryGetValue(typeof(object), out converter);
        }
        else if (type.IsEnum)
        {
          converter = enumConverter;
        }
        else
        {
          converter = defaultConverter;
        }
      }

      return converter != null;
    }
  }
}
