﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Posoh.AN.Shared.Library.Core
{
  public interface IPersistent
  {
    bool IsChanged { get; }
    void BeginEdit();
    void EndEdit();
    void CancelEdit();
  }

}
