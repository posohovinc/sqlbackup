﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Posoh.AN.Shared.Library.Serializer.Enumerator
{
  public class SerializerPair
  {
    public SerializerPair(string key, string value, object ancestor)
    {
      Key = key;
      Value = value;
      Ancestor = ancestor;
    }

    public string Key { get; }
    public string Value { get; }
    public object Ancestor { get; }
  }
}

