﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSPrecedenceEvalOp
  {
    Expression = 1,
    Constraint = 2,
    ExpressionAndConstraint = 3,
    ExpressionOrConstraint = 4
  }
}