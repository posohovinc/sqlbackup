﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Posoh.AN.UI.WPF.Net6.Views.Controls.Simple
{
    public class LabeledBase : UserControl
    {
        private TextBlock tblCaption = new TextBlock();
        private Grid grid = new Grid();
        private ColumnDefinition firstColumn;
        private ColumnDefinition lastColumn;

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            firstColumn = new ColumnDefinition() { Width = new GridLength(150, GridUnitType.Pixel) };
            lastColumn = new ColumnDefinition() { Width = new GridLength(40, GridUnitType.Pixel) };

            grid.ColumnDefinitions.Add(firstColumn);
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            grid.ColumnDefinitions.Add(lastColumn);

            tblCaption.Margin = new Thickness(10, 1, 10, 1);

            grid.Children.Add(tblCaption);
            Grid.SetRow(tblCaption, 0);

            ((IAddChild)newContent).AddChild(grid);
        }

        protected Grid Grid => grid;

        public string Caption
        {
            get => tblCaption.Text;
            set => tblCaption.Text = value;
        }

        public double LeftOffsetToCaption
        {
            get => tblCaption.Margin.Left;
            set => tblCaption.Margin = new Thickness(value, tblCaption.Margin.Top, tblCaption.Margin.Right, tblCaption.Margin.Bottom);
        }
        public double LeftOffsetToControl
        {
            get => firstColumn.Width.Value;
            set => firstColumn.Width = new GridLength(value);
        }

        public double LastWidth
        {
            get => lastColumn.Width.Value;
            set => lastColumn.Width = new GridLength(value);
        }

    }
}
