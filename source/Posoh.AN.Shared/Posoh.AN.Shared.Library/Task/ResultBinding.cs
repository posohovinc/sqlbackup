﻿using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;

namespace Posoh.AN.Shared.Library.Task
{
  [XmlRoot("ResultBinding")]
  public class ResultBinding: Element
  {
    [XmlOrderedAttribute(10, "ResultName")]
    public object ResultName { get; set; }
    [XmlOrderedAttribute(20, "DtsVariableName")]
    public string DtsVariableName { get; set; }
  }
}