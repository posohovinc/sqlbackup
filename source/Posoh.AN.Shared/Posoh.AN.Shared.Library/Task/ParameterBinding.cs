﻿using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.Task.Enumerators;

namespace Posoh.AN.Shared.Library.Task
{
  [XmlRoot("ParameterBinding")]
  public class ParameterBinding: Element
  {
    [XmlOrderedAttribute(10, "ParameterName")]
    public object ParameterName { get; set; }
    [XmlOrderedAttribute(20, "DtsVariableName")]
    public string DtsVariableName { get; set; }
    [XmlOrderedAttribute(30, "ParameterDirection")]
    public ParameterDirections ParameterDirection { get; set; }
    [XmlOrderedAttribute(40, "DataType")]
    public int DataType { get; set; }
    [XmlOrderedAttribute(50, "ParameterSize")]
    public int ParameterSize { get; set; }
  }
}