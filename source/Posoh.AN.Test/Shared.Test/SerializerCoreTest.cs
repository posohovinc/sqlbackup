﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;
using Posoh.AN.Shared.Library.Serializer.Converter;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text.Json;
using System.Xml;
using System.Xml.Serialization;
using Posoh.AN.Shared.Library.Serializer;
using Posoh.AN.Shared.Library.Serializer.Enumerator;

namespace Shared.Test
{
  [TestClass]
  public class SerializerCoreTest
  {
    private const string GraphXmlResult = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Shared.Test.ConvertElement PropInt=""10"" PropBool=""True"" PropDateTime=""26.08.2020 12:53:32"" PropEnum=""four"" PropGuid=""dae9d0ca-daa3-4e0e-8c52-d9a2e5442438"" PropPoint=""{X=1,Y=1}"" PropSize=""{Width=10, Height=10}"" PropTimeSpan=""10:55:44"" PropVersion=""1.1.56.1000"">
  <PropArrayInt>
    <int_item>1</int_item>
    <int_item>2</int_item>
    <int_item>3</int_item>
    <int_item>4</int_item>
  </PropArrayInt>
  <PropArrayString>
    <string_item>1</string_item>
    <string_item>2</string_item>
    <string_item>3</string_item>
  </PropArrayString>
  <PropDictionaryString>
    <l_1>
      <PropArrayString>
        <string_item>101</string_item>
        <string_item>202</string_item>
        <string_item>303</string_item>
      </PropArrayString>
    </l_1>
    <l_2 PropDateTime=""26.08.2020 12:53:32"" />
    <l_3 PropVersion=""4.5"" />
  </PropDictionaryString>
  <PropArrayObject>
    <array_item PropEnum=""four"" />
    <array_item PropInt=""99"" />
  </PropArrayObject>
  <PropList>
    <list_item PropBool=""True"" />
    <list_item PropInt=""25"" />
  </PropList>
  <PropObject PropBool=""True"" />
</Shared.Test.ConvertElement>";

    private ConvertElement Graph()
    {
      var n = new Dictionary<string, ConvertSimpleElement>();
      n.Add("l_1", new ConvertSimpleElement() { PropBool = false, PropArrayString = new[] { "101", "202", "303" }, });
      n.Add("l_2", new ConvertSimpleElement() {PropDateTime = new DateTime(2020, 08, 26, 12, 53, 32)});
      n.Add("l_3", new ConvertSimpleElement() { PropVersion = new Version(4,5) });
      return new ConvertElement()
      {
        PropInt = 10,
        PropBool = true,
        PropDateTime = new DateTime(2020, 08, 26, 12, 53, 32),
        PropEnum = ConvertEnum.four,
        PropGuid = new Guid("dae9d0ca-daa3-4e0e-8c52-d9a2e5442438"),
        PropPoint = new Point(1, 1),
        PropSize = new Size(10, 10),
        PropTimeSpan = new TimeSpan(10, 55, 44),
        PropVersion = new Version(1, 1, 56, 1000),

        PropList = new List<ConvertSimpleElement>() { new ConvertSimpleElement(){PropBool = true}, new ConvertSimpleElement(){PropInt = 25} },
        PropObject = new ConvertSimpleElement() { PropBool = true },
        PropArrayObject = new[] { new ConvertSimpleElement(){PropEnum = ConvertEnum.four}, new ConvertSimpleElement(){PropInt = 99} },

        PropArrayInt = new[] { 1, 2, 3, 4 },
        PropArrayString = new[] { "1","2","3" },
        PropDictionaryString = n,
      };
    }
    private ConvertElement DesirializeGraph()
    {
      return new ConvertElement()
      {
        //PropInt = 10,
        //PropBool = true,
        //PropDateTime = new DateTime(2020, 08, 26, 12, 53, 32),
        //PropEnum = ConvertEnum.four,
        //PropGuid = new Guid("dae9d0ca-daa3-4e0e-8c52-d9a2e5442438"),
        //PropPoint = new Point(1, 1),
        //PropSize = new Size(10, 10),
        //PropTimeSpan = new TimeSpan(10, 55, 44),
        //PropVersion = new Version(1, 1, 56, 1000),

        PropList = new List<ConvertSimpleElement>(),
        PropObject = new ConvertSimpleElement(),
        PropArrayObject = new[] { new ConvertSimpleElement(), new ConvertSimpleElement() },

        //PropArrayInt = new[],
        //PropArrayString = new[],
        PropDictionaryString = new Dictionary<string, ConvertSimpleElement>(),
      };
    }

    [TestMethod]
    public void TestJson()
    {
      var json = new JsonElement();
    }

    [TestMethod]
    public void TestDeserializerXml()
    {
      var graph = DesirializeGraph();
      var serializer = new SerializerXml();
      serializer.Deserialize(graph, GraphXmlResult);
      Assert.IsTrue(graph.PropBool && graph.PropInt == 10);
    }
    [TestMethod]
    public void TestSerializerXml()
    {
      var graph = Graph();
      var serializer = new SerializerXml();
      serializer.Serialize(graph, out string result);
      Assert.IsTrue(result.Equals(GraphXmlResult));
    }

    [TestMethod]
    public void TestSerializeXmlEnumerator()
    {
      var xml = @"<TABLE name=""fileset""><COLUMN>LogicalName</COLUMN><COLUMN>PhysicalName</COLUMN><COLUMN>Type</COLUMN><COLUMN>FileGroupName</COLUMN><COLUMN>Size</COLUMN><COLUMN>MaxSize</COLUMN><COLUMN>FileId</COLUMN><COLUMN>BackupSizeInBytes</COLUMN><COLUMN>FileGroupId</COLUMN><ROW><COLUMN>bug_01</COLUMN><COLUMN>C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\bug_01.mdf</COLUMN><COLUMN>D</COLUMN><COLUMN>PRIMARY</COLUMN><COLUMN>8388608</COLUMN><COLUMN>35184372080640</COLUMN><COLUMN>1</COLUMN><COLUMN>2752512</COLUMN><COLUMN>1</COLUMN></ROW><ROW><COLUMN>bug_01_log</COLUMN><COLUMN>C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\bug_01_log.ldf</COLUMN><COLUMN>L</COLUMN><COLUMN/><COLUMN>8388608</COLUMN><COLUMN>35184372080640</COLUMN><COLUMN>2</COLUMN><COLUMN>8704</COLUMN><COLUMN/></ROW></TABLE>";
      var xmlDoc = new XmlDocument();
      using (TextReader tr = new StringReader(xml))
      {
        xmlDoc.Load(tr);
      }

      var first = xmlDoc.FirstChild;
      var serializeXmlEnumerator = new SerializeXmlIterator(first);
      while (serializeXmlEnumerator.MoveNext())
      {
        var current = serializeXmlEnumerator.Current;
        var key = current.Key;
        var value = current.Value;
        var a = current.Ancestor;
      }
    }

    [TestMethod]
    public void TestSerializeXmlAttributeIterator()
    {
      var xml = @"<DATA fileioflags=""1744830464"" threads=""11"" compression=""2"" maxtransfersize=""1048576"" blocksize=""65536"" basesize=""1048576"" databegin=""2695680"" dataend=""3367936""/>
";
                                                                                                                                                                              
      var xmlDoc = new XmlDocument();
      using (TextReader tr = new StringReader(xml))
      {
        xmlDoc.Load(tr);
      }

      var first = xmlDoc.FirstChild;
      var serializeXmlEnumerator = new SerializeXmlAttributeIterator(first);
      while (serializeXmlEnumerator.MoveNext())
      {
        var current = serializeXmlEnumerator.Current;
        var key = current.Key;
        var value = current.Value;
        var a = current.Ancestor;
      }
    }


    [TestMethod]
    public void TestSerializeBooleanConverter()
    {
      var graph = Graph();
      var reflections = Reflection.PropertyList<XmlOrderedAttributeAttribute>(graph);
      foreach (ReflectionClient client in reflections)
      {
        var type = client.InfoType;
        if (type.IsArray)
        {
          Type itemType = type.GetElementType();
          var array = Array.CreateInstance(itemType, 0);
        }
        if (type.IsGenericType)
        {
          var genericTypeDefinition = type.GetGenericTypeDefinition();
          var typeArgs = type.GetGenericArguments();
          Type constructed = genericTypeDefinition.MakeGenericType(typeArgs);
          object o = Activator.CreateInstance(constructed);

          if (genericTypeDefinition == typeof(List<>))
          {
          }
          if (genericTypeDefinition == typeof(Queue<>))
          {
          }
          if (genericTypeDefinition == typeof(Stack<>))
          {
          }

        }
        if (type == typeof(ArrayList))
        {
          var itemType = type.GetGenericArguments();
        }
        if (typeof(Element).IsAssignableFrom(type))
        {
        }

      }
    }
  }

  enum ConvertEnum
  {
    one,
    two,
    three,
    four
  }

  class ConvertSimpleElement : Element
  {
    [Element(1)]
    [XmlOrderedAttribute(1)]
    [DefaultValue(0)]
    public int PropInt { get; set; }

    [Element(10)]
    [XmlOrderedAttribute(10)]
    [DefaultValue(false)]
    public bool PropBool { get; set; }

    [Element(20)]
    [XmlOrderedAttribute(20)]
    [DefaultValue(typeof(DateTime), "01.01.0001 0:00:00")]
    public DateTime PropDateTime { get; set; }

    [Element(30)]
    [XmlOrderedAttribute(30)]
    [DefaultValue(typeof(ConvertEnum), "one")]
    public ConvertEnum PropEnum { get; set; }

    [Element(40)]
    [XmlOrderedAttribute(40)]
    [DefaultValue(typeof(Guid), "00000000-0000-0000-0000-000000000000")]
    public Guid PropGuid { get; set; }

    [Element(50)]
    [XmlOrderedAttribute(50)]
    [DefaultValue(typeof(Point), "0, 0")]
    public Point PropPoint { get; set; }

    [Element(60)]
    [XmlOrderedAttribute(60)]
    [DefaultValue(typeof(Size), "0, 0")]
    public Size PropSize { get; set; }

    [Element(70)]
    [XmlOrderedAttribute(70)]
    [DefaultValue(typeof(TimeSpan), "00:00:00")]
    public TimeSpan PropTimeSpan { get; set; }

    [Element(80)]
    [XmlOrderedAttribute(80)]
    public Version PropVersion { get; set; }

    [Element(90)]
    [XmlArrayItem("int_item")]
    [XmlOrderedElement(81)]
    public int[] PropArrayInt { get; set; }

    [Element(100)]
    [XmlArrayItem("string_item")]
    [XmlOrderedElement(82)]
    public string[] PropArrayString { get; set; }

    [Element(110)]
    [XmlArrayItem("dictionary_item")]
    [XmlOrderedElement(83)]
    public Dictionary<string, ConvertSimpleElement> PropDictionaryString { get; set; }
  }
  class ConvertElement : ConvertSimpleElement
  {
    [Element(120)]
    [XmlArrayItem("array_item")]
    [XmlOrderedElement(90)]
    public ConvertSimpleElement[] PropArrayObject { get; set; }

    [Element(130)]
    [XmlArrayItem("list_item")]
    [XmlOrderedElement(100)]
    public List<ConvertSimpleElement> PropList { get; set; }


    [Element(140)]
    [XmlArrayItem("query_item")]
    [XmlOrderedElement(100)]
    public Queue<ConvertSimpleElement> PropQueue { get; set; }

    [Element(150)]
    [XmlArrayItem("stack_item")]
    [XmlOrderedElement(100)]
    public Stack<ConvertSimpleElement> PropStack { get; set; }

    [Element(160)]
    [XmlArrayItem("sorted_item")]
    [XmlOrderedElement(100)]
    public SortedList<string, ConvertSimpleElement> PropSortedList { get; set; }


    [Element(170)]
    [XmlOrderedElement(120)]
    public ConvertSimpleElement PropObject { get; set; }
  }
}



