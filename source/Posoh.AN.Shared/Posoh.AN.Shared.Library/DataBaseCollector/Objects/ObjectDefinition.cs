﻿namespace Posoh.AN.Shared.Library.DataBaseCollector.Objects
{
    public class ObjectDefinition
    {
        public string FileName { get; set; }
        public string Definition { get; set; }
        public override string ToString()
        {
            return FileName;
        }
    }
}
