﻿using System;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{

    [Serializable]
    public class ExcludeFilters : Filters
    {
        [XmlIgnore]
        public override Direction Direction => Direction.Exclude;
    }
}
