﻿using System;

namespace Posoh.AN.Shared.Library.Attributes
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
  public class XmlNodeValueAttribute: Attribute
  {
  }
}