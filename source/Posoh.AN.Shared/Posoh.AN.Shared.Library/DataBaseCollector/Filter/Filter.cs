﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    [XmlInclude(typeof(MaskFilter))]
    [XmlInclude(typeof(NativeFilter))]
    [XmlInclude(typeof(RegexFilter))]
    [Serializable]
    public abstract class Filter
    {
        [XmlElement]
        public List<string> Patterns { get; set; }

        public FilterDecision Match(string value, Direction direction)
        {
            if ((Patterns != null) && (Patterns.Count != 0))
            {
                foreach (var pattern in Patterns)
                {
                    if (Decide(pattern, value))
                        return direction == Direction.Include ? FilterDecision.Accept : FilterDecision.Dined;
                }
                return direction == Direction.Include ? FilterDecision.Dined : FilterDecision.Accept;
            }
            return FilterDecision.Neutral;
        }

        public abstract bool Decide(string pattern, string value);
    }
}
