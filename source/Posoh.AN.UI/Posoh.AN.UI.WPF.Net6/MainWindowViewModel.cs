﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Posoh.AN.Shared.Library.Entity.Local;
using Posoh.AN.UI.VM;
using Posoh.AN.UI.WPF.Net6.ViewModels.Windows;
using Posoh.AN.UI.WPF.Net6.Views.Windows;

namespace Posoh.AN.UI.WPF.Net6;

public class MainWindowViewModel : ViewModelBase
{
    #region Fields

    private ICommand? _exitCommand;
    private ICommand? _editConnectionCommand;
    private ICommand? _deleteConnectionCommand;
    private ICommand? _testCommand;
    private ObservableCollection<CoreConnection>? _connectionToBase;

    #endregion

    #region Constructor

    public MainWindowViewModel()
    {
        _connectionToBase = new ObservableCollection<CoreConnection>();
        using (var context = new LocalDbContext())
        {
            if (!(((context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator)!).Exists()))
                context.Database.Migrate();

            context.ConnectionsToBase.Load();
            ConnectionsToBase = new ObservableCollection<CoreConnection>(context.ConnectionsToBase.ToList());
        }
    }

    #endregion

    #region Properties

    public ObservableCollection<CoreConnection>? ConnectionsToBase
    {
        get => _connectionToBase;
        set
        {
            _connectionToBase = value;
            OnPropertyChanged();
        }
    }

    #endregion

    #region Commands

    public ICommand? ExitCommand
    {
        get
        {
            if (_exitCommand != null) return _exitCommand;
            _exitCommand = new RelayCommand(p => Exit(p), true);
            return _exitCommand;
        }
    }

    private void Exit(object param)
    {
        if (param is Window win)
        {
            win.Close();
        }
    }

    public ICommand? EditConnectionCommand
    {
        get
        {
            if (_editConnectionCommand != null) return _editConnectionCommand;
            _editConnectionCommand = new RelayCommand(p => EditConnection(p), true);
            return _editConnectionCommand;
        }
    }

    private void EditConnection(object model)
    {
        CoreConnection coreConnection;
        if (model == null)
            coreConnection = new CoreConnection();
        else
            coreConnection = model as CoreConnection;

        var view = new ConnectionPropertiesWindow();
        var viewMode = new ConnectionPropertiesWindowViewModel() {CoreConnection = coreConnection};
        view.DataContext = viewMode;

        coreConnection.BeginEdit();
        try
        {
            if (view.ShowDialog() == true)
            {
                using (var context = new LocalDbContext())
                {
                    if (model == null)
                    {
                        context.Add(coreConnection);
                        ConnectionsToBase.Add(coreConnection);
                    }

                    context.SaveChanges();
                }
            }
            else
            {
                coreConnection.CancelEdit();
            }
        }
        catch (Exception ex)
        {
            coreConnection.CancelEdit();
        }
    }


    public ICommand? DeleteConnectionCommand
    {
        get
        {
            if (_deleteConnectionCommand != null) return _deleteConnectionCommand;
            _deleteConnectionCommand = new RelayCommand(p => DeleteConnection(p), true);
            return _deleteConnectionCommand;
        }
    }

    private void DeleteConnection(object model)
    {
        if (model == null) return;

        if (MessageBox.Show($"Действительно хотите удалить - {model}", "Удаление", MessageBoxButton.YesNo,
                MessageBoxImage.Warning) == MessageBoxResult.Yes)
        {
            var coreConnection = model as CoreConnection;
            using (var context = new LocalDbContext())
            {
                context.Remove(coreConnection);
                _connectionToBase.Remove(coreConnection);
                context.SaveChanges();
            }
        }
    }

    public ICommand? TestCommand
    {
        get
        {
            if (_testCommand != null) return _testCommand;
            _testCommand = new RelayCommand(() => Test());
            return _testCommand;
        }
    }

    private void Test()
    {
        var window = new TestWindow();
        window.ShowDialog();
    }

    #endregion
}