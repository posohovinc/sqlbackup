﻿using System;
using System.ComponentModel;

namespace Posoh.AN.Shared.Library.Core
{
  [Serializable]
  public abstract class Disposable : IDisposable
  {
    private readonly object locker = new object();

    protected bool Disposed { get; private set; }   

    [Browsable(false)]
    public object Locker => locker;

    protected virtual void Free()
    {
    }

    #region IDisposable Support

    ~Disposable()
    {
      Dispose(false);
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (!Disposed)
        {
          lock (Locker)
          {
            if (!Disposed)
            {
              Free();
              Disposed = true;
            }
          }
        }
      }
    }
    #endregion
  }
}