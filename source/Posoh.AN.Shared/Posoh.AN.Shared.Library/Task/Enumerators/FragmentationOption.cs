﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum FragmentationOption
  {
    Fast = 1,
    Sampled = 2,
    Detailed = 3,
  }
}