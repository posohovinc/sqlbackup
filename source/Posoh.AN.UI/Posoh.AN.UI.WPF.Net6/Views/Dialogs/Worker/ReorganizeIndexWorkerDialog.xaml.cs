﻿namespace Posoh.AN.UI.WPF.Net6.Views.Dialogs.Worker
{
    /// <summary>
    /// Interaction logic for ReorgonaizeIndexWorkerDialog.xaml
    /// </summary>
    public partial class ReorganizeIndexWorkerDialog : AbstractReorganizeIndexWorkerDialog
    {
        public ReorganizeIndexWorkerDialog()
        {
            InitializeComponent();
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();
            ReorganizeIndexWorkerControl.Self = Vm;
        }
    }
}
