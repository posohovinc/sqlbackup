﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSPriorityClass
  {
    Default,
    AboveNormal,
    Normal,
    BelowNormal,
    Idle
  }
}