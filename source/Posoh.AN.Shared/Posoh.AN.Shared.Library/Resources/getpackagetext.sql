select
  name + '.dtsx'                                                  as [File]
 ,convert(varchar(max), convert(varbinary(max), packagedata, 2)) as [Definition]
from
  msdb..sysssispackages
where
  id = @id
  and [folderid] = '00000000-0000-0000-0000-000000000000'
order by
  name
