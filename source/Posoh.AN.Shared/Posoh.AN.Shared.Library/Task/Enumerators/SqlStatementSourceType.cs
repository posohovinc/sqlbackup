﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum SqlStatementSourceType
  {
    DirectInput = 1,
    FileConnection = 2,
    Variable = 3
  }
}