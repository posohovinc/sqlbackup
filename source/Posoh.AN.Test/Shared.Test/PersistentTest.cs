using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Posoh.AN.Shared.Library.Attributes;
using Posoh.AN.Shared.Library.Core;

namespace Shared.Test
{

  [Serializable]
  class SimpleElement : Element
  {
    private int propInt = 10;
    private string propString = "������";
    [NotPersistent]
    private int notPersistent = 99;
    [Element(10, "Prop 10")]
    [XmlOrderedAttribute(10, "Prop Xml 10")]
    public int PropNotPersistent
    {
      get => notPersistent;
      set
      {
        notPersistent = value;
        OnPropertyChanged();
      }
    }
    [Element(20, "Prop 20")]
    public int PropInt
    {
      get => propInt;
      set
      {
        propInt = value;
        OnPropertyChanged();
      }
    }
    [Element(30, "Prop 30")]
    public string PropString
    {
      get => propString;
      set
      {
        propString = value;
        OnPropertyChanged();
      }
    }
  }

  
  [Serializable]
  class ListSimpleElement : SimpleElement
  {
    List<int> list = new List<int>();

    public List<int> List
    {
      get => list;
      set
      {
        list.Clear();
        if (value != null)
          list.AddRange(value);
      }
    }
  }

  [Serializable]
  class ListObjectElement : ListSimpleElement
  {
    List<SimpleElement> list = new List<SimpleElement>();

    [Element(5, "Prop 40")]
    [XmlOrderedAttribute(40, "Prop Xml 40")]
    public List<SimpleElement> ElementList
    {
      get => list;
      set
      {
        list.Clear();
        if (value != null)
          list.AddRange(value);
      }
    }
  }


  [TestClass]
  public class PersistentTest
  {
    [TestMethod]
    public void TestCancelEdit()
    {
      var element = new SimpleElement();
      element.BeginEdit();
      element.PropInt = 20;
      element.CancelEdit();
      Assert.IsTrue(element.PropInt == 10);
    }
    [TestMethod]
    public void TestEndEdit()
    {
      var element = new SimpleElement();
      element.BeginEdit();
      element.PropInt = 20;
      element.EndEdit();
      Assert.IsTrue(element.PropInt == 20);
    }
    [TestMethod]
    public void TestNotPersistentCancelEdit()
    {
      var element = new SimpleElement();
      element.BeginEdit();
      element.PropNotPersistent = 41;
      element.CancelEdit();
      Assert.IsTrue(element.PropNotPersistent == 41);
    }

    [TestMethod]
    public void TestListIntCancelEdit()
    {
      var element = new ListSimpleElement();
      element.BeginEdit();
      element.List.Add(1);
      element.List.Add(2);
      element.List.Add(3);
      element.CancelEdit();
      Assert.IsTrue(element.List.Count == 0);
    }

    [TestMethod]
    public void TestListClassCancelEdit()
    {
      var element = new ListObjectElement();
      element.BeginEdit();
      element.ElementList.Add(new SimpleElement());
      element.ElementList.Add(new SimpleElement());
      element.ElementList.Add(new SimpleElement());
      element.EndEdit();
      element.BeginEdit();
      element.ElementList.RemoveAt(1);
      element.CancelEdit();
      Assert.IsTrue(element.ElementList.Count == 3);
    }

    [TestMethod]
    public void TestReflectionElementAttribute()
    {
      var element = new ListObjectElement();
      var attributes = Reflection.PropertyList<ElementAttribute>(element);
      Assert.IsTrue(attributes.Count == 4);
      Assert.IsTrue(attributes[0].Key == "Prop 40");
      Assert.IsTrue(attributes[3].Key == "Prop 30");
    }

    [TestMethod]
    public void TestReflectionElementXmlAttribute()
    {
      var element = new ListObjectElement();
      var attributes = Reflection.PropertyList<XmlOrderedAttributeAttribute>(element);
      Assert.IsTrue(attributes.Count == 2);
      Assert.IsTrue(attributes[0].Key == "Prop Xml 10");
      Assert.IsTrue(attributes[1].Key == "Prop Xml 40");
    }

  }
}
