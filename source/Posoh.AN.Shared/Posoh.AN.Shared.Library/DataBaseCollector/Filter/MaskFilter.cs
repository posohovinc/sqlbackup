﻿using System;
using System.Runtime.InteropServices;

namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    [Serializable]
    public class MaskFilter : Filter
    {
        [DllImport("shlwapi.dll", CharSet = CharSet.Auto, EntryPoint = "PathMatchSpecW")]
        public static extern bool PathMatchSpec(string pszFile, string pszSpec);

        public override bool Decide(string pattern, string value)
        {
            return PathMatchSpec(value, pattern);
        }
    }
}
