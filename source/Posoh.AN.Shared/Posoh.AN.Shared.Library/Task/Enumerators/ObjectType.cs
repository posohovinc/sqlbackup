﻿namespace Posoh.AN.Shared.Library.Task.Enumerators
{
  public enum ObjectType
  {
    Invalid = -1,
    Table = 0,
    View = 1,
    TableView = 2
  }
}