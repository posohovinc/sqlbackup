﻿using System.ComponentModel;
using Posoh.AN.Shared.Library.Dts.Enumerators;

namespace Posoh.AN.Shared.Library.Dts
{
  public class DtsConnectionManager : DtsObject
  {
    public string ConnectionString { get; set; }
    public string CreationName { get; }
    public bool DelayValidation { get; set; }
    public string Description { get; set; }
    public bool HasExpressions { get; }
    public DTSObjectHostType HostType { get; }
    public string ID { get; }
    public object InnerObject { get; }
    public string Name { get; set; }
    public bool OfflineMode { get; set; }
    public DtsProperties Properties { get; }
    public DTSProtectionLevel ProtectionLevel { get; set; }
    public string Qualifier { get; set; }
    public DTSConnectionManagerScope Scope { get; }
    //public ISite Site { get; set; }
    public bool SupportsDTCTransactions { get; }
    //public VariableDispenser VariableDispenser { get; }
    public DtsVariables Variables { get; }
  }
}