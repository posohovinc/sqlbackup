﻿namespace Posoh.AN.Shared.Library.DataBaseCollector.Filter
{
    public enum FilterType
    {
        Mask,
        Regex,
        Native
    }
}
