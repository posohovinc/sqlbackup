﻿using System.Collections.Generic;
using Posoh.AN.Shared.Library.Entity.Local;

namespace Posoh.AN.UI.VM.Worker
{
    public class WorkerViewModelBase<T> : ViewModelBase
    {
        private T worker;
        private CoreConnection _selectedConnection;
        private string _taskName;

        public WorkerViewModelBase(IList<CoreConnection> connections)
        {
            Connections = connections;
        }

        public T Worker
        {
            get => worker;
            set
            {
                if (worker != null && worker.Equals(value)) return;
                worker = value;
                OnPropertyChanged();
            }
        }

        public IList<CoreConnection> Connections { get; }

        public CoreConnection SelectedConnection
        {
            get => _selectedConnection;
            set
            {
                if (_selectedConnection == value) return;
                _selectedConnection = value;
                OnPropertyChanged();
            }
        }

        public string TaskName
        {
            get => _taskName;
            set
            {
                if (_taskName == value) return;
                _taskName = value;
                OnPropertyChanged();
            }
        }
    }
}