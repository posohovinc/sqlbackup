﻿namespace Posoh.AN.Shared.Library.Dts.Enumerators
{
  public enum DTSTargetServerVersion
  {
    Latest = 150,
    SQLServer2012 = 110,
    SQLServer2014 = 120,
    SQLServer2016 = 130,
    SQLServer2017 = 140,
    SQLServer2019 = 150
  }
}